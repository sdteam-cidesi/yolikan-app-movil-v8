package mx.com.appyolikancv01;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.fragment.app.Fragment;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.yarolegovich.lovelydialog.LovelyStandardDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import mx.com.appyolikancv01.fragments.PageCuenta;

public class ResetPassword extends AppCompatActivity implements View.OnClickListener {

    TextView usuario, correo;
    Button enviar;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        setContentView(R.layout.activity_recuperar_password);

        usuario = findViewById(R.id.et_usuario);
        correo = findViewById(R.id.et_Correo);
        enviar = findViewById(R.id.btnEnviarC);

        enviar.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnEnviarC:
                validarCampos();
                break;
        }
    }

    public void validarCampos(){
        if (usuario.getText().toString().equals("")) {
            usuario.setError("Ingrese su usuario");
            Toast.makeText(getApplicationContext(), "Ingrese su usuario.", Toast.LENGTH_LONG).show();
            usuario.requestFocus();
        } else {
            if (correo.getText().toString().equals("")) {
                correo.setError("Ingrese su correo");
                Toast.makeText(getApplicationContext(), "Ingrese su correo.", Toast.LENGTH_LONG).show();
                correo.requestFocus();
            } else {
                if (Configuration.isEmailValid(correo.getText().toString().trim()) == false) {
                    correo.setError("Correo no valido");
                    Toast.makeText(getApplicationContext(), "Correo no valido.", Toast.LENGTH_LONG).show();
                    correo.requestFocus();
                    correo.setText("");
                } else {

                    //se envia la peticion para restablecer el password.
                    String Usuario = usuario.getText().toString();
                    String Correo = correo.getText().toString();
                    RestablecerPassword(Usuario.trim(),Correo.trim());

                }
            }
        }
    }
    //Debe tener mayusculas, minusculas, números y 8 caracteres

    public void RestablecerPassword(String User, String Email) {

        progressDialog = new ProgressDialog(ResetPassword.this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Restableciendo contraseña...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setProgress(0);
        progressDialog.show();

        final String[] respuesta = {""};

        try {
            RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
            final JSONObject jsonBody = new JSONObject();

            jsonBody.put("username", User);
            jsonBody.put("email", Email);

            final String requestBody = jsonBody.toString();

            //Log.d(Configuration.TAG, "requestBody: " + requestBody);

            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, Configuration.HOST_RESET_PASSWORD, jsonBody, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    //Dialog.cancel();
                    //Log.i("Respuesta: ", response.toString());

                    progressDialog.dismiss();

                    Toast.makeText(getApplicationContext(), "Verifique su correo.", Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(ResetPassword.this, MainActivity.class);
                    ResetPassword.this.finish();
                    startActivity(intent);

                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    try {
                        //Dialog.cancel();
                        progressDialog.dismiss();
                        //Log.d("Login...", error.networkResponse.statusCode + "");

                        int estatus = error.networkResponse.statusCode;
                        String aString = Integer.toString(estatus);//403

                        //Log.d("Codigo...", aString);

                        switch (aString) {
                            case "401":
                                //Log.i("-------case 1-------", "Contraseña o usuario incorrectos.");
                                CharSequence text = "Contraseña o usuario incorrecto.";
                                Toast.makeText(getApplicationContext(), text, Toast.LENGTH_LONG).show();
                                break;

                            case "406":
                                //Log.i("-------case 1-------", "Contraseña o usuario incorrectos.");
                                CharSequence text2 = "Usuario o correo incorrecto.";
                                Toast.makeText(getApplicationContext(), text2, Toast.LENGTH_LONG).show();
                                break;

                            default:
                                //Log.i("-------Default-------", "Error.");
                                CharSequence text3 = "Error de comunicación";
                                Toast.makeText(getApplicationContext(), text3, Toast.LENGTH_LONG).show();
                        }

                    } catch (Exception e) {
                        e.getMessage();
                    }

                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("Content-Type", "application/json");
                    params.put("Api-Function", "recoveryPasswordMobile()");
                    return params;
                }
            };

            requestQueue.add(request);

            requestQueue.addRequestFinishedListener(new RequestQueue.RequestFinishedListener<Object>() {
                @Override
                public void onRequestFinished(Request<Object> request) {
                    requestQueue.getCache().clear();
                }
            });

        } catch (JSONException e) {
            progressDialog.dismiss();
            e.printStackTrace();

            new LovelyStandardDialog(ResetPassword.this, LovelyStandardDialog.ButtonLayout.VERTICAL)
                    .setTopColorRes(R.color.colorAccent)
                    .setButtonsColorRes(R.color.colorPrimaryDark)
                    .setTitle("Yolikan")
                    .setMessage("Error de conexión. " + e.getMessage())
                    .setPositiveButton(android.R.string.ok, new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                        }
                    })
                    //.setNegativeButton(android.R.string.no, null)
                    .show();
        }

    }


}
