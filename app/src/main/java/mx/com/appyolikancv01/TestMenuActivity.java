package mx.com.appyolikancv01;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;


import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.yarolegovich.lovelydialog.LovelyStandardDialog;

import org.jetbrains.annotations.NotNull;

import mx.com.appyolikancv01.bluetooth.BluetoothController;
import mx.com.appyolikancv01.fragments.FragmentServiceActivity;
import mx.com.appyolikancv01.fragments.PageCuenta;
import mx.com.appyolikancv01.fragments.PageFragment;

public class TestMenuActivity extends AppCompatActivity {

    private int tipo;
    BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
    public BluetoothDevice mDevice;
    private String uuid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menu_activity);
        BottomNavigationView bottomNav = findViewById(R.id.bottom_navigation);
        bottomNav.setOnNavigationItemSelectedListener(navListener);
        bottomNav.setSelectedItemId(R.id.listaDispositivos);
        //bottomNav.getMenu().findItem(R.id.temperatura).setEnabled(false);

    }

    private BottomNavigationView.OnNavigationItemSelectedListener navListener = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            Fragment selectedFragment = null;

            switch (item.getItemId()) {
                case R.id.listaDispositivos:
                    Fragment currentFragment=getSupportFragmentManager().findFragmentByTag("temperatura");
                    if (currentFragment==null?false:currentFragment.isVisible()) {
                        new LovelyStandardDialog(TestMenuActivity.this, LovelyStandardDialog.ButtonLayout.VERTICAL)
                                .setTopColorRes(R.color.colorAccent)
                                .setButtonsColorRes(R.color.colorPrimaryDark)
                                //.setIcon(R.drawable.ic_icon_yolikan)
                                .setTitle("Yolikan")
                                .setMessage("Esta acción desconectará el dispositivo Yolikan. ¿Desea continuar?")
                                .setPositiveButton(android.R.string.ok, new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        bluetoothAdapter.disable();
                                        mDevice=null;
                                        uuid=null;
                                        Fragment selectedFragment = new PageFragment();
                                        getSupportFragmentManager().beginTransaction().replace(R.id.container, selectedFragment).addToBackStack(null).commit();
                                    }
                                })
                                .setNegativeButton(android.R.string.no, new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        BottomNavigationView bottomNavigation = findViewById(R.id.bottom_navigation);
                                        bottomNavigation.setSelectedItemId(R.id.temperatura);
                                    }
                                })
                                .show();
                    }else{
                        selectedFragment = new PageFragment();
                        getSupportFragmentManager().beginTransaction().replace(R.id.container, selectedFragment,"dispositivos").addToBackStack(null).commit();
                    }
                    break;
                case R.id.temperatura:

                    selectedFragment = new FragmentServiceActivity();
                    if (!selectedFragment.isVisible()) {
                        if (selectedFragment.isAdded()){
                            getSupportFragmentManager().beginTransaction().show(selectedFragment);
                        }else {
                            Bundle bundle = new Bundle();
                            bundle.putParcelable("device", mDevice);
                            bundle.putString("UUID", uuid);
                            selectedFragment.setArguments(bundle);
                            if (mDevice != null) {
                                getSupportFragmentManager().beginTransaction().replace(R.id.container, selectedFragment,"temperatura").addToBackStack(null).commit();
                            } else {
                                msgVincular();
                                return false;
                            }
                        }
                    }
                    break;
                case R.id.configuracion:
                    selectedFragment = new PageCuenta();
                    getSupportFragmentManager().beginTransaction().replace(R.id.container, selectedFragment,"cuenta").addToBackStack(null).commit();
                    break;
            }

            return true;
        }


    };

    public boolean changeMenu(BluetoothDevice device, String id) {
        this.mDevice = device;
        this.uuid = id;
        BottomNavigationView bottomNavigation = findViewById(R.id.bottom_navigation);
        bottomNavigation.setSelectedItemId(R.id.temperatura);
        bottomNavigation.getMenu().findItem(R.id.temperatura).setEnabled(true);

        return true;
    }

    public boolean changeMenuListDisp() {
        BottomNavigationView bottomNavigation = findViewById(R.id.bottom_navigation);
        bottomNavigation.setSelectedItemId(R.id.listaDispositivos);
        return true;
    }

    private void msgVincular() {
        Toast.makeText(getApplicationContext(), "Es necesario seleccionar un dispositivo YOLIKAN.", Toast.LENGTH_SHORT).show();
    }

}