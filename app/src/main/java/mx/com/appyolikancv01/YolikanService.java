package mx.com.appyolikancv01;

import android.annotation.SuppressLint;
import android.app.IntentService;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;

public class YolikanService extends IntentService implements LocationListener {

    final Handler handler = new Handler();
    Timer timer = new Timer();

    public static final String ACTION_TEMPERATURA = "mx.com.appyolikancv01.intent.action.TEMPERATURA";
    public static final String ACTION_ESTATUS = "mx.com.appyolikancv01.intent.action.ESTATUS";
    public static final String ACTION_TERMINAR_SERVICIO = "mx.com.appyolikancv01.intent.action.TERMINAR_SERVICIO";
    public static final String ACTION_CERRAR_SESION = "mx.com.appyolikancv01.intent.action.CERRAR_SESION";
    public static final String ACTION_ACTUALIZA_REGISTROS = "mx.com.appyolikancv01.intent.action.ACTUALIZA_REGISTROS";
    public static final String ACTION_AVISO_GPS = "mx.com.appyolikancv01.intent.action.AVISO_GPS";//

    private BluetoothDevice mDevice;
    //private UUID mDeviceUUID;
    static BluetoothSocket mBTSocket;
    static boolean mIsBluetoothConnected = false;
    static ReadInput mReadThread = null;
    static boolean mIsUserInitiatedDisconnect = false;

    double temp_format = 0.0;
    Integer counter = 0;

    //tiempo en espera
    String l_cronometro = "";
    MiHandler miHandler;
    String salida;
    int horas, min, seg, micros;
    boolean pausado;

    String nickname_dispositivo = "", id_usuario, personaReg = "", nombrePersonaReg = "", device_name, device_mac;

    // location
    private Location currentLocation;
    FusedLocationProviderClient mFusedLocationClient;
    double current_latitude = 0.0, current_longitude = 0.0;

    //private static Timer timer = new Timer();

    String Preferencias_token = "";
    String Preferencias_datosUsuario = "";

    private UUID mDeviceUUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");

    private enum Actions {
        FINISH, START
    }

    private enum PARAM {
        RESULT_RECEIVER
    }

    public YolikanService(String name) {
        super(name);
    }

    public YolikanService() {
        super("yolikanService");
    }

    @SuppressLint("WrongThread")
    @Override
    protected void onHandleIntent(@Nullable Intent intent) {

        if (intent != null) {
            final String action = intent.getAction();

            if (Actions.START.name().equals(action)) {
                //Log.d(Configuration.TAG, "inicia: " + intent.getAction());

                //b_start();


                mDevice = intent.getParcelableExtra("DEVICE_ID");
                mDeviceUUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");

                device_name = mDevice.getName();
                device_mac = mDevice.getAddress();

                //Log.d(Configuration.TAG, "device start: " + mDevice.getName() + ", " + mDevice.getAddress());

                if (mBTSocket == null || !mIsBluetoothConnected) {
                    new ConnectBT().execute();
                    //Log.d("Yulis", "- " + mIsBluetoothConnected);

                }

                mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
                LocationManager locationManager =
                        (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);

                if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                    ///////showGPSDisabledAlertToUser();
                    Intent bcIntent = new Intent();
                    bcIntent.setAction(ACTION_AVISO_GPS);
                    sendBroadcast(bcIntent);

                } else {
                    startLocationUpdates();
                }
            } else if (Actions.FINISH.name().equals(intent.getAction())) {
                //Log.d(Configuration.TAG, "termina: " + intent.getAction());

                new DisConnectBT().execute();
            }
        }
    }

    private class ConnectBT extends AsyncTask<Void, Void, Void> {
        private boolean mConnectSuccessful = true;

        @Override
        protected void onPreExecute() {
            //Comunicamos el estatus
            Intent bcIntent = new Intent();
            bcIntent.setAction(ACTION_ESTATUS);
            bcIntent.putExtra("estatus", "Conectando...");
            sendBroadcast(bcIntent);
        }

        @Override
        protected Void doInBackground(Void... devices) {

            try {
                if (mBTSocket == null || !mIsBluetoothConnected) {
                    //Log.d("Yulis", "strInput: " + mDeviceUUID);
                    mBTSocket = mDevice.createInsecureRfcommSocketToServiceRecord(mDeviceUUID);
                    BluetoothAdapter.getDefaultAdapter().cancelDiscovery();
                    mBTSocket.connect();
                }
            } catch (IOException e) {
                // Unable to connect to device
                //Log.d(Configuration.TAG, "Disconnect: " + e.getMessage());
                e.printStackTrace();
                mConnectSuccessful = false;
                new DisConnectBT().execute();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

            if (!mConnectSuccessful) {
                Toast.makeText(getApplicationContext(), "Conexión fallida, por favor, compruebe el estado de su dispositivo.", Toast.LENGTH_LONG).show();
                //Log.d("Yulis", "strInput: " + "Conexión fallida, por favor, compruebe el estado de su dispositivo");

                Intent bcIntent = new Intent();
                bcIntent.setAction(ACTION_TERMINAR_SERVICIO);
                bcIntent.putExtra("motivo", "Conexión fallida, por favor, compruebe el estado de su dispositivo.");
                sendBroadcast(bcIntent);

            } else {
                //Log.d("Yulis,","Conectado");
                ///msg("Dispositivo conectado");
                String strMensaje = "", strPersona = "";

                if (nickname_dispositivo.equals("") || nickname_dispositivo == null || nickname_dispositivo.equals("null")) {
                    strMensaje = "Dispositivo conectado " + mDevice.getName();
                } else {
                    strMensaje = "Dispositivo conectado " + mDevice.getName() + " (" + nickname_dispositivo + ")";
                }

                if (!nombrePersonaReg.equals("")) {
                    strPersona = "\nRealizando medición de " + nombrePersonaReg;
                }

                Intent bcIntent = new Intent();
                bcIntent.setAction(ACTION_ESTATUS);
                bcIntent.putExtra("estatus", strMensaje + strPersona);
                sendBroadcast(bcIntent);

                mIsBluetoothConnected = true;
                mReadThread = new ReadInput(); // Kick off input reader
            }

        }
    }

    public class DisConnectBT extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected Void doInBackground(Void... params) {

            if (mReadThread != null) {
                mReadThread.stop();
                while (mReadThread.isRunning()) ; // Wait until it stops
                mReadThread = null;
            }

            try {
                mBTSocket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            mIsBluetoothConnected = false;
            if (mIsUserInitiatedDisconnect) {
                Intent bcIntent = new Intent();
                bcIntent.setAction(ACTION_TERMINAR_SERVICIO);
                bcIntent.putExtra("motivo", "Conexión fallida, por favor, compruebe el estado de su dispositivo.");
                sendBroadcast(bcIntent);
            }
        }
    }

    private class ReadInput implements Runnable {

        private boolean bStop = false;
        private Thread t;

        public ReadInput() {
            t = new Thread(this, "Input Thread");
            t.start();
        }

        public boolean isRunning() {
            return t.isAlive();
        }

        @Override
        public void run() {
            InputStream inputStream;
            try {

                Thread.sleep(20000);
                inputStream = mBTSocket.getInputStream();

                //Log.d("*****YULIS*****", "------" +  mBTSocket.getInputStream());



                while (true) {

                    byte[] buffer = new byte[256];
                    if (inputStream.available() > 0) {
                        inputStream.read(buffer);
                        int i = 0;
                        for (i = 0; i < buffer.length && buffer[i] != 0; i++) {
                        }

                        String strInput = new String(buffer, 0, i);

                        //Log.d(Configuration.TAG, "strInput temp: " + strInput);

                        if (strInput.contains(",")) {

                            try {
                                String valueTemp = "";
                                String valueTemperatura = strInput.replace(",", "");
                                //Log.d(Configuration.TAG, "valueTemperatura: " + valueTemperatura);

                                if(valueTemperatura.contains("t")){

                                    valueTemp = valueTemperatura.replace("t", "");

                                } else {
                                    valueTemp = strInput.replace(",", "");
                                }

                                //Log.d(Configuration.TAG, "valueTemp: " + valueTemp);

                                temp_format = Float.parseFloat(valueTemp);

                                //Log.d(Configuration.TAG, "temp: " + temp_format);

                                //se valida que se tenga dato
                                if (temp_format > 0) {
                                    Intent bcIntent = new Intent();
                                    bcIntent.setAction(ACTION_TEMPERATURA);

                                    bcIntent.putExtra("temperatura", String.valueOf(temp_format));
                                    sendBroadcast(bcIntent);

                                }

                            } catch (NumberFormatException e) {
                                ///temperatura = "- -";
                            }

                        } else {

                            //Log.d(Configuration.TAG, "comma");
                        }
                    }
                    ///Thread.sleep(100);
                }
            }
            //catch (IOException | InterruptedException e) {
            catch (IOException | InterruptedException e) {
                Toast.makeText(mx.com.appyolikancv01.YolikanService.this, "Se perdió la conexión, vuelve a emparejar con el dispositivo.", Toast.LENGTH_SHORT).show();

                Intent bcIntent = new Intent();
                bcIntent.setAction(ACTION_TERMINAR_SERVICIO);
                bcIntent.putExtra("motivo", "Se perdió la conexión, vuelve a emparejar con el dispositivo.");
                sendBroadcast(bcIntent);
            }
        }

        public void stop() {
            bStop = true;
        }
    }


    public void guardaRegistro(double temp) {
        //Log.d("Yulis", "guardaRegistro: ");
        String temperature = String.format("%.1f", temp);
        //Log.d(Configuration.TAG, "Guardando... " + temperature);
        String datos_token = CargarToken();
        String datosUser = CargarDatosUsuario();
        //Log.d(Configuration.TAG, "Token... " + datos_token);

        final String[] respuesta = new String[1];

        try {

            //cargamos datos del usuario que tiene sesion activa
            JSONObject prueba = new JSONObject(datosUser);
            //Log.i("Yulis---", prueba.getString("auth_user_data"));

            JSONObject messageJsonPrueba = new JSONObject(prueba.getString("auth_user_data").toString());

            //auth_user_id = messageJsonPrueba.getString("auth_user_id");
            String username = messageJsonPrueba.getString("username");
            String first_name = messageJsonPrueba.getString("first_name");
            String last_name = messageJsonPrueba.getString("last_name");
            RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());

            final JSONObject jsonBody = new JSONObject();

            if (personaReg.equals("")) {
                personaReg = "0";
            }

            /**
             * {
             *   "device":"dev0001",
             * 	"measurement":32.00,
             * 	"gps_coordinates":"20.174610115203176, -100.13677041713382",
             * 	"patient_name":"Maria Yuliana Mata Corchado"
             * }
             *
             */

            jsonBody.put("device", device_name);
            jsonBody.put("measurement", temperature);
            jsonBody.put("gps_coordinates", "20.174610115203176, -100.13677041713382");
            jsonBody.put("patient_name", first_name + " " + last_name);

            final String requestBody = jsonBody.toString();
            //Log.d(Configuration.TAG, "body guarda reg: " + requestBody);

            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, Configuration.HOST_RegistroMedicion, jsonBody, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    //Dialog.cancel();
                    //Log.i("Respuesta: ", response.toString());

                    //Log.i(Configuration.TAG, "VOLLEY response guarda medicion: " + response);

                    counter = 0;

                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    try {
                        //Dialog.cancel();

                        //Log.d("Login...", error.networkResponse.statusCode + "");

                        int estatus = error.networkResponse.statusCode;
                        String aString = Integer.toString(estatus);//403

                        //Log.d("Codigo...", aString);

                        switch (aString) {
                            case "401":
                                //Log.i("-------case 1-------", "Contraseña o usuario incorrectos.");
                                CharSequence text = "Contraseña o usuario incorrecto.";
                                Toast.makeText(getApplicationContext(), text, Toast.LENGTH_LONG).show();
                                break;

                            /*case "403":
                                //Log.i("-------case 2-------", "Sesión iniciada en otro dispositivo.");
                                CharSequence text1 = "Sesión iniciada en otro dispositivo.";
                                Toast.makeText(getApplicationContext(), text1, Toast.LENGTH_SHORT).show();
                                break;*/

                            default:
                                //Log.i("-------Default-------", "Error.");
                                CharSequence text2 = "Error de comunicación";
                                Toast.makeText(getApplicationContext(), text2, Toast.LENGTH_LONG).show();
                        }

                    } catch (Exception e) {
                        e.getMessage();
                    }

                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("Content-Type", "application/json");
                    params.put("Api-Function", "setItemMobile()");
                    params.put("Authentication-Token", Preferencias_token);
                    //params.put("api-parameters","{}");
                    return params;
                }
            };
            requestQueue.add(request);

//            StringRequest stringRequest = new StringRequest(Request.Method.POST, Configuration.HOST_RegistroMedicion, new Response.Listener<String>() {
//                @Override
//                public void onResponse(String response) {
//                    try {
//                        jsonObj = new JSONObject(response);
//                        JsonResponse = response;
//
//                        //Log.i(Configuration.TAG, "VOLLEY response guarda medicion: " + response);
//
//                        counter = 0;
//
//                       /*
//                        bcIntent.setAction(ACTION_ACTUALIZA_REGISTROS);
//                        bcIntent.putExtra("actualiza", "registros");
//                        sendBroadcast(bcIntent);*/
//
//                    } catch (JSONException e) {
//                        counter = 0;
//                        //Log.d(Configuration.TAG, "Exception mediciones: " + e.getMessage());
//                        e.printStackTrace();
//                    }
//                }
//
//            }, new Response.ErrorListener() {
//                @Override
//                public void onErrorResponse(VolleyError error) {
//                    //Log.d(Configuration.TAG, "Error de conexión Guarda Medicion " + error.toString());
//                    //////finish();
//                    Intent bcIntent = new Intent();
//                    bcIntent.setAction(ACTION_TERMINAR_SERVICIO);
//                    bcIntent.putExtra("motivo", "Error de conexión al Guardar Medicion ");
//                    sendBroadcast(bcIntent);
//                }
//            }) {
//                @Override
//                public String getBodyContentType() {
//                    return "application/json; charset=utf-8";
//                }
//
//                @Override
//                public byte[] getBody() throws AuthFailureError {
//                    try {
//                        return requestBody == null ? null : requestBody.getBytes("utf-8");
//                    } catch (UnsupportedEncodingException uee) {
//                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
//                        return null;
//                    }
//                }
//            };
//
//            requestQueue.add(stringRequest);
//            requestQueue.addRequestFinishedListener(new RequestQueue.RequestFinishedListener<Object>() {
//                @Override
//                public void onRequestFinished(Request<Object> request) {
//                    requestQueue.getCache().clear();
//                }
//            });

        } catch (JSONException e) {
            counter = 0;
            //Log.e(Configuration.TAG, "Error mediciones! " + e.getMessage());
            e.printStackTrace();
            counter = 0;
        }
    }

    @SuppressWarnings("MissingPermission")
    private void startLocationUpdates() {
        mFusedLocationClient
                .getLastLocation()
                .addOnCompleteListener(
                        new OnCompleteListener<Location>() {
                            @Override
                            public void onComplete(@NonNull Task<Location> task) {
                                Location location = task.getResult();
                                if (location == null) {
                                    requestNewLocationData();
                                } else {
                                    current_latitude = location.getLatitude();
                                    current_longitude = location.getLongitude();
                                    //Log.d(Configuration.TAG, "onComplete");
                                }
                            }
                        });

    }

    @SuppressLint("MissingPermission")
    private void requestNewLocationData() {

        // Initializing LocationRequest
        // object with appropriate methods
        LocationRequest mLocationRequest
                = new LocationRequest();
        mLocationRequest.setPriority(
                LocationRequest
                        .PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(5);
        mLocationRequest.setFastestInterval(0);
        mLocationRequest.setNumUpdates(1);

        // setting LocationRequest
        // on FusedLocationClient
        mFusedLocationClient
                = LocationServices
                .getFusedLocationProviderClient(this);

        mFusedLocationClient
                .requestLocationUpdates(
                        mLocationRequest,
                        mLocationCallback,
                        Looper.getMainLooper());
    }

    private LocationCallback mLocationCallback = new LocationCallback() {

        @Override
        public void onLocationResult(LocationResult locationResult) {
            /*Location mLastLocation = locationResult
                    .getLastLocation();
            current_latitude = mLastLocation.getLatitude();
            current_longitude = mLastLocation.getLongitude();*/

            currentLocation = locationResult.getLocations().get(0);

            //Log.d(Configuration.TAG, "onLocationResult: " + locationResult.toString());
        }
    };

    @Override
    public void onLocationChanged(Location location) {
        //Log.d(Configuration.TAG, "onLocationChanged");

        // New location has now been determined
        String msg = "Updated Location: " +
                Double.toString(location.getLatitude()) + "," +
                Double.toString(location.getLongitude());
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
        // You can now create a LatLng Object for use with maps
        ///LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());

        current_latitude = location.getLatitude();
        current_longitude = location.getLatitude();
        //Log.d(Configuration.TAG, "on location changed:" + location.getLatitude() + ", Longitude:" + location.getLongitude());

    }

    public static void killService(Context context, YolikanResultReceiver.ResultReceiverCallBack resultReceiverCallBack) {
        YolikanResultReceiver yolikanResultReceiver = new YolikanResultReceiver(new Handler(context.getMainLooper()));
        yolikanResultReceiver.setReceiver(resultReceiverCallBack);

        //Log.d(Configuration.TAG, "finishService");

        Intent intent = new Intent(context, mx.com.appyolikancv01.YolikanService.class);
        intent.setAction(Actions.FINISH.name());
        intent.putExtra(PARAM.RESULT_RECEIVER.name(), yolikanResultReceiver);
        context.startService(intent);
    }

    private String CargarToken() {
        SharedPreferences preferencias = getSharedPreferences("token", Context.MODE_PRIVATE);
        String Servicio = preferencias.getString("DatosToken", "No hay token");
        Preferencias_token = Servicio;
        return Servicio;
    }

    private String CargarDatosUsuario() {
        SharedPreferences preferencias = getSharedPreferences("usuario", Context.MODE_PRIVATE);
        String Servicio = preferencias.getString("DatosUsuario", "No hay datos");
        Preferencias_datosUsuario = Servicio;
        return Servicio;
    }


    //Boton de Start
    public void b_start() {
        if(pausado) {
            synchronized (this) {
                pausado = false;
                //parar.setEnabled(true);
                //inio.setEnabled(false);
                cronopost();
                this.notifyAll();
            }
        }
        else{
            //parar.setEnabled(true);
            //inio.setEnabled(false);
            cronopost();

        }
    }

    public void b_pausa(){
        pausado=true;
        //parar.setEnabled(false);
        //inio.setEnabled(true);
    }


    public void cronopost(){
        Thread th = new Thread(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    if (pausado) {
                        synchronized (this) {
                            try {
                                this.wait();
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                    espera(10);

                    if (!pausado) {
                        Message msg = new Message();
                        msg.what=1;
                        miHandler.sendMessage(msg);

                    }
                }
            }
        });
        th.start();

    }

    public void crono(){
        micros++;
        if(micros == 100){
            seg++;
            micros =0;
        }
        if (seg == 60) {
            min++;
            seg = 0;
        }
        if (min == 60) {
            min = 0;
            horas++;
        }
    }

    public String formato(){
        salida="";
        if (horas <= 9) {
            salida += "0";
        }
        salida += horas;
        salida += ":";
        if (min <= 9) {
            salida += "0";
        }
        salida += min;
        salida += ":";
        if (seg <= 9) {
            salida += "0";
        }
        salida += seg;
        //Log.d("Yulis", "cronometro: " + l_cronometro);
        /*salida += seg + ":" ;
        if(micros<=9){
            salida+="0";
        }
        salida+=micros;*/

        return salida;

    }

    static public void espera(int e) {
        try {
            Thread.sleep(e);
        } catch (InterruptedException ex) {
        }
    }


    static class MiHandler extends Handler {
        YolikanService ma;

        MiHandler(YolikanService ma ){
            this.ma = ma;
        }

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch( msg.what){
                case 1:
                    ma.crono();
                    //Formato de la salida:
                    String salida = ma.formato();
                    //Log.d("Yulis", "cronometro: " + ma.l_cronometro);
                    //ma.l_cronometro.setText(salida);
                    break;
                default:
                    break;
            }
        }
    }

}