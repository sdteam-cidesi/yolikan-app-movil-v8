package mx.com.appyolikancv01;

import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.yarolegovich.lovelydialog.LovelyStandardDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.ref.WeakReference;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import mx.com.appyolikancv01.fragments.PageFragment;

public class prueba {
}
//package mx.com.appyolikancv01.fragments;
//
//        import android.bluetooth.BluetoothDevice;
//        import android.content.BroadcastReceiver;
//        import android.content.Context;
//        import android.content.DialogInterface;
//        import android.content.Intent;
//        import android.content.IntentFilter;
//        import android.content.SharedPreferences;
//        import android.os.Bundle;
//        import android.provider.Settings;
//        import android.util.Log;
//        import android.view.LayoutInflater;
//        import android.view.View;
//        import android.view.ViewGroup;
//        import android.widget.ImageView;
//        import android.widget.LinearLayout;
//        import android.widget.ScrollView;
//        import android.widget.TableLayout;
//        import android.widget.TextView;
//        import android.widget.Toast;
//        import android.widget.ToggleButton;
//
//        import androidx.annotation.NonNull;
//        import androidx.annotation.Nullable;
//        import androidx.appcompat.app.AlertDialog;
//        import androidx.core.content.ContextCompat;
//        import androidx.fragment.app.Fragment;
//        import androidx.fragment.app.FragmentActivity;
//        import androidx.fragment.app.FragmentManager;
//        import androidx.fragment.app.FragmentTransaction;
//        import androidx.localbroadcastmanager.content.LocalBroadcastManager;
//
//        import com.android.volley.AuthFailureError;
//        import com.android.volley.Request;
//        import com.android.volley.RequestQueue;
//        import com.android.volley.Response;
//        import com.android.volley.VolleyError;
//        import com.android.volley.toolbox.JsonObjectRequest;
//        import com.android.volley.toolbox.Volley;
//        import com.google.android.material.bottomnavigation.BottomNavigationView;
//        import com.yarolegovich.lovelydialog.LovelyStandardDialog;
//
//        import org.json.JSONException;
//        import org.json.JSONObject;
//
//        import java.lang.ref.WeakReference;
//        import java.text.DateFormat;
//        import java.util.ArrayList;
//        import java.util.Date;
//        import java.util.HashMap;
//        import java.util.Map;
//        import java.util.UUID;
//
//        import mx.com.appyolikancv01.Configuration;
//        import mx.com.appyolikancv01.ListaDispositivosActivity;
//        import mx.com.appyolikancv01.MainActivity;
//        import mx.com.appyolikancv01.R;
//        import mx.com.appyolikancv01.ServiceActivity;
//        import mx.com.appyolikancv01.TestMenuActivity;
//        import mx.com.appyolikancv01.YolikanResultReceiver;
//        import mx.com.appyolikancv01.YolikanService;
//
//public class FragmentServiceActivity extends Fragment implements View.OnClickListener {
//
//    String Preferencias_datosUsuario = "";
//    String auth_user_id = "";
//
//    TextView txtEspera;
//    TextView grados;
//
//    TextView activity_chat_status;
//    TextView tv_temperatura;
//    //TextView btn_todas_temp;
//    //TextView btn_ayuda;
//    TextView tv_mensaje_temp;
//    //TextView tv_mensaje_emergencia;
//    TextView tv_usuario;
//    TextView tv_misMediciones;
//    ScrollView sv;
//
//    LinearLayout llayout;
//
//    ToggleButton tg_actualizar;
//
//    ImageView btn_config, btn_close, btn_salir;
//    Integer counter = 0;
//    double temp_format = 0.0;
//    ArrayList notas = new ArrayList();
//
//    String Preferencias_token = "";
//
//    View tempView;
//
//    String id_usuario, temperatura, device_name, device_mac, actualizar = "SI", personaReg = "", nickname_dispositivo = "", nombrePersonaReg = "";
//    JSONObject jsonObj;
//    String JsonResponse = null;
//
//    // Table Layout
//    TableLayout tableLayout;
//    Context context;
//
//    String id_registro, fecha, tempRecord, dispositivo;
//
//    // Service
//    mx.com.appyolikancv01.fragments.FragmentServiceActivity.YolikanReceiver receiver;
//    private BluetoothDevice mDevice;
//    private UUID mDeviceUUID;
//    String Preferencias_device = "";
//
//    // connect to service
//
//    //tiempo en espera
//    String l_cronometro = "";
//    String salida;
//    int horas, min, seg, micros;
//    boolean pausado;
//
//    //tiempo en espera para enviar medicion
//    String l_cronometro_server = "";
//    String salida_server;
//    int horas_server, min_server, seg_server, micros_server;
//    boolean pausado_server;
//
//
//    boolean tempPrimera = true;
//
//    private enum Actions {
//        START
//    }
//
//    private View view;
//
//    @Override
//    public void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//
//        if (getArguments() != null) {
//
//            mDevice = getArguments().getParcelable("device"); //getActivity().getIntent().getParcelableExtra("device");
//            device_name = mDevice.getName();
//            Log.d("Yulis_Device", mDevice.toString());
//        }
//
//
//        //mDevice = getArguments().getParcelable("device");
//
//        /*try {
//            if (mDevice == null) {
//                Log.d("Yulis_Device", "Es null");
//                mDevice = getArguments().getParcelable("device"); //getActivity().getIntent().getParcelableExtra("device");
//                device_name = mDevice.getName();
//                Log.d("Yulis_Device", mDevice.toString());
//            }
//
//        } catch (Exception e) {
//            e.printStackTrace();
//            Log.d("Yulis","Entro al catch");
//        }*/
//
//        // Check whether we're recreating a previously destroyed instance
//        if (savedInstanceState != null) {
//            // Restore value of members from saved state
//            Log.d("Yulis_Device", "Restore value of members from saved state");
//        } else {
//            // Probably initialize members with default values for a new instance
//            Log.d("Yulis_Device", "Probably initialize members with default values for a new instance");
//        }
//
//    }
//
//    @Nullable
//    @Override
//    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//
//        view = inflater.inflate(R.layout.activity_visor_temperatura, container, false);
//
//        //obtenemos el device
//
//        //mDevice = getArguments().getParcelable("device");
//        //Log.d("Yulis", mDevice.toString());
//        //mDevice = deviceP; // getActivity().getIntent().getParcelableExtra("device");
//        mDeviceUUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
//
//        //((TestMenuActivity) getActivity()).changeMenu();
//
//        grados = view.findViewById(R.id.grados);
//        txtEspera = view.findViewById(R.id.txtEspera);
//        txtEspera.setVisibility(View.GONE);
//
//
//        btn_close = view.findViewById(R.id.imageView);
//        btn_config = view.findViewById(R.id.btn_config);
//        tv_temperatura = view.findViewById(R.id.tv_temperatura);
//
//        tv_mensaje_temp = view.findViewById(R.id.tv_mensaje_temp);
//        tv_usuario = view.findViewById(R.id.tv_usuario);
//        llayout = view.findViewById(R.id.liyimag);
//
//        tempView = view.findViewById(R.id.constraint_layout);
//        tempView.setBackgroundColor(ContextCompat.getColor(this.getActivity(), R.color.fondo));
//
//        btn_config.setOnClickListener(this);
//
//
//        //cargamos datos del usuario que tiene sesion activa
//        String datosUser = CargarDatosUsuario();
//        Log.i("Yulis---", datosUser);
//
//        try {
//
//            JSONObject DatosUsuario = new JSONObject(datosUser);
//            Log.i("Yulis---", DatosUsuario.getString("auth_user_data"));
//
//            JSONObject messageJsonPrueba = new JSONObject(DatosUsuario.getString("auth_user_data").toString());
//
//            auth_user_id = messageJsonPrueba.getString("auth_user_id");
//            String first_name = messageJsonPrueba.getString("first_name");
//            String last_name = messageJsonPrueba.getString("last_name");
//
//            tv_usuario.setText(first_name + " " + last_name);
//
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//
//
//        l_cronometro = "00:00:00";
//        l_cronometro_server = "00:00:00";
//        tempPrimera = true;
//
//        tv_mensaje_temp.setText("");
//
//        txtEspera.setVisibility(View.VISIBLE);
//        tv_temperatura.setVisibility(View.GONE);
//        grados.setVisibility(View.GONE);
//        tv_mensaje_temp.setText("- -");
//
//
//        horas = 0;
//        min = 0;
//        seg = 0;
//        micros = 0;
//        pausado = false;
//
//        horas_server = 0;
//        min_server = 0;
//        seg_server = 0;
//        micros_server = 0;
//        pausado_server = false;
//
//        b_start_server();
//        //b_start();
//
//        context = getActivity().getApplicationContext();
//
//
//        Intent yolikanIntent = new Intent(getActivity(), YolikanService.class);
//        yolikanIntent.setAction(mx.com.appyolikancv01.fragments.FragmentServiceActivity.Actions.START.name());
//        yolikanIntent.putExtra("DEVICE_ID", mDevice);
//        getActivity().startService(yolikanIntent);
//
//        IntentFilter filter = new IntentFilter();
//        filter.addAction(YolikanService.ACTION_TEMPERATURA);
//        filter.addAction(YolikanService.ACTION_ESTATUS);
//        mx.com.appyolikancv01.fragments.FragmentServiceActivity.YolikanReceiver rcv = new mx.com.appyolikancv01.fragments.FragmentServiceActivity.YolikanReceiver();
//        getActivity().registerReceiver(rcv, filter);
//
//
//        return view;
//    }
//
//    @Override
//    public void onClick(View view) {
//        switch (view.getId()) {
//            case R.id.imageView:
//                //terminarLectura();
//                break;
//            case R.id.btn_config:
//                terminarLectura();
//                break;
//        }
//    }
//
////    public class YolikanReceiver extends BroadcastReceiver {
////
////        @Override
////        public void onReceive(Context context, Intent intent) {
////
////            Log.d("Yulis", "action: " + intent.getAction());
////            Log.d("Yulis", "Actualiza temperatura");
////            //start();
////
////            if (intent.getAction().equals(YolikanService.ACTION_ESTATUS)) {
////                String estatus = intent.getStringExtra("estatus");
////                //activity_chat_status.setText(estatus);
////            } else if (intent.getAction().equals(YolikanService.ACTION_TEMPERATURA)) {
////
////                String temperatura = intent.getStringExtra("temperatura");
////
////                assert temperatura != null;
////                Log.d("Yulis", "temp actualiza lista: " + temperatura);
////                if (temperatura.equals("actualiza")) {
////                    Log.d("Yulis", "temp actualiza lista: " + temperatura);
////                    //showRegisters2();
////                } else {
////
////                    Log.d("Yulis", "l_cronometro: " + l_cronometro);
////
////                    Log.d("Yulis", "Temperatura_: " + String.format("%.1f", Double.valueOf(temperatura)));
////
////                    //tiempo de espera para mostrar temperatura en pantalla
////                    if (l_cronometro.equals(Configuration.TIME)) {
////
////                        counter++;
////                        //validamos primera terperatura
////                        if (tempPrimera == true) {
////
////                            Log.d("CIDESI", "tempPrimera: " + tempPrimera);
////                            String currentDateTimeString = DateFormat.getDateTimeInstance().format(new Date());
////                            System.out.println("**** Tiempo primer medicion:" + currentDateTimeString);
////                            Log.d("CIDESI", "contador_yulis: " + counter);
////
////
////                            notas.add(Float.parseFloat(temperatura));
////                            //contador
////                            if (counter == Configuration.INTERVALO_PROMEDIO) {
////                                tempPrimera = false;
////                                Log.d("CIDESI", "Se guarda la medicion");
////                                //hacemos el promedio
////                                Log.d("******", "*********************");
////
////                                double n = 0;
////                                double promedio = 0;
////                                for (int a = 0; a < notas.size(); a++) {
////                                    n += Double.parseDouble(notas.get(a).toString());
////                                    promedio = n / notas.size();
////                                }
////
////                                System.out.println("El promedio de los valores del ArrayList es: " + n / notas.size());
////                                System.out.println("El medicion promedio a enviar: " + promedio);
////                                System.out.println("valor n" + n);
////                                System.out.println("size: " + notas.size());
////                                System.out.println("contador: " + counter);
////
////                                //limpiamos la temperatura anterior de las propertys
////                                ClearDatoTemperatura();
////                                //guardamos la primer temperatura registrada propertys
////                                GuardarTemperatura(String.valueOf(promedio));
////                                //guardar registro en el server si la temperatura es mayor a 33
////                                if (promedio >= 33.0) {
////
////                                    //no enviamos dato al server
////                                    System.out.println("Enviamos dato al server");
////
////                                    guardaRegistro(promedio);
////
////                                }
////
////                                //iniciamos el siguiente el tiempo de una hora para enviar la medicion
////                                b_start_server();
////
////                                notas.clear();
////
////                                Log.d("******", "*********************");
////
////                                counter = 0;
////
////                            }
////                        }
////                        //Log.d("Yulis", "l_cronometro_server: " + l_cronometro_server);
////                        //validamos si el tiempo de envio ya llego
////                        if (l_cronometro_server.equals(Configuration.TIME_SEND)) {
////
////                            String currentDateTimeString = DateFormat.getDateTimeInstance().format(new Date());
////                            System.out.println("**** Tiempo segunda medicion:" + currentDateTimeString);
////
////                            Log.d("CIDESI", "contador_yulis: " + counter);
////                            //counter++;
////
////                            //almacenamos los 9 datos para hacer el promedio de la temperatura
////                            notas.add(Float.parseFloat(temperatura));
////                            //contador
////                            if (counter == Configuration.INTERVALO_PROMEDIO) {
////                                Log.d("CIDESI", "Se guarda la medicion");
////                                //hacemos el promedio
////                                Log.d("******", "*********************");
////
////                                double n = 0;
////                                double promedio = 0;
////                                for (int a = 0; a < notas.size(); a++) {
////                                    n += Double.parseDouble(notas.get(a).toString());
////                                    promedio = n / notas.size();
////                                }
////
////                                System.out.println("El promedio de los valores del ArrayList es: " + n / notas.size());
////                                System.out.println("El medicion promedio a enviar: " + promedio);
////                                System.out.println("valor n" + n);
////                                System.out.println("size: " + notas.size());
////                                System.out.println("contador: " + counter);
////
////                                //limpiamos la temperatura anterior de las propertys
////                                ClearDatoTemperatura();
////                                //guardamos la temperatura registrada en las propertys
////                                GuardarTemperatura(String.valueOf(promedio));
////                                //guardar registro en el server si la temperatura es mayor a 33
////                                if (promedio >= 33.0) {
////
////                                    //no enviamos dato al server
////                                    System.out.println("Enviamos dato al server");
////
////                                    guardaRegistro(promedio);
////
////                                }
////                                //no es la primer temperatura
////                                tempPrimera = false;
////                                //iniciamos el siguiente el tiempo de una hora para enviar la medicion
////                                l_cronometro_server = "00:00:00";
////                                horas_server = 0;
////                                min_server = 0;
////                                seg_server = 0;
////                                micros_server = 0;
////                                pausado_server = false;
////
////                                b_start_server();
////
////                                notas.clear();
////
////                                Log.d("******", "*********************");
////
////                                counter = 0;
////
////                            }
////
////
////                        } else {
////                            //validamos que las temperaturas tengan cambios muy drasticos
////
////                            Log.d("CIDESI", "contador_yulis: " + counter);
////                            //counter++;
////
////                            //almacenamos los 9 datos para hacer el promedio de la temperatura
////                            notas.add(Float.parseFloat(temperatura));
////                            //contador
////                            if (counter == Configuration.INTERVALO_PROMEDIO) {
////                                Log.d("CIDESI", "Se guarda la medicion");
////                                //hacemos el promedio
////                                Log.d("******", "*********************");
////
////                                double n = 0;
////                                double promedio = 0;
////                                for (int a = 0; a < notas.size(); a++) {
////                                    n += Double.parseDouble(notas.get(a).toString());
////                                    promedio = n / notas.size();
////                                }
////
////                                System.out.println("El promedio de los valores del ArrayList es: " + n / notas.size());
////                                System.out.println("El medicion promedio a enviar: " + promedio);
////                                System.out.println("valor n" + n);
////                                System.out.println("size: " + notas.size());
////                                System.out.println("contador: " + counter);
////
////                                if (promedio >= 20.0 && promedio <= 30.0) {
////
////                                    //no enviamos dato al server
////                                    System.out.println("No enviamos dato al server");
////
////
////                                }
////                                if (promedio >= 33.0 && promedio <= 35.0) {
////
////                                    String TemperaturaAnterior = CargarTemperatura();
////
////                                    System.out.println("Temperatura Anterior: " + TemperaturaAnterior);
////                                    System.out.println("Temperatura Actual: " + promedio);
////                                    System.out.println("diferencia: " + (promedio - Double.valueOf(TemperaturaAnterior)));
////
////                                    if (promedio >= Double.valueOf(TemperaturaAnterior)) {
////
////                                        System.out.println("El promedio es mayor al anterior");
////
////                                        double rango = promedio - Double.valueOf(TemperaturaAnterior);
////
////                                        if (rango >= 1) {
////                                            //enviamos la medicion al server inmediatamente
////
////                                            //limpiamos la temperatura anterior de las propertys
////                                            ClearDatoTemperatura();
////                                            //guardamos la temperatura registrada en las propertys
////                                            GuardarTemperatura(String.valueOf(promedio));
////                                            //guardar registro en el server
////                                            guardaRegistro(promedio);
////                                            //no es la primer temperatura
////                                            tempPrimera = false;
////                                            //reiniciamos el tiempo porque hubo un cambio importante
////                                            l_cronometro_server = "00:00:00";
////                                            horas_server = 0;
////                                            min_server = 0;
////                                            seg_server = 0;
////                                            micros_server = 0;
////                                            pausado_server = false;
////
////                                            b_start_server();
////
////                                        } else {
////                                            //esperamos a que se cumpla el tiempo de 1 hora
////                                        }
////                                    }
////                                }
////
////                                if (promedio >= 35.1 && promedio <= 38.0) {
////
////                                    String TemperaturaAnterior = CargarTemperatura();
////
////                                    if (promedio >= Double.valueOf(TemperaturaAnterior)) {
////
////                                        double rango = promedio - Double.valueOf(TemperaturaAnterior);
////
////                                        if (rango >= 0.5) {
////                                            //enviamos la medicion al server inmediatamente
////
////                                            //limpiamos la temperatura anterior de las propertys
////                                            ClearDatoTemperatura();
////                                            //guardamos la temperatura registrada en las propertys
////                                            GuardarTemperatura(String.valueOf(promedio));
////                                            //guardar registro en el server
////                                            guardaRegistro(promedio);
////                                            //no es la primer temperatura
////                                            tempPrimera = false;
////                                            //reiniciamos el tiempo porque hubo un cambio importante
////                                            l_cronometro_server = "00:00:00";
////                                            horas_server = 0;
////                                            min_server = 0;
////                                            seg_server = 0;
////                                            micros_server = 0;
////                                            pausado_server = false;
////
////                                            b_start_server();
////
////                                        } else {
////                                            //esperamos a que se cumpla el tiempo de 1 hora
////                                        }
////                                    }
////                                }
////
////                                if (promedio >= 38.1) {
////
////                                    String TemperaturaAnterior = CargarTemperatura();
////
////                                    if (promedio >= Double.valueOf(TemperaturaAnterior)) {
////
////                                        double rango = promedio - Double.valueOf(TemperaturaAnterior);
////
////                                        if (rango >= 0.2) {
////                                            //enviamos la medicion al server inmediatamente
////
////                                            //limpiamos la temperatura anterior de las propertys
////                                            ClearDatoTemperatura();
////                                            //guardamos la temperatura registrada en las propertys
////                                            GuardarTemperatura(String.valueOf(promedio));
////                                            //guardar registro en el server
////                                            guardaRegistro(promedio);
////                                            //no es la primer temperatura
////                                            tempPrimera = false;
////                                            //reiniciamos el tiempo porque hubo un cambio importante
////                                            l_cronometro_server = "00:00:00";
////                                            horas_server = 0;
////                                            min_server = 0;
////                                            seg_server = 0;
////                                            micros_server = 0;
////                                            pausado_server = false;
////
////                                            b_start_server();
////
////                                        } else {
////                                            //esperamos a que se cumpla el tiempo de 1 hora
////                                        }
////                                    }
////                                }
////
////                                notas.clear();
////
////                                Log.d("******", "*********************");
////
////                                counter = 0;
////
////                            }
////                        }
////
////                    }
////
////                    //tiempo de espera para mostrar temperatura en pantalla
////                    if (l_cronometro.equals(Configuration.TIME)) {
////
////                        //guardamos la temperatura inicial despues de haber transcurrido el tiempo de espera
////                        //GuardarTemperatura(String.format("%.1f", Double.valueOf(temperatura)));
////
////                        txtEspera.setVisibility(View.GONE);
////                        tv_temperatura.setVisibility(View.VISIBLE);
////                        grados.setVisibility(View.VISIBLE);
////
////                        tv_temperatura.setText(String.format("%.1f", Double.valueOf(temperatura)));
////
////                        temp_format = Float.parseFloat(temperatura);
////
////                        if (temp_format <= 33.0) {
////
////                            txtEspera.setVisibility(View.VISIBLE);
////                            tv_temperatura.setVisibility(View.GONE);
////                            grados.setVisibility(View.GONE);
////
////                            tempView.setBackgroundColor(ContextCompat.getColor(getActivity().getApplicationContext(), R.color.fondo));
////                            //llayout.setBackgroundColor(getResources().getColor(R.color.colorId));
////                            llayout.setBackground(getResources().getDrawable(R.drawable.temp_baja));
////                            tv_mensaje_temp.setText("- -");
////                            //tv_mensaje_emergencia.setVisibility(View.INVISIBLE);
////                        }
////
////                        if (temp_format >= 33.1 && temp_format < 36.1) {
////                            txtEspera.setVisibility(View.GONE);
////                            tv_temperatura.setVisibility(View.VISIBLE);
////                            grados.setVisibility(View.VISIBLE);
////
////                            tempView.setBackgroundColor(ContextCompat.getColor(getActivity().getApplicationContext(), R.color.fondo));
////                            //llayout.setBackgroundColor(getResources().getColor(R.color.colorId));
////                            llayout.setBackground(getResources().getDrawable(R.drawable.temp_baja));
////                            tv_mensaje_temp.setText("Temperatura Baja");
////                            //tv_mensaje_emergencia.setVisibility(View.INVISIBLE);
////                        }
////
////                        if (temp_format >= 36.2 && temp_format < 37.2) {
////                            txtEspera.setVisibility(View.GONE);
////                            tv_temperatura.setVisibility(View.VISIBLE);
////                            grados.setVisibility(View.VISIBLE);
////
////                            tempView.setBackgroundColor(ContextCompat.getColor(getActivity().getApplicationContext(), R.color.fondo));
////                            llayout.setBackground(getResources().getDrawable(R.drawable.tem_normal));
////                            tv_mensaje_temp.setText("Temperatura Normal");
////                            //tv_mensaje_emergencia.setVisibility(View.INVISIBLE);
////                        }
////                        if (temp_format >= 37.3 && temp_format <= 38.9) {
////                            txtEspera.setVisibility(View.GONE);
////                            tv_temperatura.setVisibility(View.VISIBLE);
////                            grados.setVisibility(View.VISIBLE);
////
////                            tempView.setBackgroundColor(ContextCompat.getColor(getActivity().getApplicationContext(), R.color.fondo));
////                            llayout.setBackground(getResources().getDrawable(R.drawable.temp_media));
////                            tv_mensaje_temp.setText("Temperatura Alta");
////                            //tv_mensaje_emergencia.setVisibility(View.INVISIBLE);
////                        }
////                        if (temp_format >= 39.0) {
////                            txtEspera.setVisibility(View.GONE);
////                            tv_temperatura.setVisibility(View.VISIBLE);
////                            grados.setVisibility(View.VISIBLE);
////
////                            tempView.setBackgroundColor(ContextCompat.getColor(getActivity().getApplicationContext(), R.color.fondo));
////                            llayout.setBackground(getResources().getDrawable(R.drawable.temp_alta));
////                            tv_mensaje_temp.setText("Temperatura muy Alta");
////                            //tv_mensaje_emergencia.setVisibility(View.VISIBLE);
////                        }
////
////                    } else {
////
////                        Log.d("Yulis", "Llego al tiempo estimado...");
////
////
////                    }
////
////
////                }
////
////
////            } else if (intent.getAction().equals(YolikanService.ACTION_TERMINAR_SERVICIO)) {
////                String motivo = intent.getStringExtra("motivo");
////                Toast.makeText(getActivity().getApplicationContext(), motivo, Toast.LENGTH_LONG).show();
////                Intent cerrar = new Intent(getActivity().getApplicationContext(), MainActivity.class);
////                startActivity(cerrar);
////                getActivity().finish();
////            } else if (intent.getAction().equals(YolikanService.ACTION_ACTUALIZA_REGISTROS)) {
////                Log.d("Yulis", "Actualiza registros");
////                //String motivo = intent.getStringExtra("actualiza");
////                //Log.d(Configuration.TAG, "actualiza: " + motivo);
////                //showRegisters2();
////            } else if (intent.getAction().equals(YolikanService.ACTION_AVISO_GPS)) {
////                showGPSDisabledAlertToUser();
////            }
////        }
////    }
//
//    private void showGPSDisabledAlertToUser() {
//        AlertDialog.Builder builder = new AlertDialog.Builder(mx.com.appyolikancv01.fragments.FragmentServiceActivity.this.getActivity());
//        builder.setMessage("El GPS de tu dispositivo está apagado. La aplicación utiliza los datos " +
//                "de geolocalización para fines estadísticos - analíticos. ¿Desea habilitarlo?")
//                .setCancelable(false)
//                .setPositiveButton("Activar GPS", new DialogInterface.OnClickListener() {
//                    public void onClick(DialogInterface dialog, int id) {
//                        Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
//                        startActivity(intent);
//                    }
//                })
//                .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
//                    public void onClick(DialogInterface dialog, int id) {
//                        dialog.cancel();
//                    }
//                });
//        AlertDialog alert = builder.create();
//        alert.show();
//    }
//
//    public void onStart() {
//        super.onStart();
//    }
//
//    private void terminarLectura() {
//        new LovelyStandardDialog(mx.com.appyolikancv01.fragments.FragmentServiceActivity.this.getActivity(), LovelyStandardDialog.ButtonLayout.VERTICAL)
//                .setTopColorRes(R.color.colorAccent)
//                .setButtonsColorRes(R.color.colorPrimaryDark)
//                //.setIcon(R.drawable.ic_icon_yolikan)
//                .setTitle("Yolikan")
//                .setMessage("¿Desea desconectar el dispositivo?")
//                .setPositiveButton(android.R.string.ok, new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        terminaServicio();
//
//                    }
//                })
//                .setNegativeButton(android.R.string.no, null)
//                .show();
//    }
//
//
//    private void terminaServicio() {
//
//        YolikanService.killService(mx.com.appyolikancv01.fragments.FragmentServiceActivity.this.getActivity(), new mx.com.appyolikancv01.fragments.FragmentServiceActivity.AccountInfoResultReceiver(getActivity()));
//        ((TestMenuActivity) getActivity()).changeMenuListDisp();
//
//
//        PageFragment pageFragment = new PageFragment();
//        //guardar datos device
//
//        FragmentManager manager = getFragmentManager();
//        FragmentTransaction transaction = manager.beginTransaction();
//        transaction.add(R.id.container, pageFragment, "Prueba");
//        transaction.addToBackStack(null);
//        transaction.attach(pageFragment);
//        transaction.commit();
//    }
//
//    // CONNECT TO SERVICE
//    private class AccountInfoResultReceiver implements YolikanResultReceiver.ResultReceiverCallBack<Integer> {
//        private WeakReference<FragmentActivity> activityRef;
//
//        public AccountInfoResultReceiver(FragmentActivity activity) {
//            activityRef = new WeakReference<FragmentActivity>(activity);
//        }
//
//        @Override
//        public void onSuccess(Integer data) {
//            if (activityRef != null && activityRef.get() != null) {
//                ///activityRef.get().label.setText("Your balance: "+data);
//            }
//        }
//
//        @Override
//        public void onError(Exception exception) {
//            ///activityRef.get().showMessage("Account info failed");
//        }
//    }
//
//    private String CargarDevice() {
//        SharedPreferences preferencias = getActivity().getSharedPreferences("device", Context.MODE_PRIVATE);
//        String Servicio = preferencias.getString("Datosdevice", "No hay device");
//        Preferencias_device = Servicio;
//        return Servicio;
//    }
//
//    //Boton de Start
//    public void b_start() {
//        if (pausado) {
//            synchronized (this) {
//                pausado = false;
//                //parar.setEnabled(true);
//                //inio.setEnabled(false);
//                cronopost();
//                this.notifyAll();
//            }
//        } else {
//            //parar.setEnabled(true);
//            //inio.setEnabled(false);
//            cronopost();
//
//        }
//    }
//
//    public void b_pausa() {
//        pausado = true;
//        //parar.setEnabled(false);
//        //inio.setEnabled(true);
//    }
//
//    public void cronopost() {
//        Thread th = new Thread(new Runnable() {
//            @Override
//            public void run() {
//                while (true) {
//                    if (pausado) {
//                        synchronized (this) {
//                            try {
//                                this.wait();
//                            } catch (InterruptedException e) {
//                                e.printStackTrace();
//                            }
//                        }
//                    }
//                    espera(10);
//
//                    if (!pausado) {
//                        salida = "";
//                        crono();
//                        //Formato de la salida:
//                        formato();
//
//                        tv_mensaje_temp.post(new Runnable() {
//                            @Override
//                            public void run() {
//                                l_cronometro = salida;
//
//                                if (salida.equals(Configuration.TIME)) {
//                                    //Log.d("Yulis", "Llego al tiempo esperado -...");
//                                    txtEspera.setVisibility(View.GONE);
//                                    tv_temperatura.setText("- -");
//                                    tv_mensaje_temp.setText("- - -");
//                                    pausado = true;
//
//                                }
//
//                                if (salida != Configuration.TIME) {
//                                    //Log.d("Yulis", "tiempo transcurrido....." + salida);
//                                    //tv_temperatura.setText("Estabilizando sistema de medición \\n\\n Temperatura corporal \\n\\n Espere 30 minutos..");
//                                    txtEspera.setVisibility(View.VISIBLE);
//                                    tv_temperatura.setVisibility(View.GONE);
//                                    grados.setVisibility(View.GONE);
//                                    tv_mensaje_temp.setText(salida);
//                                } else {
//                                    pausado = true;
//                                }
//                                //tv_mensaje_temp.setText(salida);
//                            }
//                        });
//
//
//                    }
//
//
//                }
//            }
//        });
//        if (!th.isAlive()) {
//            th.start();
//        }
//    }
//
//    public void crono() {
//        micros++;
//        if (micros == 100) {
//            seg++;
//            micros = 0;
//        }
//        if (seg == 60) {
//            min++;
//            seg = 0;
//        }
//        if (min == 60) {
//            min = 0;
//            horas++;
//        }
//    }
//
//    public void formato() {
//        if (horas <= 9) {
//            salida += "0";
//        }
//        salida += horas;
//        salida += ":";
//        if (min <= 9) {
//            salida += "0";
//        }
//        salida += min;
//        salida += ":";
//        if (seg <= 9) {
//            salida += "0";
//        }
//        salida += seg;
//        //Log.d("Yulis", "Tiempo transcurrido: " + salida);
//
//
//        /*salida += seg + ":" ;
//        if(micros<=9){
//            salida+="0";
//        }
//        salida+=micros;*/
//    }
//
//    static public void espera(int e) {
//        try {
//            Thread.sleep(e);
//        } catch (InterruptedException ex) {
//        }
//    }
//
//    private String CargarDatosUsuario() {
//        SharedPreferences preferencias = getActivity().getSharedPreferences("usuario", Context.MODE_PRIVATE);
//        String Servicio = preferencias.getString("DatosUsuario", "No hay datos");
//        Preferencias_datosUsuario = Servicio;
//        return Servicio;
//    }
//
//
//    private void GuardarTemperatura(String Dato) {
//        SharedPreferences preferencias = getActivity().getSharedPreferences("temperatura", Context.MODE_PRIVATE);
//        String DatoTem = Dato;
//        SharedPreferences.Editor editor = preferencias.edit();
//        editor.putString("DatoTemperatura", DatoTem);
//        editor.commit();
//    }
//
//    private String CargarTemperatura() {
//        SharedPreferences preferencias = getActivity().getSharedPreferences("temperatura", Context.MODE_PRIVATE);
//        String Servicio = preferencias.getString("DatoTemperatura", "No hay datos");
//        Preferencias_datosUsuario = Servicio;
//        return Servicio;
//    }
//
//    public void ClearDatoTemperatura() {
//        SharedPreferences preferenciasSA = getActivity().getSharedPreferences("temperatura", Context.MODE_PRIVATE);
//        SharedPreferences.Editor edito = preferenciasSA.edit();
//        edito.clear().apply();
//        edito.commit();
//    }
//
//
//    public void temperaturaEntrante(String Temperatura) {
//
//    }
//
//    private double Promedio() {
//
//        double n = 0;
//        double promedio = 0;
//        for (int a = 0; a < notas.size(); a++) {
//            n += Double.parseDouble(notas.get(a).toString());
//            promedio = n / notas.size();
//        }
//
//        System.out.println("El promedio de los valores del ArrayList es: " + n / notas.size());
//        System.out.println("El medicion promedio a enviar: " + promedio);
//
//        return promedio;
//    }
//
//    private void VareacionesTemperatura(double temp) {
//        //si la temperatura promedio es
//        if (temp >= 20.0 && temp <= 30.0) {
//
//        }
//        if (temp >= 33.0 && temp <= 35.0) {
//
//            if (temp >= 34.0) {
//                //enviamos el dato
//            }
//
//
//        }
//        if (temp >= 35.0 && temp <= 38.0) {
//
//            if (temp >= 35.5) {
//                //enviamos el dato
//            }
//
//        }
//        if (temp >= 38.0) {
//
//            if (temp >= 38.2) {
//                //enviamos el dato
//
//            }
//        }
//    }
//
//
//    public void guardaRegistro(double temp) {
//        Log.d("Yulis", "guardaRegistro: ");
//        String temperature = String.format("%.1f", temp);
//        Log.d("CIDESI", "Guardando... " + temperature);
//        String datos_token = CargarToken();
//        String datosUser = CargarDatosUsuario();
//        Log.d("CIDESI", "datosUser... " + datosUser);
//
//        Log.d("CIDESI", "Token... " + datos_token);
//
//        final String[] respuesta = new String[1];
//
//        try {
//
//            //cargamos datos del usuario que tiene sesion activa
//            JSONObject prueba = new JSONObject(datosUser);
//            Log.i("Yulis---", prueba.getString("auth_user_data"));
//
//            JSONObject messageJsonPrueba = new JSONObject(prueba.getString("auth_user_data").toString());
//
//            //auth_user_id = messageJsonPrueba.getString("auth_user_id");
//            String username = messageJsonPrueba.getString("username");
//            String first_name = messageJsonPrueba.getString("first_name");
//            String last_name = messageJsonPrueba.getString("last_name");
//            RequestQueue requestQueue = Volley.newRequestQueue(getActivity().getApplicationContext());
//
//            final JSONObject jsonBody = new JSONObject();
//
//            if (personaReg.equals("")) {
//                personaReg = "0";
//            }
//
//            /**
//             * {
//             *   "device":"dev0001",
//             * 	"measurement":32.00,
//             * 	"gps_coordinates":"20.174610115203176, -100.13677041713382",
//             * 	"patient_name":"Maria Yuliana Mata Corchado"
//             * }
//             *
//             */
//
//            jsonBody.put("device", device_name);
//            jsonBody.put("measurement", temperature);
//            jsonBody.put("gps_coordinates", "20.174610115203176, -100.13677041713382");
//            jsonBody.put("patient_name", first_name + " " + last_name);
//
//            final String requestBody = jsonBody.toString();
//            Log.d("CIDESI", "body guarda reg: " + requestBody);
//
//            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, Configuration.HOST_RegistroMedicion, jsonBody, new Response.Listener<JSONObject>() {
//                @Override
//                public void onResponse(JSONObject response) {
//                    //Dialog.cancel();
//                    Log.i("Respuesta: ", response.toString());
//
//                    Log.i("CIDESI", "VOLLEY response guarda medicion: " + response);
//
//                    counter = 0;
//
//                }
//            }, new Response.ErrorListener() {
//
//                @Override
//                public void onErrorResponse(VolleyError error) {
//                    try {
//                        //Dialog.cancel();
//
//                        Log.d("Login...", error.networkResponse.statusCode + "");
//
//                        int estatus = error.networkResponse.statusCode;
//                        String aString = Integer.toString(estatus);//403
//
//                        Log.d("Codigo...", aString);
//
//                        switch (aString) {
//                            case "401":
//                                Log.i("-------case 1-------", "Contraseña o usuario incorrectos.");
//                                CharSequence text = "Contraseña o usuario incorrecto.";
//                                Toast.makeText(getActivity().getApplicationContext(), text, Toast.LENGTH_LONG).show();
//                                break;
//
//                            /*case "403":
//                                Log.i("-------case 2-------", "Sesión iniciada en otro dispositivo.");
//                                CharSequence text1 = "Sesión iniciada en otro dispositivo.";
//                                Toast.makeText(getApplicationContext(), text1, Toast.LENGTH_SHORT).show();
//                                break;*/
//
//                            default:
//                                Log.i("-------Default-------", "Error.");
//                                CharSequence text2 = "Error de comunicación";
//                                Toast.makeText(getActivity().getApplicationContext(), text2, Toast.LENGTH_LONG).show();
//                        }
//
//                    } catch (Exception e) {
//                        e.getMessage();
//                    }
//
//                }
//            }) {
//                @Override
//                public Map<String, String> getHeaders() throws AuthFailureError {
//                    Map<String, String> params = new HashMap<String, String>();
//                    params.put("Content-Type", "application/json");
//                    params.put("Api-Function", "setItemMobile()");
//                    params.put("Authentication-Token", Preferencias_token);
//                    //params.put("api-parameters","{}");
//                    return params;
//                }
//            };
//            requestQueue.add(request);
//
//        } catch (JSONException e) {
//            counter = 0;
//            Log.e("CIDESI", "Error mediciones! " + e.getMessage());
//            e.printStackTrace();
//            counter = 0;
//        }
//    }
//
//    private String CargarToken() {
//        SharedPreferences preferencias = getActivity().getSharedPreferences("token", Context.MODE_PRIVATE);
//        String Servicio = preferencias.getString("DatosToken", "No hay token");
//        Preferencias_token = Servicio;
//        return Servicio;
//    }
//
//
//    //cronometro para enviar la medicion al server
//    public void b_start_server() {
//        if (pausado_server) {
//            synchronized (this) {
//                pausado_server = false;
//                //parar.setEnabled(true);
//                //inio.setEnabled(false);
//                cronopost_server();
//                this.notifyAll();
//            }
//        } else {
//            //parar.setEnabled(true);
//            //inio.setEnabled(false);
//            cronopost_server();
//
//        }
//    }
//
//    public void b_pausa_server() {
//        pausado_server = true;
//        //parar.setEnabled(false);
//        //inio.setEnabled(true);
//    }
//
//    public void cronopost_server() {
//        Thread th = new Thread(new Runnable() {
//            @Override
//            public void run() {
//                while (true) {
//                    if (pausado_server) {
//                        synchronized (this) {
//                            try {
//                                this.wait();
//                            } catch (InterruptedException e) {
//                                e.printStackTrace();
//                            }
//                        }
//                    }
//                    espera_server(10);
//
//                    if (!pausado_server) {
//                        salida_server = "";
//                        crono_server();
//                        //Formato de la salida:
//                        formato_server();
//
//                        tv_mensaje_temp.post(new Runnable() {
//                            @Override
//                            public void run() {
//                                l_cronometro_server = salida_server;
//
//                                if (salida_server.equals(Configuration.TIME_SEND)) {
//                                    ///Log.d("Yulis", "Envia dato al server porque llego al tiempo -...");
//                                    /*txtEspera.setVisibility(View.GONE);
//                                    tv_temperatura.setText("- -");
//                                    tv_mensaje_temp.setText("- - -");*/
//                                    pausado_server = true;
//
//                                }
//
//                                if (salida_server != Configuration.TIME_SEND) {
//                                    Log.d("Yulis", "tiempo transcurrido de envio al server....." + salida_server);
//                                    //tv_temperatura.setText("Estabilizando sistema de medición \\n\\n Temperatura corporal \\n\\n Espere 30 minutos..");
//                                    /*txtEspera.setVisibility(View.VISIBLE);
//                                    tv_temperatura.setVisibility(View.GONE);
//                                    grados.setVisibility(View.GONE);
//                                    tv_mensaje_temp.setText(salida);*/
//                                } else {
//                                    pausado_server = true;
//                                }
//                            }
//                        });
//                    }
//                }
//            }
//        });
//        if (!th.isAlive()) {
//            th.start();
//        }
//    }
//
//    public void crono_server() {
//        micros_server++;
//        if (micros_server == 100) {
//            seg_server++;
//            micros_server = 0;
//        }
//        if (seg_server == 60) {
//            min_server++;
//            seg_server = 0;
//        }
//        if (min_server == 60) {
//            min_server = 0;
//            horas_server++;
//        }
//    }
//
//    public void formato_server() {
//        if (horas_server <= 9) {
//            salida_server += "0";
//        }
//        salida_server += horas_server;
//        salida_server += ":";
//        if (min_server <= 9) {
//            salida_server += "0";
//        }
//        salida_server += min_server;
//        salida_server += ":";
//        if (seg_server <= 9) {
//            salida_server += "0";
//        }
//        salida_server += seg_server;
//        //Log.d("Yulis", "Tiempo transcurrido server: " + salida_server);
//    }
//
//    static public void espera_server(int e) {
//        try {
//            Thread.sleep(e);
//        } catch (InterruptedException ex) {
//        }
//    }
//
//
//    @Override
//    public void onPause() {
//        super.onPause();
//        Log.d("Yulis", "onPause");
//    }
//
//    @Override
//    public void onResume() {
//        super.onResume();
//        Log.d("Yulis", "onResume");
//    }
//
//    @Override
//    public void onStop() {
//        super.onStop();
//        Log.d("Yulis", "onStop");
//    }
//
//    @Override
//    public void onDestroy() {
//        super.onDestroy();
//        Log.d("Yulis", "onDestroy");
//
//    }
//
//
//    public class YolikanReceiver extends BroadcastReceiver {
//
//        @Override
//        public void onReceive(Context context, Intent intent) {
//
//            Log.d("Yulis", "action: " + intent.getAction());
//            Log.d("Yulis", "Actualiza temperatura");
//            //start();
//
//            if (intent.getAction().equals(YolikanService.ACTION_ESTATUS)) {
//                String estatus = intent.getStringExtra("estatus");
//                //activity_chat_status.setText(estatus);
//            } else if (intent.getAction().equals(YolikanService.ACTION_TEMPERATURA)) {
//
//                String temperatura = intent.getStringExtra("temperatura");
//
//                assert temperatura != null;
//                Log.d("Yulis", "temp actualiza lista: " + temperatura);
//                if (temperatura.equals("actualiza")) {
//                    Log.d("Yulis", "temp actualiza lista: " + temperatura);
//                    //showRegisters2();
//                } else {
//
//                    Log.d("Yulis", "l_cronometro: " + l_cronometro);
//
//                    Log.d("Yulis", "Temperatura_: " + String.format("%.1f", Double.valueOf(temperatura)));
//
//                    counter++;
//
//                    //Log.d("Yulis", "l_cronometro_server: " + l_cronometro_server);
//                    //validamos si el tiempo de envio ya llego
//                    if (l_cronometro_server.equals(Configuration.TIME_SEND)) {
//
//                        String currentDateTimeStringw = DateFormat.getDateTimeInstance().format(new Date());
//                        System.out.println("**** Tiempo segunda medicion:" + currentDateTimeStringw);
//
//                        Log.d("CIDESI", "contador_yulis: " + counter);
//                        //counter++;
//
//                        //almacenamos los 9 datos para hacer el promedio de la temperatura
//                        notas.add(Float.parseFloat(temperatura));
//                        //contador
//                        if (counter == Configuration.INTERVALO_PROMEDIO) {
//                            Log.d("CIDESI", "Se guarda la medicion");
//                            //hacemos el promedio
//                            Log.d("******", "*********************");
//
//                            double n = 0;
//                            double promedio = 0;
//                            for (int a = 0; a < notas.size(); a++) {
//                                n += Double.parseDouble(notas.get(a).toString());
//                                promedio = n / notas.size();
//                            }
//
//                            System.out.println("El promedio de los valores del ArrayList es: " + n / notas.size());
//                            System.out.println("El medicion promedio a enviar: " + promedio);
//                            System.out.println("valor n" + n);
//                            System.out.println("size: " + notas.size());
//                            System.out.println("contador: " + counter);
//
//                            //limpiamos la temperatura anterior de las propertys
//                            ClearDatoTemperatura();
//                            //guardamos la temperatura registrada en las propertys
//                            GuardarTemperatura(String.valueOf(promedio));
//                            //guardar registro en el server si la temperatura es mayor a 33
//                            if (promedio >= 33.0) {
//
//                                //no enviamos dato al server
//                                System.out.println("Enviamos dato al server");
//
//                                guardaRegistro(promedio);
//
//                            }
//                            //no es la primer temperatura
//                            tempPrimera = false;
//                            //iniciamos el siguiente el tiempo de una hora para enviar la medicion
//                            l_cronometro_server = "00:00:00";
//                            horas_server = 0;
//                            min_server = 0;
//                            seg_server = 0;
//                            micros_server = 0;
//                            pausado_server = false;
//
//                            b_start_server();
//
//                            notas.clear();
//
//                            Log.d("******", "*********************");
//
//                            counter = 0;
//
//                        }
//
//
//                    } else {
//                        //validamos que las temperaturas tengan cambios muy drasticos
//
//                        Log.d("CIDESI", "contador_yulis: " + counter);
//                        //counter++;
//
//                        //almacenamos los 9 datos para hacer el promedio de la temperatura
//                        notas.add(Float.parseFloat(temperatura));
//                        //contador
//                        if (counter == Configuration.INTERVALO_PROMEDIO) {
//                            Log.d("CIDESI", "Se guarda la medicion");
//                            //hacemos el promedio
//                            Log.d("******", "*********************");
//
//                            double n = 0;
//                            double promedio = 0;
//                            for (int a = 0; a < notas.size(); a++) {
//                                n += Double.parseDouble(notas.get(a).toString());
//                                promedio = n / notas.size();
//                            }
//
//                            System.out.println("El promedio de los valores del ArrayList es: " + n / notas.size());
//                            System.out.println("El medicion promedio a enviar: " + promedio);
//                            System.out.println("valor n" + n);
//                            System.out.println("size: " + notas.size());
//                            System.out.println("contador: " + counter);
//
//                            if (promedio >= 20.0 && promedio <= 30.0) {
//
//                                //no enviamos dato al server
//                                System.out.println("No enviamos dato al server");
//
//
//                            }
//                            if (promedio >= 33.0 && promedio <= 35.0) {
//
//                                String TemperaturaAnterior = CargarTemperatura();
//
//                                System.out.println("Temperatura Anterior: " + TemperaturaAnterior);
//                                System.out.println("Temperatura Actual: " + promedio);
//                                System.out.println("diferencia: " + (promedio - Double.valueOf(TemperaturaAnterior)));
//
//                                if (promedio >= Double.valueOf(TemperaturaAnterior)) {
//
//                                    System.out.println("El promedio es mayor al anterior");
//
//                                    double rango = promedio - Double.valueOf(TemperaturaAnterior);
//
//                                    if (rango >= 1) {
//                                        //enviamos la medicion al server inmediatamente
//
//                                        //limpiamos la temperatura anterior de las propertys
//                                        ClearDatoTemperatura();
//                                        //guardamos la temperatura registrada en las propertys
//                                        GuardarTemperatura(String.valueOf(promedio));
//                                        //guardar registro en el server
//                                        guardaRegistro(promedio);
//                                        //no es la primer temperatura
//                                        tempPrimera = false;
//                                        //reiniciamos el tiempo porque hubo un cambio importante
//                                        l_cronometro_server = "00:00:00";
//                                        horas_server = 0;
//                                        min_server = 0;
//                                        seg_server = 0;
//                                        micros_server = 0;
//                                        pausado_server = false;
//
//                                        b_start_server();
//
//                                    } else {
//                                        //esperamos a que se cumpla el tiempo de 1 hora
//                                    }
//                                }
//                            }
//
//                            if (promedio >= 35.1 && promedio <= 38.0) {
//
//                                String TemperaturaAnterior = CargarTemperatura();
//
//                                if (promedio >= Double.valueOf(TemperaturaAnterior)) {
//
//                                    double rango = promedio - Double.valueOf(TemperaturaAnterior);
//
//                                    if (rango >= 0.5) {
//                                        //enviamos la medicion al server inmediatamente
//
//                                        //limpiamos la temperatura anterior de las propertys
//                                        ClearDatoTemperatura();
//                                        //guardamos la temperatura registrada en las propertys
//                                        GuardarTemperatura(String.valueOf(promedio));
//                                        //guardar registro en el server
//                                        guardaRegistro(promedio);
//                                        //no es la primer temperatura
//                                        tempPrimera = false;
//                                        //reiniciamos el tiempo porque hubo un cambio importante
//                                        l_cronometro_server = "00:00:00";
//                                        horas_server = 0;
//                                        min_server = 0;
//                                        seg_server = 0;
//                                        micros_server = 0;
//                                        pausado_server = false;
//
//                                        b_start_server();
//
//                                    } else {
//                                        //esperamos a que se cumpla el tiempo de 1 hora
//                                    }
//                                }
//                            }
//
//                            if (promedio >= 38.1) {
//
//                                String TemperaturaAnterior = CargarTemperatura();
//
//                                if (promedio >= Double.valueOf(TemperaturaAnterior)) {
//
//                                    double rango = promedio - Double.valueOf(TemperaturaAnterior);
//
//                                    if (rango >= 0.2) {
//                                        //enviamos la medicion al server inmediatamente
//
//                                        //limpiamos la temperatura anterior de las propertys
//                                        ClearDatoTemperatura();
//                                        //guardamos la temperatura registrada en las propertys
//                                        GuardarTemperatura(String.valueOf(promedio));
//                                        //guardar registro en el server
//                                        guardaRegistro(promedio);
//                                        //no es la primer temperatura
//                                        tempPrimera = false;
//                                        //reiniciamos el tiempo porque hubo un cambio importante
//                                        l_cronometro_server = "00:00:00";
//                                        horas_server = 0;
//                                        min_server = 0;
//                                        seg_server = 0;
//                                        micros_server = 0;
//                                        pausado_server = false;
//
//                                        b_start_server();
//
//                                    } else {
//                                        //esperamos a que se cumpla el tiempo de 1 hora
//                                    }
//                                }
//                            }
//
//                            notas.clear();
//
//                            Log.d("******", "*********************");
//
//                            counter = 0;
//
//                        }
//                    }
//
//                    //guardamos la temperatura inicial despues de haber transcurrido el tiempo de espera
//                    //GuardarTemperatura(String.format("%.1f", Double.valueOf(temperatura)));
//
//                        /*txtEspera.setVisibility(View.GONE);
//                        tv_temperatura.setVisibility(View.VISIBLE);
//                        grados.setVisibility(View.VISIBLE);*/
//
//                    tv_temperatura.setText(String.format("%.1f", Double.valueOf(temperatura)));
//
//                    temp_format = Float.parseFloat(temperatura);
//
//                    if (temp_format <= 33.0) {
//
//                        txtEspera.setVisibility(View.VISIBLE);
//                        tv_temperatura.setVisibility(View.GONE);
//                        grados.setVisibility(View.GONE);
//
//                        tempView.setBackgroundColor(ContextCompat.getColor(getActivity().getApplicationContext(), R.color.fondo));
//                        //llayout.setBackgroundColor(getResources().getColor(R.color.colorId));
//                        llayout.setBackground(getResources().getDrawable(R.drawable.temp_baja));
//                        tv_mensaje_temp.setText("- -");
//                        //tv_mensaje_emergencia.setVisibility(View.INVISIBLE);
//                    }
//
//                    if (temp_format >= 33.1 && temp_format < 36.1) {
//                        txtEspera.setVisibility(View.GONE);
//                        tv_temperatura.setVisibility(View.VISIBLE);
//                        grados.setVisibility(View.VISIBLE);
//
//                        tempView.setBackgroundColor(ContextCompat.getColor(getActivity().getApplicationContext(), R.color.fondo));
//                        //llayout.setBackgroundColor(getResources().getColor(R.color.colorId));
//                        llayout.setBackground(getResources().getDrawable(R.drawable.temp_baja));
//                        tv_mensaje_temp.setText("Temperatura Baja");
//                        //tv_mensaje_emergencia.setVisibility(View.INVISIBLE);
//                    }
//
//                    if (temp_format >= 36.2 && temp_format < 37.2) {
//                        txtEspera.setVisibility(View.GONE);
//                        tv_temperatura.setVisibility(View.VISIBLE);
//                        grados.setVisibility(View.VISIBLE);
//
//                        tempView.setBackgroundColor(ContextCompat.getColor(getActivity().getApplicationContext(), R.color.fondo));
//                        llayout.setBackground(getResources().getDrawable(R.drawable.tem_normal));
//                        tv_mensaje_temp.setText("Temperatura Normal");
//                        //tv_mensaje_emergencia.setVisibility(View.INVISIBLE);
//                    }
//                    if (temp_format >= 37.3 && temp_format <= 38.9) {
//                        txtEspera.setVisibility(View.GONE);
//                        tv_temperatura.setVisibility(View.VISIBLE);
//                        grados.setVisibility(View.VISIBLE);
//
//                        tempView.setBackgroundColor(ContextCompat.getColor(getActivity().getApplicationContext(), R.color.fondo));
//                        llayout.setBackground(getResources().getDrawable(R.drawable.temp_media));
//                        tv_mensaje_temp.setText("Temperatura Alta");
//                        //tv_mensaje_emergencia.setVisibility(View.INVISIBLE);
//                    }
//                    if (temp_format >= 39.0) {
//                        txtEspera.setVisibility(View.GONE);
//                        tv_temperatura.setVisibility(View.VISIBLE);
//                        grados.setVisibility(View.VISIBLE);
//
//                        tempView.setBackgroundColor(ContextCompat.getColor(getActivity().getApplicationContext(), R.color.fondo));
//                        llayout.setBackground(getResources().getDrawable(R.drawable.temp_alta));
//                        tv_mensaje_temp.setText("Temperatura muy Alta");
//                        //tv_mensaje_emergencia.setVisibility(View.VISIBLE);
//                    }
//
//
//
//
//                }
//
//
//            } else if (intent.getAction().equals(YolikanService.ACTION_TERMINAR_SERVICIO)) {
//                String motivo = intent.getStringExtra("motivo");
//                Toast.makeText(getActivity().getApplicationContext(), motivo, Toast.LENGTH_LONG).show();
//                Intent cerrar = new Intent(getActivity().getApplicationContext(), MainActivity.class);
//                startActivity(cerrar);
//                getActivity().finish();
//            } else if (intent.getAction().equals(YolikanService.ACTION_ACTUALIZA_REGISTROS)) {
//                Log.d("Yulis", "Actualiza registros");
//                //String motivo = intent.getStringExtra("actualiza");
//                //Log.d(Configuration.TAG, "actualiza: " + motivo);
//                //showRegisters2();
//            } else if (intent.getAction().equals(YolikanService.ACTION_AVISO_GPS)) {
//                showGPSDisabledAlertToUser();
//            }
//        }
//    }
//
//
//}
