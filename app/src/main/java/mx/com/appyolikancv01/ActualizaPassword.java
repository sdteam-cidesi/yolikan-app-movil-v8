package mx.com.appyolikancv01;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.fragment.app.Fragment;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.yarolegovich.lovelydialog.LovelyStandardDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import mx.com.appyolikancv01.fragments.PageCuenta;

public class ActualizaPassword extends AppCompatActivity implements View.OnClickListener{

    private EditText PassAnterior, password_nuevo, confirmarNewPass;
    ProgressDialog progressDialog;
    Button btn_actualizaP;

    String Preferencias_datosUsuario = "";
    String Preferencias_token = "";
    String Preferencias_Password = "";
    String auth_user_id = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        setContentView(R.layout.activity_actualiza_password);

        PassAnterior = (EditText) findViewById(R.id.et_password_actual);
        password_nuevo = (EditText) findViewById(R.id.et_password_nuevo);
        confirmarNewPass = (EditText) findViewById(R.id.et_password_confirmacion);
        btn_actualizaP = (Button) findViewById(R.id.btn_actualizaP);

        btn_actualizaP.setOnClickListener(this);


        //cargamos datos del usuario que tiene sesion activa
        String datosUser = CargarDatosUsuario();
        String datos_token = CargarToken();
        String datos_password = CargarPassword();
        //Log.i("Yulis---datosUser", datosUser);
        //Log.i("Yulis---datos_token", datos_token);


        try {
            JSONObject prueba = new JSONObject(datosUser);

            //Log.i("Yulis---", prueba.getString("auth_user_data"));

            JSONObject messageJsonPrueba = new JSONObject(prueba.getString("auth_user_data").toString());

            auth_user_id = messageJsonPrueba.getString("auth_user_id");

        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_actualizaP:
                UpdatePassword();
                break;
        }

    }

    private void UpdatePassword() {

        //Usa 8 o más caracteres con una combinación de letras, números y símbolos.

        if (PassAnterior.getText().toString().equals("")) {
            PassAnterior.setError("Ingrese su contraseña actual");
            Toast.makeText(getApplicationContext(), "Ingrese su contraseña actual.", Toast.LENGTH_LONG).show();
            PassAnterior.requestFocus();
        } else {
            if (password_nuevo.getText().toString().equals("")) {
                password_nuevo.setError("Ingrese su nueva contraseña");
                Toast.makeText(getApplicationContext(), "Ingrese su nueva contraseña.", Toast.LENGTH_LONG).show();
                password_nuevo.requestFocus();
            } else {
                if (confirmarNewPass.getText().toString().equals("")) {
                    confirmarNewPass.setError("Ingrese la confirmación de su nueva contraseña");
                    Toast.makeText(getApplicationContext(), "Ingrese la confirmación de su nueva contraseña.", Toast.LENGTH_LONG).show();
                    confirmarNewPass.requestFocus();
                } else {
                    if (password_nuevo.getText().toString().equals(confirmarNewPass.getText().toString())) {

                        //Toast.makeText(getApplicationContext(), "Ya se puede guardar.", Toast.LENGTH_LONG).show();

                        Integer idUsuario = Integer.parseInt(auth_user_id);
                        String Password = password_nuevo.getText().toString();

                        //Log.d("YUlis",idUsuario.toString());
                        //Log.d("YUlis",Password);

                        //metodo para realizar el update
                        ActualizarPassword(idUsuario, Password);

                    } else {
                        password_nuevo.setError("Las contraseñas no coinciden.");
                        confirmarNewPass.setError("Las contraseñas no coinciden.");
                        Toast.makeText(getApplicationContext(), "Las contraseñas no coinciden.", Toast.LENGTH_LONG).show();
                        password_nuevo.requestFocus();
                        password_nuevo.setText("");
                        confirmarNewPass.setText("");
                    }
                }
            }
        }

    }

    public void ActualizarPassword(Integer id, String Password) {

        progressDialog = new ProgressDialog(ActualizaPassword.this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Actualizanzo contraseña...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setProgress(0);
        progressDialog.show();

        final String[] respuesta = {""};

        try {
            RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
            final JSONObject jsonBody = new JSONObject();

            jsonBody.put("password", Password);

            final String requestBody = jsonBody.toString();

            //Log.d(Configuration.TAG, "requestBody: " + requestBody);

            //Log.d(Configuration.TAG, "URL: " + Configuration.HOST_UPDATE_PASSWORD + id);

            JsonObjectRequest request = new JsonObjectRequest(Request.Method.PUT, Configuration.HOST_UPDATE_PASSWORD + id, jsonBody, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    //Dialog.cancel();
                    //Log.i("Respuesta: ", response.toString());

                    progressDialog.dismiss();

                    Fragment selectedFragment=null;
                    selectedFragment=new PageCuenta();
                    ActualizaPassword.this.finish();

                    Toast.makeText(getApplicationContext(), "Contraseña actualizada.", Toast.LENGTH_LONG).show();
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    try {
                        //Dialog.cancel();
                        progressDialog.dismiss();
                        //Log.d("Login...", error.networkResponse.statusCode + "");

                        int estatus = error.networkResponse.statusCode;
                        String aString = Integer.toString(estatus);//403

                        //Log.d("Codigo...", aString);
                        //Log.d("Codigo...", error.getMessage());

                        switch (aString) {
                            case "401":
                                //Log.i("-------case 1-------", "Contraseña o usuario incorrectos.");
                                CharSequence text = "Contraseña o usuario incorrecto.";
                                Toast.makeText(getApplicationContext(), text, Toast.LENGTH_LONG).show();
                                break;

                            /*case "403":
                                //Log.i("-------case 2-------", "Sesión iniciada en otro dispositivo.");
                                CharSequence text1 = "Sesión iniciada en otro dispositivo.";
                                Toast.makeText(getApplicationContext(), text1, Toast.LENGTH_SHORT).show();
                                break;*/

                            default:
                                //Log.i("-------Default-------", "Error.");
                                CharSequence text3 = "Error de comunicación";
                                Toast.makeText(getApplicationContext(), text3, Toast.LENGTH_LONG).show();
                        }

                    } catch (Exception e) {
                        e.getMessage();
                    }

                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("Content-Type", "application/json");
                    params.put("Api-Function", "updatePasswordMobile()");
                    params.put("Authentication-Token", Preferencias_token);
                    //params.put("api-parameters","{}");
                    return params;
                }
            };

            requestQueue.add(request);

            requestQueue.addRequestFinishedListener(new RequestQueue.RequestFinishedListener<Object>() {
                @Override
                public void onRequestFinished(Request<Object> request) {
                    requestQueue.getCache().clear();
                }
            });

        } catch (JSONException e) {
            progressDialog.dismiss();
            e.printStackTrace();

            new LovelyStandardDialog(ActualizaPassword.this, LovelyStandardDialog.ButtonLayout.VERTICAL)
                    .setTopColorRes(R.color.colorAccent)
                    .setButtonsColorRes(R.color.colorPrimaryDark)
                    .setTitle("Yolikan")
                    .setMessage("Error de conexión. " + e.getMessage())
                    .setPositiveButton(android.R.string.ok, new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                        }
                    })
                    //.setNegativeButton(android.R.string.no, null)
                    .show();
        }

    }


    private void Regresar() {
        Intent intent = new Intent(ActualizaPassword.this, MainActivity.class);
        ActualizaPassword.this.finish();
        startActivity(intent);
    }

    private String CargarDatosUsuario() {
        SharedPreferences preferencias = getSharedPreferences("usuario", Context.MODE_PRIVATE);
        String Servicio = preferencias.getString("DatosUsuario", "No hay datos");
        Preferencias_datosUsuario = Servicio;
        return Servicio;
    }

    private String CargarToken() {
        SharedPreferences preferencias = getSharedPreferences("token", Context.MODE_PRIVATE);
        String Servicio = preferencias.getString("DatosToken", "No hay token");
        Preferencias_token = Servicio;
        return Servicio;
    }

    private String CargarPassword() {
        SharedPreferences preferencias = getSharedPreferences("password", Context.MODE_PRIVATE);
        String Servicio = preferencias.getString("DatosPassword", "No hay password");
        Preferencias_Password = Servicio;
        return Servicio;
    }


}
