package mx.com.appyolikancv01;

import com.android.volley.BuildConfig;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Esta clase contiene las variables de configuración general de la aplicación, por ejemplo, la versión de
 * la base de datos interna (SQLite), la URL de los servicios Web, la definición de los servicios Web
 *
 */
public class Configuration {
    static int VERSION_DB = 6;
    static final String TAG = "YOLIKAN";
    public static final String FONT_TEXTO = "fonts/Helvetica.ttf";

    ///public static final String HOST = "http://yolikan.vlim.com.mx/yolikan_ws/"; /* VLIM server */
    public static final String HOST = "http://52.70.79.124/yolikan_ws/"; /* Yolikan AWS server */


    public static final String HOST_SERVER = "https://iot.cidesi.mx/";
    public static final String HOST_API = "apirestcore/yolikan";

    public static final String HOST_prueba = "https://iot.cidesi.mx/apirestcore/yolikan/apirest/mobile/register";
    public static final String HOST_RegistroMedicion = "https://iot.cidesi.mx/apirestcore/yolikan/apirest/mobile/sensor";
    public static final String HOST_login = "https://iot.cidesi.mx/apirestcore/yolikan/apirest/system/auth/gettoken";
    public static final String HOST_Instituciones = "https://iot.cidesi.mx/apirestcore/yolikan/apirest/mobile/institution";
    public static final String HOST_REGISTRO_USUARIO = "https://iot.cidesi.mx/apirestcore/yolikan/apirest/mobile/mobile/user/";
    public static final String HOST_UPDATE_USUARIO = "https://iot.cidesi.mx/apirestcore/yolikan/apirest/mobile/mobile/user?id=";
    public static final String HOST_UPDATE_PASSWORD = "https://iot.cidesi.mx/apirestcore/yolikan/apirest/mobile/mobile/user?id=";
    public static final String HOST_DELETE_USER = "https://iot.cidesi.mx/apirestcore/yolikan/apirest/mobile/mobile/user?id=";
    public static final String HOST_RESET_PASSWORD = HOST_SERVER + HOST_API + "/apirest/mobile/mobile/user/recovery/password";

    public static final String TIME = "00:01:00";
    public static final String TIME_SEND = "59:00";
    public static final int INTERVALO_PROMEDIO = 4;

    public static final int TIME_E = 20000;

    public static final String server = "www.google.com";

    public static final String GUARDA_MEDICION = HOST + "guardaMedicion";
    public static final String MIS_MEDICIONES = HOST + "misMediciones";



    static String version = "Yolikan beta_" + BuildConfig.VERSION_NAME;

    //funcion para validar email
    public static boolean isEmailValid(String email) {
        boolean isValid = false;

        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        CharSequence inputStr = email;

        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);
        if (matcher.matches()) {
            isValid = true;
        }
        return isValid;
    }

    public static boolean isFechaValida(String fecha) {
        try {
            SimpleDateFormat formatoFecha = new SimpleDateFormat("yyyy/MM/dd", Locale.getDefault());
            formatoFecha.setLenient(false);
            formatoFecha.parse(fecha);
        } catch (ParseException e) {
            return false;
        }
        return true;
    }

}
