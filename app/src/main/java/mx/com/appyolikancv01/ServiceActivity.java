package mx.com.appyolikancv01;


import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.yarolegovich.lovelydialog.LovelyStandardDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.lang.ref.WeakReference;
import java.util.UUID;

public class ServiceActivity extends AppCompatActivity implements View.OnClickListener {

    String Preferencias_datosUsuario = "";
    String auth_user_id = "";

    TextView txtEspera;
    TextView grados;

    TextView activity_chat_status;
    TextView tv_temperatura;
    //TextView btn_todas_temp;
    //TextView btn_ayuda;
    TextView tv_mensaje_temp;
    //TextView tv_mensaje_emergencia;
    TextView tv_usuario;
    TextView tv_misMediciones;
    ScrollView sv;

    LinearLayout llayout;

    ToggleButton tg_actualizar;

    ImageView btn_config, btn_close, btn_salir;
    Integer counter = 0;
    double temp_format = 0.0;

    View tempView;

    String id_usuario, temperatura, device_name, device_mac, actualizar = "SI", personaReg = "", nickname_dispositivo = "", nombrePersonaReg = "";
    JSONObject jsonObj;
    String JsonResponse = null;

    // Table Layout
    TableLayout tableLayout;
    Context context;

    String id_registro, fecha, tempRecord, dispositivo;

    // Service
    YolikanReceiver receiver;
    private BluetoothDevice mDevice;
    private UUID mDeviceUUID;

    // connect to service


    //tiempo en espera
    String l_cronometro = "";
    String salida;
    int horas, min, seg, micros;
    boolean pausado;

    private enum Actions {
        START
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_visor_temperatura);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        mDevice = getIntent().getParcelableExtra("device");
        mDeviceUUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");


        btn_config = findViewById(R.id.btn_config);
        grados = findViewById(R.id.grados);
        txtEspera = findViewById(R.id.txtEspera);
        txtEspera.setVisibility(View.GONE);


        btn_close = findViewById(R.id.imageView);
        tv_temperatura = findViewById(R.id.tv_temperatura);

        tv_mensaje_temp = findViewById(R.id.tv_mensaje_temp);
        tv_usuario = findViewById(R.id.tv_usuario);
        llayout = findViewById(R.id.liyimag);

        tempView = findViewById(R.id.constraint_layout);
        tempView.setBackgroundColor(ContextCompat.getColor(this, R.color.fondo));

        btn_config.setOnClickListener(this);

        //cargamos datos del usuario que tiene sesion activa
        String datosUser = CargarDatosUsuario();
        //Log.i("Yulis---", datosUser);

        try {

            JSONObject DatosUsuario = new JSONObject(datosUser);
            //Log.i("Yulis---", DatosUsuario.getString("auth_user_data"));

            JSONObject messageJsonPrueba = new JSONObject(DatosUsuario.getString("auth_user_data").toString());

            auth_user_id = messageJsonPrueba.getString("auth_user_id");
            String first_name = messageJsonPrueba.getString("first_name");
            String last_name = messageJsonPrueba.getString("last_name");

            tv_usuario.setText(first_name + " " + last_name);

        } catch (JSONException e) {
            e.printStackTrace();
        }


        context = getApplicationContext();
        l_cronometro = "00:00:00";

        tv_mensaje_temp.setText("00:00:00");
        horas = 0;
        min = 0;
        seg = 0;
        micros = 0;
        pausado = false;

        b_start();

        //showRegisters2();

        Intent yolikanIntent = new Intent(mx.com.appyolikancv01.ServiceActivity.this, YolikanService.class);
        yolikanIntent.setAction(Actions.START.name());
        yolikanIntent.putExtra("DEVICE_ID", mDevice);
        startService(yolikanIntent);


        IntentFilter filter = new IntentFilter();
        filter.addAction(YolikanService.ACTION_TEMPERATURA);
        filter.addAction(YolikanService.ACTION_ESTATUS);
        YolikanReceiver rcv = new YolikanReceiver();
        registerReceiver(rcv, filter);

        //start();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.imageView:
                terminarLectura();
                break;

            case R.id.btn_config:
                terminarLectura();
                break;
        }
    }

    public class YolikanReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {

            //Log.d(Configuration.TAG, "action: " + intent.getAction());
            //Log.d(Configuration.TAG, "Actualiza temperatura");
            //start();

            if (intent.getAction().equals(YolikanService.ACTION_ESTATUS)) {
                String estatus = intent.getStringExtra("estatus");
                //activity_chat_status.setText(estatus);
            } else if (intent.getAction().equals(YolikanService.ACTION_TEMPERATURA)) {

                String temperatura = intent.getStringExtra("temperatura");

                assert temperatura != null;
                //Log.d(Configuration.TAG, "temp actualiza lista: " + temperatura);
                if (temperatura.equals("actualiza")) {
                    //Log.d(Configuration.TAG, "temp actualiza lista: " + temperatura);
                    //showRegisters2();
                } else {

                    //Log.d("Yulis", "cronometro: " + l_cronometro);

                    if (l_cronometro.equals(Configuration.TIME)) {

                        txtEspera.setVisibility(View.GONE);
                        tv_temperatura.setVisibility(View.VISIBLE);
                        grados.setVisibility(View.VISIBLE);

                        tv_temperatura.setText(String.format("%.1f", Double.valueOf(temperatura)));

                        temp_format = Float.parseFloat(temperatura);

                        if (temp_format <= 36.1) {
                            tempView.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.fondo));
                            //llayout.setBackgroundColor(getResources().getColor(R.color.colorId));
                            llayout.setBackground(getResources().getDrawable(R.drawable.temp_baja));
                            tv_mensaje_temp.setText("Temperatura Baja");
                            //tv_mensaje_emergencia.setVisibility(View.INVISIBLE);
                        }

                        if (temp_format >= 36.2 && temp_format < 37.2) {
                            tempView.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.fondo));
                            llayout.setBackground(getResources().getDrawable(R.drawable.tem_normal));
                            tv_mensaje_temp.setText("Temperatura Normal");
                            //tv_mensaje_emergencia.setVisibility(View.INVISIBLE);
                        }
                        if (temp_format >= 37.3 && temp_format <= 38.9) {
                            tempView.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.fondo));
                            llayout.setBackground(getResources().getDrawable(R.drawable.temp_media));
                            tv_mensaje_temp.setText("Temperatura Alta");
                            //tv_mensaje_emergencia.setVisibility(View.INVISIBLE);
                        }
                        if (temp_format >= 39.0) {
                            tempView.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.fondo));
                            llayout.setBackground(getResources().getDrawable(R.drawable.temp_alta));
                            tv_mensaje_temp.setText("Temperatura muy Alta");
                            //tv_mensaje_emergencia.setVisibility(View.VISIBLE);
                        }

                    } else {

                        //Log.d("Yulis", "Llego al tiempo estimado...");


                    }


                }


            } else if (intent.getAction().equals(YolikanService.ACTION_TERMINAR_SERVICIO)) {
                String motivo = intent.getStringExtra("motivo");
                Toast.makeText(getApplicationContext(), motivo, Toast.LENGTH_LONG).show();
                Intent cerrar = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(cerrar);
                finish();
            } else if (intent.getAction().equals(YolikanService.ACTION_ACTUALIZA_REGISTROS)) {
                //Log.d(Configuration.TAG, "Actualiza registros");
                //String motivo = intent.getStringExtra("actualiza");
                ////Log.d(Configuration.TAG, "actualiza: " + motivo);
                //showRegisters2();
            } else if (intent.getAction().equals(YolikanService.ACTION_AVISO_GPS)) {
                showGPSDisabledAlertToUser();
            }
        }
    }

    private void showGPSDisabledAlertToUser() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("El GPS de tu dispositivo está apagado. La aplicación utiliza los datos " +
                "de geolocalización para fines estadísticos - analíticos. ¿Desea habilitarlo?")
                .setCancelable(false)
                .setPositiveButton("Activar GPS", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivity(intent);
                    }
                })
                .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    private void showRegisters2() {
        //Log.d(Configuration.TAG, "registros...");

        final String[] respuesta = new String[1];
        final String[] dataRecords = new String[1];
        final String[] mediciones = {""};

        /*//Log.d(Configuration.TAG, "Scroll: " + actualizar);
        if(actualizar.equals("SI")){
            sv.scrollTo(0, 0);
        }*/

        try {
            RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
            final JSONObject jsonBody = new JSONObject();

            jsonBody.put("id_user", id_usuario);
            jsonBody.put("tipo", "20");

            final String requestBody = jsonBody.toString();
            //Log.d(Configuration.TAG, "body: " + requestBody);

            StringRequest stringRequest = new StringRequest(Request.Method.POST, Configuration.MIS_MEDICIONES, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    try {
                        JSONArray array = new JSONArray(response);
                        JSONObject obj;
                        for (int i = 0; i < array.length(); i++) {
                            obj = array.getJSONObject(i);
                            respuesta[0] = obj.getString("status");

                            if (respuesta[0].equals("OK")) {
                                dataRecords[0] = obj.getString("mediciones");
                            }

                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    //Log.i(Configuration.TAG, "VOLLEY response mis mediciones: " + respuesta[0]);

                    if (respuesta[0].equals("OK")) {

                        try {
                            JSONArray arrayOK = new JSONArray(dataRecords[0]);
                            JSONObject obj;
                            for (int i = 0; i < arrayOK.length(); i++) {
                                obj = arrayOK.getJSONObject(i);
                                id_registro = obj.getString("id_registro");
                                fecha = obj.getString("fecha");
                                tempRecord = obj.getString("temperatura");
                                dispositivo = obj.getString("dispositivo");

                                String fechaFormat = fecha; //parseDate(fecha, originalFormat, dateFormat);

                                mediciones[0] += tempRecord + " °C\t\t\t\t\t " + fechaFormat + "\n";

                                // Add a TextView in the first column.
                                ///TextView textView = new TextView(context);
                                //Log.d(Configuration.TAG, "med: " + tempRecord + " °C\t\t\t\t\t " + fechaFormat);

                            }
                            tv_misMediciones.setText("");
                            tv_misMediciones.setText(mediciones[0]);
                            ////Log.d(Configuration.TAG, "Mis mediciones: " + mediciones[0]);
                            //Log.d(Configuration.TAG, "tableLayout re printing...");

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        //Log.d(Configuration.TAG, "No hay registros");
                    }
                }

            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    //Log.d(Configuration.TAG, "Error de conexión mis mediciones " + error.toString());
                    finish();
                }
            }) {
                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }

                @Override
                public byte[] getBody() throws AuthFailureError {
                    try {
                        return requestBody == null ? null : requestBody.getBytes("utf-8");
                    } catch (UnsupportedEncodingException uee) {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                        return null;
                    }
                }
            };

            requestQueue.add(stringRequest);

            requestQueue.addRequestFinishedListener(new RequestQueue.RequestFinishedListener<Object>() {
                @Override
                public void onRequestFinished(Request<Object> request) {
                    requestQueue.getCache().clear();
                }
            });

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        // nothing happens
    }

    private void terminarLectura() {
        new LovelyStandardDialog(this, LovelyStandardDialog.ButtonLayout.VERTICAL)
                .setTopColorRes(R.color.colorAccent)
                .setButtonsColorRes(R.color.colorPrimaryDark)
                //.setIcon(R.drawable.ic_icon_yolikan)
                .setTitle("Yolikan")
                .setMessage("¿Desea desconectar el dispositivo?")
                .setPositiveButton(android.R.string.ok, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        terminaServicio();
                    }
                })
                .setNegativeButton(android.R.string.no, null)
                .show();
    }

    private void cerrarSesion() {

        new LovelyStandardDialog(this, LovelyStandardDialog.ButtonLayout.VERTICAL)
                .setTopColorRes(R.color.colorAccent)
                .setButtonsColorRes(R.color.colorPrimaryDark)
                //.setIcon(R.drawable.ic_icon_yolikan)
                .setTitle("Yolikan")
                .setMessage("¿Desea cerrar sesión?")
                .setPositiveButton(android.R.string.ok, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //Toast.makeText(getApplicationContext(), "positive clicked", Toast.LENGTH_SHORT).show();


                    }
                })
                .setNegativeButton(android.R.string.no, null)
                .show();
    }

    private void terminaServicio() {
        YolikanService.killService(this, new AccountInfoResultReceiver(this));

        Intent back = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(back);
        finish();
    }

    // CONNECT TO SERVICE
    private class AccountInfoResultReceiver implements YolikanResultReceiver.ResultReceiverCallBack<Integer> {
        private WeakReference<ServiceActivity> activityRef;

        public AccountInfoResultReceiver(mx.com.appyolikancv01.ServiceActivity activity) {
            activityRef = new WeakReference<ServiceActivity>(activity);
        }

        @Override
        public void onSuccess(Integer data) {
            if (activityRef != null && activityRef.get() != null) {
                ///activityRef.get().label.setText("Your balance: "+data);
            }
        }

        @Override
        public void onError(Exception exception) {
            ///activityRef.get().showMessage("Account info failed");
        }
    }

    //Boton de Start
    public void b_start() {
        if (pausado) {
            synchronized (this) {
                pausado = false;
                //parar.setEnabled(true);
                //inio.setEnabled(false);
                cronopost();
                this.notifyAll();
            }
        } else {
            //parar.setEnabled(true);
            //inio.setEnabled(false);
            cronopost();

        }
    }

    public void b_pausa() {
        pausado = true;
        //parar.setEnabled(false);
        //inio.setEnabled(true);
    }

    public void cronopost() {
        Thread th = new Thread(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    if (pausado) {
                        synchronized (this) {
                            try {
                                this.wait();
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                    espera(10);

                    if (!pausado) {
                        salida = "";
                        crono();
                        //Formato de la salida:
                        formato();

                        tv_mensaje_temp.post(new Runnable() {
                            @Override
                            public void run() {
                                l_cronometro = salida;

                                if (salida.equals(Configuration.TIME)) {
                                    //Log.d("Yulis", "Llego al tiempo esperado -...");
                                    txtEspera.setVisibility(View.GONE);
                                    tv_temperatura.setText("- -");
                                    tv_mensaje_temp.setText("- - -");
                                    pausado = true;

                                }

                                if (salida != Configuration.TIME) {
                                    //Log.d("Yulis", "tiempo transcurrido....." + salida);
                                    //tv_temperatura.setText("Estabilizando sistema de medición \\n\\n Temperatura corporal \\n\\n Espere 30 minutos..");
                                    txtEspera.setVisibility(View.VISIBLE);
                                    tv_temperatura.setVisibility(View.GONE);
                                    grados.setVisibility(View.GONE);
                                    tv_mensaje_temp.setText(salida);
                                } else {
                                    pausado = true;
                                }
                                //tv_mensaje_temp.setText(salida);
                            }
                        });


                    }


                }
            }
        });
        if (!th.isAlive()) {
            th.start();
        }
    }

    public void crono() {
        micros++;
        if (micros == 100) {
            seg++;
            micros = 0;
        }
        if (seg == 60) {
            min++;
            seg = 0;
        }
        if (min == 60) {
            min = 0;
            horas++;
        }
    }

    public void formato() {
        if (horas <= 9) {
            salida += "0";
        }
        salida += horas;
        salida += ":";
        if (min <= 9) {
            salida += "0";
        }
        salida += min;
        salida += ":";
        if (seg <= 9) {
            salida += "0";
        }
        salida += seg;
        ////Log.d("Yulis", "Tiempo transcurrido: " + salida);


        /*salida += seg + ":" ;
        if(micros<=9){
            salida+="0";
        }
        salida+=micros;*/
    }

    static public void espera(int e) {
        try {
            Thread.sleep(e);
        } catch (InterruptedException ex) {
        }
    }

    private String CargarDatosUsuario() {
        SharedPreferences preferencias = getSharedPreferences("usuario", Context.MODE_PRIVATE);
        String Servicio = preferencias.getString("DatosUsuario", "No hay datos");
        Preferencias_datosUsuario = Servicio;
        return Servicio;
    }

}
