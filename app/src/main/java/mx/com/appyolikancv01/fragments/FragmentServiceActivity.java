package mx.com.appyolikancv01.fragments;

import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.SystemClock;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Chronometer;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.yarolegovich.lovelydialog.LovelyStandardDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.ref.WeakReference;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.UUID;

import mx.com.appyolikancv01.Configuration;
import mx.com.appyolikancv01.ListaDispositivosActivity;
import mx.com.appyolikancv01.MainActivity;
import mx.com.appyolikancv01.R;
import mx.com.appyolikancv01.ServiceActivity;
import mx.com.appyolikancv01.TestMenuActivity;
import mx.com.appyolikancv01.YolikanResultReceiver;
import mx.com.appyolikancv01.YolikanService;

public class FragmentServiceActivity extends Fragment implements View.OnClickListener {
    private Chronometer chronometer;
    private long pauseOffset;
    private boolean running;

    DecimalFormat df = new DecimalFormat("#.00");

    String Preferencias_datosUsuario = "";
    String auth_user_id = "";

    TextView txt_arreglo;
    TextView txtEspera;
    TextView grados;

    TextView activity_chat_status;
    TextView tv_temperatura;
    //TextView btn_todas_temp;
    //TextView btn_ayuda;
    TextView tv_mensaje_temp;
    TextView promedio;
    //TextView tv_mensaje_emergencia;
    TextView tv_usuario;
    TextView tv_misMediciones;
    ScrollView sv;

    LinearLayout llayout;
    LinearLayout loader;

    ToggleButton tg_actualizar;

    ImageView btn_config, btn_close, btn_salir;
    Integer counter = 0;
    double temp_format = 0.0;
    ArrayList notas = new ArrayList();

    String Preferencias_token = "";

    View tempView;

    String id_usuario, temperatura, device_name, device_mac, actualizar = "SI", personaReg = "", nickname_dispositivo = "", nombrePersonaReg = "";
    JSONObject jsonObj;
    String JsonResponse = null;

    // Table Layout
    TableLayout tableLayout;
    Context context;

    String id_registro, fecha, tempRecord, dispositivo;

    // Service
    YolikanReceiver receiver;
    private BluetoothDevice mDevice;
    private UUID mDeviceUUID;
    String Preferencias_device = "";

    String Log_prueba = "";

    // connect to service

    //tiempo en espera
    TextView l_cronometro;
    String salida;
    int horas, min, seg, micros;
    boolean pausado;

    //tiempo en espera para enviar medicion
    String l_cronometro_server = "";
    String salida_server;
    int horas_server, min_server, seg_server, micros_server;
    boolean pausado_server;


    boolean tempPrimera = true;

    private enum Actions {
        START
    }

    private View view;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        if (getArguments() != null) {

            mDevice = getArguments().getParcelable("device"); //getActivity().getIntent().getParcelableExtra("device");
            device_name = mDevice.getName();
            //Log.d("Yulis_Device", mDevice.toString());
        }


        //mDevice = getArguments().getParcelable("device");

        /*try {
            if (mDevice == null) {
                Log.d("Yulis_Device", "Es null");
                mDevice = getArguments().getParcelable("device"); //getActivity().getIntent().getParcelableExtra("device");
                device_name = mDevice.getName();
                Log.d("Yulis_Device", mDevice.toString());
            }

        } catch (Exception e) {
            e.printStackTrace();
            Log.d("Yulis","Entro al catch");
        }*/

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.activity_visor_temperatura, container, false);

        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        //obtenemos el device

        //mDevice = getArguments().getParcelable("device");
        //Log.d("Yulis", mDevice.toString());
        //mDevice = deviceP; // getActivity().getIntent().getParcelableExtra("device");
        mDeviceUUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");

        //((TestMenuActivity) getActivity()).changeMenu();

        grados = view.findViewById(R.id.grados);
        txtEspera = view.findViewById(R.id.txtEspera);
        txtEspera.setVisibility(View.GONE);


        btn_close = view.findViewById(R.id.imageView);
        btn_config = view.findViewById(R.id.btn_config);
        tv_temperatura = view.findViewById(R.id.tv_temperatura);

        tv_mensaje_temp = view.findViewById(R.id.tv_mensaje_temp);
        tv_usuario = view.findViewById(R.id.tv_usuario);
        llayout = view.findViewById(R.id.liyimag);
        loader = view.findViewById(R.id.layoutLoading);
        promedio = view.findViewById(R.id.promedio);
        txt_arreglo = view.findViewById(R.id.txt_arreglo);

        promedio.setText("No enviando");

        tempView = view.findViewById(R.id.constraint_layout);
        tempView.setBackgroundColor(ContextCompat.getColor(this.getActivity(), R.color.fondo));

        btn_config.setOnClickListener(this);


        //cargamos datos del usuario que tiene sesion activa
        String datosUser = CargarDatosUsuario();
        Log.i("Yulis---", datosUser);

        try {

            JSONObject DatosUsuario = new JSONObject(datosUser);
            Log.i("Yulis---", DatosUsuario.getString("auth_user_data"));

            JSONObject messageJsonPrueba = new JSONObject(DatosUsuario.getString("auth_user_data").toString());

            auth_user_id = messageJsonPrueba.getString("auth_user_id");
            String first_name = messageJsonPrueba.getString("first_name");
            String last_name = messageJsonPrueba.getString("last_name");

            tv_usuario.setText(first_name + " " + last_name);

        } catch (JSONException e) {
            e.printStackTrace();
        }


        //l_cronometro = view.findViewById(R.id.crono);
        //l_cronometro.setText("00:00:00");
        l_cronometro_server = "";
        tempPrimera = true;

        tv_mensaje_temp.setText("- -");

        txtEspera.setVisibility(View.VISIBLE);
        llayout.setBackground(null);
        loader.setVisibility(View.VISIBLE);
        tv_temperatura.setVisibility(View.GONE);
        grados.setVisibility(View.GONE);

        horas = 0;
        min = 0;
        seg = 0;
        micros = 0;
        pausado = false;

        horas_server = 0;
        min_server = 0;
        seg_server = 0;
        micros_server = 0;
        pausado_server = false;


        chronometer = view.findViewById(R.id.chronometer);
        chronometer.setFormat("Time: %s");
        chronometer.setBase(SystemClock.elapsedRealtime());

        chronometer.setOnChronometerTickListener(new Chronometer.OnChronometerTickListener() {
            @Override
            public void onChronometerTick(Chronometer chronometer) {
                if ((SystemClock.elapsedRealtime() - chronometer.getBase()) >= 3540000) {
                    pauseChronometer();
                    //chronometer.setBase(SystemClock.elapsedRealtime());
                    //Toast.makeText(MainActivity.this, "Bing!", Toast.LENGTH_SHORT).show();
                }
                String chronoText = chronometer.getText().toString();
                String valueTemperatura = chronoText.replace("Time:", "");

                if(valueTemperatura.trim().equals("59:00")){
                    //Log.d("Yulis", "Llego al tiempo...." + valueTemperatura);
                }

            }
        });

        ClearDatoTemperatura();

        //b_start();
        //b_start_server();
        startChronometer();

        context = getActivity().getApplicationContext();


        Intent yolikanIntent = new Intent(getActivity(), YolikanService.class);
        yolikanIntent.setAction(Actions.START.name());
        yolikanIntent.putExtra("DEVICE_ID", mDevice);
        getActivity().startService(yolikanIntent);

        IntentFilter filter = new IntentFilter();
        filter.addAction(YolikanService.ACTION_TEMPERATURA);
        filter.addAction(YolikanService.ACTION_ESTATUS);
        YolikanReceiver rcv = new YolikanReceiver();
        getActivity().registerReceiver(rcv, filter);


        return view;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.imageView:
                //terminarLectura();
                break;
            case R.id.btn_config:
                terminarLectura();
                break;
        }
    }

    public void startChronometer() {
        if (!running) {
            chronometer.setBase(SystemClock.elapsedRealtime() - pauseOffset);
            chronometer.start();
            running = true;
        }
    }
    public void pauseChronometer() {
        if (running) {
            chronometer.stop();
            pauseOffset = SystemClock.elapsedRealtime() - chronometer.getBase();
            running = false;
        }
    }
    public void resetChronometer() {
        chronometer.setBase(SystemClock.elapsedRealtime());
        pauseOffset = 0;
    }

    public class YolikanReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {

            //Log.d("Yulis", "action: " + intent.getAction());
            //Log.d("Yulis", "Actualiza temperatura");
            //start();

            if (intent.getAction().equals(YolikanService.ACTION_TEMPERATURA)) {

                String temperatura = intent.getStringExtra("temperatura");

                assert temperatura != null;
                //Log.d("Yulis", "temp entrante: " + temperatura);

                if (temperatura.equals("actualiza")) {
                    //Log.d("Yulis", "temp actualiza lista: " + temperatura);

                } else {

                    //Log.d("Yulis", "l_cronometro_server: " + l_cronometro_server);

                    int stoppedMilliseconds = 0;

                    String chronoText = chronometer.getText().toString();
                    String valueTemperatura = chronoText.replace("Time:", "");

                    //Log.d("Yulis", "l_cronometro: " + valueTemperatura.trim());

                    l_cronometro_server = valueTemperatura.trim();

                    //Log.d("Yulis", "Temperatura_: " + String.format("%.1f", Double.valueOf(temperatura)));

                    if (tempPrimera == true) {

                        if (Double.valueOf(temperatura) >= 33.0) {

                            //no enviamos dato al server
                            ///System.out.println("Enviamos primer dato al server");
                            //GuardarTemperaturaAnterior(String.format("%.1f", Double.valueOf(temperatura)));
                            GuardarTempSendServer(String.format("%.1f", Double.valueOf(temperatura)));
                            guardaRegistro(Double.valueOf(temperatura));
                            tempPrimera = false;

                            ResetTime();

                        }
                    }

                    double n = 0;
                    double promedio = 0;


                    //guadarmos el dato en el arreglo
                    notas.add(Float.parseFloat(temperatura));

                    //Log.d("CIDESI", "Total array: " + notas.size());

                    //se valida si el arreglo esta con 9 posiciones o menos
                    if (notas.size() == Configuration.INTERVALO_PROMEDIO) {
                        //Log.d("CIDESI", "Arreglo igual a 9");
                        //Log.d("CIDESI", "- - - - - - - - ");

                        Iterator t = notas.iterator();
                        while (t.hasNext())
                            ///System.out.println(t.next());

                        //sacamos promedio
                        //Log.d("******", "- - - - - - - -");

                        //double n = 0;
                        //double promedio = 0;
                        for (int a = 0; a < notas.size(); a++) {
                            n += Double.parseDouble(notas.get(a).toString());
                            promedio = n / notas.size();
                        }

                        /*///System.out.println("El promedio a enviar: " + String.format("%.1f", promedio));
                        ///System.out.println("valor n: " + n);
                        ///System.out.println("size: " + notas.size());
                        ///System.out.println("arreglo: " + notas);*/

                        String temperature1 = String.format("%.1f", promedio);
                        double doble1 = Double.parseDouble(temperature1);


                        Log_prueba = "Arreglo" + notas + "-----" + "El promedio: " + doble1;


                        //se elimina la primer posicion
                        notas.remove(0);

                        //validamos el promedio para enviarlo al servidor
                        String temperature = String.format("%.1f", promedio);
                        double doble = Double.parseDouble(temperature);

                        algoritmoTem(doble);
                        //algoritmoTemP(doble);


                    } else {
                        //Log.d("CIDESI", "Arreglo menor a 9");
                        //Log.d("CIDESI", "- - - - - - - - ");

                        Iterator t = notas.iterator();
                        while (t.hasNext())
                            ///System.out.println(t.next());

                        //sacamos promedio
                        //Log.d("******", "- - - - - - - -");

                        //double n = 0;
                        //double promedio = 0;
                        for (int a = 0; a < notas.size(); a++) {
                            n += Double.parseDouble(notas.get(a).toString());
                            promedio = n / notas.size();
                        }

                        /*///System.out.println("El promedio a enviar: " + String.format("%.1f", promedio));
                        ///System.out.println("valor n: " + n);
                        ///System.out.println("size: " + notas.size());
                        ///System.out.println("arreglo: " + notas);*/

                        String temperature1 = String.format("%.1f", promedio);
                        double doble1 = Double.parseDouble(temperature1);


                        Log_prueba = "Arreglo" + notas + "-----" + "El promedio: " + doble1;

                        //txt_arreglo.setText(Log_prueba);

                        //validamos el promedio para enviarlo al servidor
                        String temperature = String.format("%.1f", promedio);
                        double doble = Double.parseDouble(temperature);

                        algoritmoTem(doble);
                        //algoritmoTemP(doble);

                    }

                    String temperature = String.format("%.1f", promedio);
                    double doble = Double.parseDouble(temperature);

                    temp_format = doble; //Float.parseFloat(String.valueOf(promedio));

                    //Log.d("PROMEDIO", String.valueOf(temp_format));

                    //loader.setVisibility(View.GONE);

                    if (temp_format <= 32.9) {

                        txtEspera.setVisibility(View.VISIBLE);
                        tv_temperatura.setVisibility(View.GONE);
                        grados.setVisibility(View.GONE);
                        loader.setVisibility(View.VISIBLE);
                        llayout.setBackground(null);

                        tempView.setBackgroundColor(ContextCompat.getColor(getActivity().getApplicationContext(), R.color.fondo));
                        //tv_temperatura.setText(String.format("%.1f", Double.valueOf(temperatura)));
                        tv_temperatura.setText(temperature);
                        //llayout.setBackgroundColor(getResources().getColor(R.color.colorId));
                        //llayout.setBackground(getResources().getDrawable(R.drawable.temp_baja));
                        tv_mensaje_temp.setText("- -");
                        //tv_mensaje_emergencia.setVisibility(View.INVISIBLE);
                    }

                    if (temp_format >= 33.0 && temp_format <= 36.1) {
                        txtEspera.setVisibility(View.GONE);
                        tv_temperatura.setVisibility(View.VISIBLE);
                        grados.setVisibility(View.VISIBLE);
                        loader.setVisibility(View.GONE);

                        tempView.setBackgroundColor(ContextCompat.getColor(getActivity().getApplicationContext(), R.color.fondo));
                        //tv_temperatura.setText(String.format("%.1f", Double.valueOf(temperatura)));
                        tv_temperatura.setText(temperature);
                        //llayout.setBackgroundColor(getResources().getColor(R.color.colorId));
                        llayout.setBackground(getResources().getDrawable(R.drawable.temp_baja));
                        tv_mensaje_temp.setText("Temperatura Baja");
                        //tv_mensaje_emergencia.setVisibility(View.INVISIBLE);
                    }

                    if (temp_format >= 36.2 && temp_format < 37.2) {
                        txtEspera.setVisibility(View.GONE);
                        tv_temperatura.setVisibility(View.VISIBLE);
                        grados.setVisibility(View.VISIBLE);
                        loader.setVisibility(View.GONE);

                        tempView.setBackgroundColor(ContextCompat.getColor(getActivity().getApplicationContext(), R.color.fondo));
                        //tv_temperatura.setText(String.format("%.1f", Double.valueOf(temperatura)));
                        tv_temperatura.setText(temperature);
                        llayout.setBackground(getResources().getDrawable(R.drawable.tem_normal));
                        tv_mensaje_temp.setText("Temperatura Normal");
                        //tv_mensaje_emergencia.setVisibility(View.INVISIBLE);
                    }
                    if (temp_format >= 37.3 && temp_format <= 38.9) {
                        txtEspera.setVisibility(View.GONE);
                        tv_temperatura.setVisibility(View.VISIBLE);
                        grados.setVisibility(View.VISIBLE);
                        loader.setVisibility(View.GONE);

                        tempView.setBackgroundColor(ContextCompat.getColor(getActivity().getApplicationContext(), R.color.fondo));
                        //tv_temperatura.setText(String.format("%.1f", Double.valueOf(temperatura)));
                        tv_temperatura.setText(temperature);
                        llayout.setBackground(getResources().getDrawable(R.drawable.temp_media));
                        tv_mensaje_temp.setText("Temperatura Alta");
                        //tv_mensaje_emergencia.setVisibility(View.INVISIBLE);
                    }
                    if (temp_format >= 39.0) {
                        txtEspera.setVisibility(View.GONE);
                        tv_temperatura.setVisibility(View.VISIBLE);
                        grados.setVisibility(View.VISIBLE);
                        loader.setVisibility(View.GONE);

                        tempView.setBackgroundColor(ContextCompat.getColor(getActivity().getApplicationContext(), R.color.fondo));
                        //tv_temperatura.setText(String.format("%.1f", Double.valueOf(temperatura)));
                        tv_temperatura.setText(temperature);
                        llayout.setBackground(getResources().getDrawable(R.drawable.temp_alta));
                        tv_mensaje_temp.setText("Temperatura muy Alta");
                        //tv_mensaje_emergencia.setVisibility(View.VISIBLE);
                    }
                }

            } else if (intent.getAction().equals(YolikanService.ACTION_TERMINAR_SERVICIO)) {
                String motivo = intent.getStringExtra("motivo");
                Toast.makeText(getActivity().getApplicationContext(), motivo, Toast.LENGTH_LONG).show();
                Intent cerrar = new Intent(getActivity().getApplicationContext(), MainActivity.class);
                startActivity(cerrar);
                getActivity().finish();
            } else if (intent.getAction().equals(YolikanService.ACTION_ACTUALIZA_REGISTROS)) {
                //Log.d("Yulis", "Actualiza registros");
                //String motivo = intent.getStringExtra("actualiza");
                //Log.d(Configuration.TAG, "actualiza: " + motivo);
                //showRegisters2();
            } else if (intent.getAction().equals(YolikanService.ACTION_AVISO_GPS)) {
                showGPSDisabledAlertToUser();
            }
        }
    }

    private void algoritmoTem(double Temperatura) {

        double DiferenciaTemperaturas = 0;
        double TemperaturaActual = Temperatura;

        String recupTemAnterior = CargarTempSendServer(); ///CargarTemperaturaAnterior();
        double TemperaturaAnterior = Double.parseDouble(recupTemAnterior);

        /*Log.d("CIDESI", "+ + + + + + ");

        Log.d("CIDESI", "Temperatura Actual: " + TemperaturaActual);
        Log.d("CIDESI", "Temperatura Anterior: " + TemperaturaAnterior);
        Log.d("CIDESI", "Diferencia entre TemAct y TemAnt: " + String.format("%.1f", (TemperaturaActual - TemperaturaAnterior)));

        Log.d("CIDESI", "+ + + + + + ");*/

        double DTem = TemperaturaActual - TemperaturaAnterior;
        String tempD = String.format("%.1f", DTem);
        double doble1 = Double.parseDouble(tempD);
        if (doble1 < 0) { //Todo numero que sea menor que cero, quiere decir que es negativo
            //a=a*-1;
            doble1 = doble1 * -1;
        }

        txt_arreglo.setText(Log_prueba + " -- " + "Temperatura Act: " + TemperaturaActual + " -- " + "Temperatura Anterior Server: " + TemperaturaAnterior + " -- " + "Diferencia/TemAct-TemAntServer: " + String.format("%.1f", (TemperaturaActual - TemperaturaAnterior)) + " -- Positivo " + doble1);


        if (TemperaturaAnterior == 0) {

            //Log.d("CIDESI", "TemperaturaAnterior igual a 0");
            //guardamos la temperatura anterior
            //GuardarTemperaturaAnterior(String.format("%.1f", TemperaturaActual));

            double DiferenciaTem = TemperaturaActual - TemperaturaAnterior;
            String temperature = String.format("%.1f", DiferenciaTem);
            double doble = Double.parseDouble(temperature);
            if (doble < 0) { //Todo numero que sea menor que cero, quiere decir que es negativo
                //a=a*-1;
                doble = doble * -1;
            }

            DiferenciaTemperaturas = doble; //(double)Math.round((TemperaturaActual - TemperaturaAnterior) * 100d) / 100d;
            //Log.d("CIDESI", "DIFERENCIA: " + DiferenciaTemperaturas);

            if (TemperaturaActual < 33.0) {
                //temperatura actual menor a 33 no se envia al server
                //Log.d("CIDESI", "TemperaturaActual menor 33.0");

            } else if (TemperaturaActual >= 33.1 && TemperaturaActual <= 35.0) {
                //Log.d("CIDESI", "TemperaturaActual mayor igual a 33.1 && TemperaturaActual menor igual a 35.0");

                if (DiferenciaTemperaturas >= 1.0) {
                    //Log.d("CIDESI", "DiferenciaTemperaturas mayor a 1.0 SI");

                    //transmitimos el dato
                    guardaRegistro(TemperaturaActual);

                    //limpiamos datos
                    ClearDatoTemperaturaServer();

                    //guardamos temperatura enviada
                    GuardarTempSendServer(String.format("%.1f", TemperaturaActual));

                    //Reiniciamos el tiempo a 0
                    ResetTime();

                } else {
                    //Log.d("CIDESI", "DiferenciaTemperaturas mayor a 1.0 NO");
                    //si no validamos si el tiempo transcurrido es igual a 1 hora
                    if (l_cronometro_server.equals(Configuration.TIME_SEND)) {
                        //Log.d("CIDESI", "Tiempo transcurrido es igual a 1 hora SI");

                        //transmitimos el dato
                        guardaRegistro(TemperaturaActual);

                        //limpiamos datos
                        ClearDatoTemperaturaServer();

                        //guardamos temperatura enviada
                        GuardarTempSendServer(String.format("%.1f", TemperaturaActual));

                        //reiniciamos el tiempo a 0
                        ResetTime();

                        //se pinta el dato en pantalla

                    } else {
                        //Log.d("CIDESI", "Tiempo transcurrido es igual a 1 hora NO");
                        // No se envia el dato
                        // solo se pinta en pantalla
                    }
                }

            } else if (TemperaturaActual >= 35.1 && TemperaturaActual <= 38.0) {
                //Log.d("CIDESI", "TemperaturaActual mayor igual a 35.1 && TemperaturaActual menor igual a 38.0");

                if (DiferenciaTemperaturas >= 0.5) {
                    //Log.d("CIDESI", "DiferenciaTemperaturas mayor a 0.5 SI");

                    //transmitimos el dato
                    guardaRegistro(TemperaturaActual);

                    //limpiamos datos
                    ClearDatoTemperaturaServer();

                    //guardamos temperatura enviada
                    GuardarTempSendServer(String.format("%.1f", TemperaturaActual));

                    //reiniciamos el tiempo a 0
                    ResetTime();


                } else {
                    //Log.d("CIDESI", "DiferenciaTemperaturas mayor a 0.5 NO");
                    //si no validamos si el tiempo transcurrido es igual a 1 hora
                    if (l_cronometro_server.equals(Configuration.TIME_SEND)) {
                        ///Log.d("CIDESI", "Tiempo transcurrido es igual a 1 hora SI");

                        //transmitimos el dato
                        guardaRegistro(TemperaturaActual);

                        //limpiamos datos
                        ClearDatoTemperaturaServer();

                        //guardamos temperatura enviada
                        GuardarTempSendServer(String.format("%.1f", TemperaturaActual));

                        //reiniciamos el tiempo a 0
                        ResetTime();

                        //se pinta el dato en pantalla

                    } else {
                        //Log.d("CIDESI", "Tiempo transcurrido es igual a 1 hora NO");
                        //No se envia el dato y solo se pinta en pantalla
                    }
                }

            } else if (TemperaturaActual >= 38.1) {
                //Log.d("CIDESI", "TemperaturaActual mayor 38.1");

                if ((double) Math.round(DiferenciaTemperaturas * 100d) / 100d >= 0.2) {
                    ///Log.d("CIDESI", "DiferenciaTemperaturas mayor a 0.2 SI");

                    //transmitimos el dato
                    guardaRegistro(TemperaturaActual);

                    //limpiamos datos
                    ClearDatoTemperaturaServer();

                    //guardamos temperatura enviada
                    GuardarTempSendServer(String.format("%.1f", TemperaturaActual));

                    //reiniciamos el tiempo a 0
                    ResetTime();

                } else {
                    //Log.d("CIDESI", "DiferenciaTemperaturas mayor a 0.2 NO");
                    //si no validamos si el tiempo transcurrido es igual a 1 hora
                    if (l_cronometro_server.equals(Configuration.TIME_SEND)) {
                        ///Log.d("CIDESI", "Tiempo transcurrido es igual a 1 hora SI");

                        //transmitimos el dato
                        guardaRegistro(TemperaturaActual);

                        //limpiamos datos
                        ClearDatoTemperaturaServer();

                        //guardamos temperatura enviada
                        GuardarTempSendServer(String.format("%.1f", TemperaturaActual));

                        //reiniciamos el tiempo a 0
                        ResetTime();

                        //se pinta el dato en pantalla

                    } else {
                        //Log.d("CIDESI", "Tiempo transcurrido es igual a 1 hora NO");
                        //No se envia el dato y solo se pinta en pantalla
                    }
                }

            }


        } else {
            ///Log.d("CIDESI", "TemperaturaAnterior diferente a 0");

            //guadamos el nuevo valor de la temperatura actual
            //ClearDatoTemperatura();
            //guardamos la temperatura registrada en las propertys
            //GuardarTemperaturaAnterior(String.valueOf(TemperaturaActual));

            double DiferenciaTem = TemperaturaActual - TemperaturaAnterior;
            String temperature = String.format("%.1f", DiferenciaTem);
            double doble = Double.parseDouble(temperature);
            if (doble < 0) { //Todo numero que sea menor que cero, quiere decir que es negativo
                //a=a*-1;
                doble = doble * -1;
            }

            DiferenciaTemperaturas = doble; //(double)Math.round((TemperaturaActual - TemperaturaAnterior) * 100d) / 100d;
            //Log.d("CIDESI", "DIFERENCIA: " + DiferenciaTemperaturas);


            if (TemperaturaActual < 33.0) {
                //temperatura actual menor a 33 no se envia al server
                ///Log.d("CIDESI", "TemperaturaActual menor 33.0");

            } else if (TemperaturaActual >= 33.1 && TemperaturaActual <= 35.0) {
                //Log.d("CIDESI", "TemperaturaActual mayor igual a 33.1 && TemperaturaActual menor igual a 35.0");

                if (DiferenciaTemperaturas >= 1.0) {
                    ///Log.d("CIDESI", "DiferenciaTemperaturas mayor a 1.0 SI");

                    //limpiamos datos
                    ClearDatoTemperaturaServer();

                    //transmitimos el dato
                    guardaRegistro(TemperaturaActual);

                    //guardamos temperatura enviada
                    GuardarTempSendServer(String.format("%.1f", TemperaturaActual));

                    //Reiniciamos el tiempo a 0
                    ResetTime();

                } else {
                    ///Log.d("CIDESI", "DiferenciaTemperaturas mayor a 1.0 NO");
                    //si no validamos si el tiempo transcurrido es igual a 1 hora
                    if (l_cronometro_server.equals(Configuration.TIME_SEND)) {
                        ///Log.d("CIDESI", "Tiempo transcurrido es igual a 1 hora SI");

                        //transmitimos el dato
                        guardaRegistro(TemperaturaActual);

                        //limpiamos datos
                        ClearDatoTemperaturaServer();

                        //guardamos temperatura enviada
                        GuardarTempSendServer(String.format("%.1f", TemperaturaActual));

                        //reiniciamos el tiempo a 0
                        ResetTime();

                        //se pinta el dato en pantalla

                    } else {
                        //Log.d("CIDESI", "Tiempo transcurrido es igual a 1 hora NO");
                        // No se envia el dato
                        // solo se pinta en pantalla
                    }
                }

            } else if (TemperaturaActual >= 35.1 && TemperaturaActual <= 38.0) {
                //Log.d("CIDESI", "TemperaturaActual mayor igual a 35.1 && TemperaturaActual menor igual a 38.0");

                if (DiferenciaTemperaturas >= 0.5) {
                    //Log.d("CIDESI", "DiferenciaTemperaturas mayor a 0.5 SI");

                    //transmitimos el dato
                    guardaRegistro(TemperaturaActual);

                    //limpiamos datos
                    ClearDatoTemperaturaServer();

                    //guardamos temperatura enviada
                    GuardarTempSendServer(String.format("%.1f", TemperaturaActual));

                    //reiniciamos el tiempo a 0
                    ResetTime();

                } else {
                    //Log.d("CIDESI", "DiferenciaTemperaturas mayor a 0.5 NO");
                    //si no validamos si el tiempo transcurrido es igual a 1 hora
                    if (l_cronometro_server.equals(Configuration.TIME_SEND)) {
                        ///Log.d("CIDESI", "Tiempo transcurrido es igual a 1 hora SI");

                        //transmitimos el dato
                        guardaRegistro(TemperaturaActual);

                        //limpiamos datos
                        ClearDatoTemperaturaServer();

                        //guardamos temperatura enviada
                        GuardarTempSendServer(String.format("%.1f", TemperaturaActual));

                        //reiniciamos el tiempo a 0
                        ResetTime();

                        //se pinta el dato en pantalla

                    } else {
                        //Log.d("CIDESI", "Tiempo transcurrido es igual a 1 hora NO");
                        //No se envia el dato y solo se pinta en pantalla
                    }
                }

            } else if (TemperaturaActual >= 38.1) {
                ///Log.d("CIDESI", "TemperaturaActual mayor 38.1");

                if ((double) Math.round(DiferenciaTemperaturas * 100d) / 100d >= 0.2) {
                    //Log.d("CIDESI", "DiferenciaTemperaturas mayor a 0.2 SI");

                    //transmitimos el dato
                    guardaRegistro(TemperaturaActual);

                    //limpiamos datos
                    ClearDatoTemperaturaServer();

                    //guardamos temperatura enviada
                    GuardarTempSendServer(String.format("%.1f", TemperaturaActual));

                    //reiniciamos el tiempo a 0
                    ResetTime();

                } else {
                    //Log.d("CIDESI", "DiferenciaTemperaturas mayor a 0.2 NO");
                    //si no validamos si el tiempo transcurrido es igual a 1 hora
                    if (l_cronometro_server.equals(Configuration.TIME_SEND)) {
                        //Log.d("CIDESI", "Tiempo transcurrido es igual a 1 hora SI");

                        //transmitimos el dato
                        guardaRegistro(TemperaturaActual);

                        //limpiamos datos
                        ClearDatoTemperaturaServer();

                        //guardamos temperatura enviada
                        GuardarTempSendServer(String.format("%.1f", TemperaturaActual));

                        //reiniciamos el tiempo a 0
                        ResetTime();

                        //se pinta el dato en pantalla

                    } else {
                        //Log.d("CIDESI", "Tiempo transcurrido es igual a 1 hora NO");
                        //No se envia el dato y solo se pinta en pantalla
                    }
                }

            }
        }

    }


    public void ResetTime() {
        //Log.d("CORCHADO", "REINICIO DE TIEMPO...: ");
        resetChronometer();
        startChronometer();


    }

    private void showGPSDisabledAlertToUser() {
        AlertDialog.Builder builder = new AlertDialog.Builder(FragmentServiceActivity.this.getActivity());
        builder.setMessage("El GPS de tu dispositivo está apagado. La aplicación utiliza los datos " +
                "de geolocalización para fines estadísticos - analíticos. ¿Desea habilitarlo?")
                .setCancelable(false)
                .setPositiveButton("Activar GPS", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivity(intent);
                    }
                })
                .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    public void onStart() {
        super.onStart();
    }

    private void terminarLectura() {
        new LovelyStandardDialog(FragmentServiceActivity.this.getActivity(), LovelyStandardDialog.ButtonLayout.VERTICAL)
                .setTopColorRes(R.color.colorAccent)
                .setButtonsColorRes(R.color.colorPrimaryDark)
                //.setIcon(R.drawable.ic_icon_yolikan)
                .setTitle("Yolikan")
                .setMessage("¿Desea desconectar el dispositivo?")
                .setPositiveButton(android.R.string.ok, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        terminaServicio();

                    }
                })
                .setNegativeButton(android.R.string.no, null)
                .show();
    }


    private void terminaServicio() {

        YolikanService.killService(FragmentServiceActivity.this.getActivity(), new FragmentServiceActivity.AccountInfoResultReceiver(getActivity()));
        ((TestMenuActivity) getActivity()).changeMenuListDisp();
        PageFragment pageFragment = new PageFragment();
        //guardar datos device

        FragmentManager manager = getFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.add(R.id.container, pageFragment, "Prueba");
        transaction.addToBackStack(null);
        transaction.attach(pageFragment);
        transaction.commit();
    }

    // CONNECT TO SERVICE
    public class AccountInfoResultReceiver implements YolikanResultReceiver.ResultReceiverCallBack<Integer> {
        private WeakReference<FragmentActivity> activityRef;

        public AccountInfoResultReceiver(FragmentActivity activity) {
            activityRef = new WeakReference<FragmentActivity>(activity);
        }

        @Override
        public void onSuccess(Integer data) {
            if (activityRef != null && activityRef.get() != null) {
                ///activityRef.get().label.setText("Your balance: "+data);
            }
        }

        @Override
        public void onError(Exception exception) {
            ///activityRef.get().showMessage("Account info failed");
        }
    }

    private String CargarDatosUsuario() {
        SharedPreferences preferencias = getActivity().getSharedPreferences("usuario", Context.MODE_PRIVATE);
        String Servicio = preferencias.getString("DatosUsuario", "No hay datos");
        Preferencias_datosUsuario = Servicio;
        return Servicio;
    }

    private void GuardarTempSendServer(String Dato) {
        SharedPreferences preferencias = getActivity().getSharedPreferences("temperatura", Context.MODE_PRIVATE);
        String DatoTem = Dato;
        SharedPreferences.Editor editor = preferencias.edit();
        editor.putString("DatoTemperatura", DatoTem);
        editor.commit();
    }

    private String CargarTempSendServer() {
        SharedPreferences preferencias = getActivity().getSharedPreferences("temperatura", Context.MODE_PRIVATE);
        String Servicio = preferencias.getString("DatoTemperatura", "0");
        Preferencias_datosUsuario = Servicio;
        return Servicio;
    }

    public void ClearDatoTemperatura() {
        SharedPreferences preferenciasSA = getActivity().getSharedPreferences("temperatura", Context.MODE_PRIVATE);
        SharedPreferences.Editor edito = preferenciasSA.edit();
        edito.clear().apply();
        edito.commit();
    }

    public void ClearDatoTemperaturaServer() {
        SharedPreferences preferenciasSA = getActivity().getSharedPreferences("temperatura", Context.MODE_PRIVATE);
        SharedPreferences.Editor edito = preferenciasSA.edit();
        edito.clear().apply();
        edito.commit();
    }

    public void guardaRegistro(double temp) {
        //Log.d("Yulis", "guardaRegistro: " + temp);
        String temperature = String.format("%.1f", temp);
        //Log.d("CIDESI", "Guardando... " + temperature);
        String datos_token = CargarToken();
        String datosUser = CargarDatosUsuario();
        //Log.d("CIDESI", "datosUser... " + datosUser);

        //Log.d("CIDESI", "Token... " + datos_token);

        final String[] respuesta = new String[1];

        try {

            //cargamos datos del usuario que tiene sesion activa
            JSONObject prueba = new JSONObject(datosUser);
            //Log.i("Yulis---", prueba.getString("auth_user_data"));

            JSONObject messageJsonPrueba = new JSONObject(prueba.getString("auth_user_data").toString());

            //auth_user_id = messageJsonPrueba.getString("auth_user_id");
            String username = messageJsonPrueba.getString("username");
            String first_name = messageJsonPrueba.getString("first_name");
            String last_name = messageJsonPrueba.getString("last_name");
            RequestQueue requestQueue = Volley.newRequestQueue(getActivity().getApplicationContext());

            final JSONObject jsonBody = new JSONObject();

            if (personaReg.equals("")) {
                personaReg = "0";
            }

            /**
             * {
             *   "device":"dev0001",
             * 	"measurement":32.00,
             * 	"gps_coordinates":"20.174610115203176, -100.13677041713382",
             * 	"patient_name":"Maria Yuliana Mata Corchado"
             * }
             *
             */

            jsonBody.put("device", device_name);
            jsonBody.put("measurement", temperature);
            jsonBody.put("gps_coordinates", "20.174610115203176, -100.13677041713382");
            jsonBody.put("patient_name", first_name + " " + last_name);

            final String requestBody = jsonBody.toString();
            //Log.d("CIDESI", "body guarda reg: " + requestBody);

            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, Configuration.HOST_RegistroMedicion, jsonBody, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    //Dialog.cancel();
                    //Log.i("Respuesta: ", response.toString());

                    //Log.i("CIDESI", "VOLLEY response guarda medicion: " + response);

                    String currentDateTimeString = DateFormat.getDateTimeInstance().format(new Date());
                    promedio.setText("- -");
                    promedio.setText("Dato enviado..." + currentDateTimeString);

                    //counter = 0;

                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    try {
                        //Dialog.cancel();

                        //Log.d("Login...", error.networkResponse.statusCode + "");

                        int estatus = error.networkResponse.statusCode;
                        String aString = Integer.toString(estatus);//403

                        //Log.d("Codigo...", aString);

                        switch (aString) {
                            case "401":
                                //Log.i("-------case 1-------", "Contraseña o usuario incorrectos.");
                                CharSequence text = "Contraseña o usuario incorrecto.";
                                Toast.makeText(getActivity().getApplicationContext(), text, Toast.LENGTH_LONG).show();
                                break;

                            /*case "403":
                                Log.i("-------case 2-------", "Sesión iniciada en otro dispositivo.");
                                CharSequence text1 = "Sesión iniciada en otro dispositivo.";
                                Toast.makeText(getApplicationContext(), text1, Toast.LENGTH_SHORT).show();
                                break;*/

                            default:
                                //Log.i("-------Default-------", "Error.");
                                CharSequence text2 = "Error de comunicación";
                                Toast.makeText(getActivity().getApplicationContext(), text2, Toast.LENGTH_LONG).show();
                        }

                    } catch (Exception e) {
                        e.getMessage();
                    }

                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("Content-Type", "application/json");
                    params.put("Api-Function", "setItemMobile()");
                    params.put("Authentication-Token", Preferencias_token);
                    //params.put("api-parameters","{}");
                    return params;
                }
            };
            requestQueue.add(request);

        } catch (JSONException e) {
            counter = 0;
            ///Log.e("CIDESI", "Error mediciones! " + e.getMessage());
            e.printStackTrace();
            counter = 0;
        }
    }

    private String CargarToken() {
        SharedPreferences preferencias = getActivity().getSharedPreferences("token", Context.MODE_PRIVATE);
        String Servicio = preferencias.getString("DatosToken", "No hay token");
        Preferencias_token = Servicio;
        return Servicio;
    }

    @Override
    public void onPause() {
        super.onPause();
        ///Log.d("Yulis", "onPause");
    }

    @Override
    public void onResume() {
        super.onResume();
        //Log.d("Yulis", "onResume");
    }

    @Override
    public void onStop() {
        super.onStop();
        //Log.d("Yulis", "onStop");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        //Log.d("Yulis", "onDestroy");

    }


}
