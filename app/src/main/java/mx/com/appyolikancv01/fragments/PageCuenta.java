package mx.com.appyolikancv01.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.yarolegovich.lovelydialog.LovelyStandardDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import mx.com.appyolikancv01.ActualizaPassword;
import mx.com.appyolikancv01.ActualizarCuentaActivity;
import mx.com.appyolikancv01.AvisoPrivacidadActivity;
import mx.com.appyolikancv01.Configuration;
import mx.com.appyolikancv01.MainActivity;
import mx.com.appyolikancv01.PoliticasUsoActivity;
import mx.com.appyolikancv01.R;
import mx.com.appyolikancv01.TerminoCondicionesActivity;
import mx.com.appyolikancv01.TestMenuActivity;

public class PageCuenta extends Fragment implements View.OnClickListener{

    private View view;
    Button btnEditar;
    TextView txtNombreP, txtEmailP, txtPhoneP;
    TextView lblAviso, lblPoliticas, lblTerminos, lblCambioPassword, lblCerrarSesion, lblEliminarCuenta,lblManualU,lblUsabilidad;
    //variable para las preferencias
    String Preferencias_datosUsuario = "";
    String Preferencias_token = "";
    String Preferencias_Password = "";
    String auth_user_id = "";

    private ProgressDialog Dialog;
    AlertDialog.Builder alertDialogBuilder;
    AlertDialog textDialog;
    final Handler handler = new Handler();
    AlertDialog.Builder builder;

    public PageCuenta() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        view = inflater.inflate(R.layout.activity_profile, container, false);
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);


        //extView textView = (TextView) view.findViewById(R.id.text_view);

        btnEditar = view.findViewById(R.id.btnEditarP);
        txtNombreP = view.findViewById(R.id.txtNombreP);
        txtEmailP = view.findViewById(R.id.txtEmailP);
        txtPhoneP = view.findViewById(R.id.txtPhoneP);

        lblAviso = view.findViewById(R.id.lblAvisoP);
        lblPoliticas = view.findViewById(R.id.lblPoliticasP);
        lblTerminos = view.findViewById(R.id.lblTerminosP);
        lblCambioPassword = view.findViewById(R.id.lblCambioPasswordP);
        lblCerrarSesion = view.findViewById(R.id.lblCerrarSesion);
        lblEliminarCuenta = view.findViewById(R.id.lblEliminarCuenta);
        //lblManualU = view.findViewById(R.id.lblManualU);
        lblUsabilidad = view.findViewById(R.id.lblUsabilidad);

        btnEditar.setOnClickListener(this);
        lblAviso.setOnClickListener(this);
        lblPoliticas.setOnClickListener(this);
        lblTerminos.setOnClickListener(this);
        lblCambioPassword.setOnClickListener(this);
        lblCerrarSesion.setOnClickListener(this);
        lblEliminarCuenta.setOnClickListener(this);
        //lblManualU.setOnClickListener(this);
        lblUsabilidad.setOnClickListener(this);


        //cargamos datos del usuario que tiene sesion activa
        String datosUser = CargarDatosUsuario();
        String datos_token = CargarToken();
        //Log.i("Yulis---", datos_token);
        //Log.i("Yulis---", datosUser);

        try {

            JSONObject prueba = new JSONObject(datosUser);
            //Log.i("Yulis---", prueba.getString("auth_user_data"));

            JSONObject messageJsonPrueba = new JSONObject(prueba.getString("auth_user_data").toString());

            auth_user_id = messageJsonPrueba.getString("auth_user_id");
            String first_name = messageJsonPrueba.getString("first_name");
            String last_name = messageJsonPrueba.getString("last_name");
            String email = messageJsonPrueba.getString("email");
            String phone = messageJsonPrueba.getString("phone");

            txtNombreP.setText(first_name + " " + last_name);
            txtEmailP.setText(email);
            txtPhoneP.setText(phone);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return view;
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnEditarP:
                //Toast.makeText(getActivity().getApplicationContext(), "prueba", Toast.LENGTH_LONG).show();
                Intent intent = new Intent(PageCuenta.this.getActivity(), ActualizarCuentaActivity.class);
                startActivity(intent);
                break;
            case R.id.lblAvisoP:
                //Toast.makeText(getActivity().getApplicationContext(), "aviso", Toast.LENGTH_LONG).show();
                Intent intentslblAviso = new Intent(PageCuenta.this.getActivity(), AvisoPrivacidadActivity.class);
                startActivity(intentslblAviso);
                break;
            case R.id.lblPoliticasP:
                //Toast.makeText(getActivity().getApplicationContext(), "politicas", Toast.LENGTH_LONG).show();
                Intent intentlblPoliticas = new Intent(PageCuenta.this.getActivity(), PoliticasUsoActivity.class);
                startActivity(intentlblPoliticas);
                break;
            case R.id.lblTerminosP:
                //Toast.makeText(getActivity().getApplicationContext(), "terminos", Toast.LENGTH_LONG).show();
                Intent intentlblTermins = new Intent(PageCuenta.this.getActivity(), TerminoCondicionesActivity.class);
                startActivity(intentlblTermins);
                break;
            case R.id.lblCambioPasswordP:
                Intent intentlbllblCambioPassword = new Intent(PageCuenta.this.getActivity(), ActualizaPassword.class);
                startActivity(intentlbllblCambioPassword);
                break;
            case R.id.lblCerrarSesion:
                Intent intentCerrar = new Intent(PageCuenta.this.getActivity(), MainActivity.class);
                PageCuenta.this.getActivity().finish();
                startActivity(intentCerrar);
                ClearDatosToken();
                ClearDatosUsuario();
                ClearPassword();
                BorrarSesion();
                //limpiar datos del usuario de las propertys

                break;
            case R.id.lblEliminarCuenta:
                EliminarCuenta();
                BorrarSesion();
                break;

            /*case R.id.lblManualU:
                String url = "https://drive.google.com/file/d/193Nq6GkfHjgCz4xaVlpqET9EKyhJWqJ9/view?usp=sharing";
                Uri uri = Uri.parse(url);
                Intent intentLink = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intentLink);
                break;*/
            case R.id.lblUsabilidad:
                String urlpag = "https://yolikan.com/ticket/";
                Uri urii = Uri.parse(urlpag);
                Intent intentLinks = new Intent(Intent.ACTION_VIEW, urii);
                startActivity(intentLinks);
                break;
        }
    }



    public void EliminarCuenta() {

        AlertDialog.Builder builder = new AlertDialog.Builder(
                PageCuenta.this.getActivity());


        //if (builder == null) {

        builder.setCancelable(false);
        builder.setTitle("Eliminar cuenta");
        builder.setMessage("¿Esta seguro que desea eliminar cuenta?");
        builder.setNegativeButton("Cancelar",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,
                                        int which) {
                        Toast.makeText(getActivity().getApplicationContext(), "cancalar", Toast.LENGTH_LONG).show();
                    }
                });
        builder.setPositiveButton("Aceptar",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        //Toast.makeText(getActivity().getApplicationContext(), "aceptar", Toast.LENGTH_LONG).show();

                        int idUsuario = Integer.parseInt(auth_user_id);
                        EliminarCuenta(idUsuario);

                    }
                });
        builder.show();
    }


    public void EliminarCuenta(Integer id) {

        RequestQueue requestQueue = Volley.newRequestQueue(getActivity().getApplicationContext());
        final JSONObject jsonBody = new JSONObject();
        final String requestBody = jsonBody.toString();

       // Log.d("Yulis", "requestBody: " + requestBody);
       // Log.d("Yulis", "URL: " + Configuration.HOST_UPDATE_PASSWORD + id);

        StringRequest stringRequest = new StringRequest(Request.Method.DELETE, Configuration.HOST_DELETE_USER + id, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
               // Log.d("Yulis", "respuesta positiva");
                /*try {
                    //Log.i("Yulis---: ", response.toString());

                    JSONObject prueba = new JSONObject(response);

                } catch (JSONException e) {
                    e.printStackTrace();
                }*/

                //redireccionamos a login
                Intent intentDelete = new Intent(PageCuenta.this.getActivity(), MainActivity.class);
                PageCuenta.this.getActivity().finish();
                startActivity(intentDelete);
                ClearDatosToken();
                ClearDatosUsuario();
                ClearPassword();

                //Log.i("Yulis", "VOLLEY response get instituciones: " + response);
                Toast.makeText(getActivity().getApplicationContext(), "La cuenta se a eliminado exitosamente.", Toast.LENGTH_LONG).show();
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Log.e("Yulis", "VOLLEY: " + error.toString());
                //progressDialogLista.dismiss();
               // Log.d("Yulis", "Error de conexión");
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("Api-Function", "deleteItemMobile()");
                params.put("Authentication-Token", Preferencias_token);
                //params.put("api-parameters","{}");
                return params;
            }
        };

        requestQueue.add(stringRequest);

        requestQueue.addRequestFinishedListener(new RequestQueue.RequestFinishedListener<Object>() {
            @Override
            public void onRequestFinished(Request<Object> request) {
                requestQueue.getCache().clear();
            }
        });
    }


    /*
    *
    *
    */
    private String CargarDatosUsuario() {
        SharedPreferences preferencias = getActivity().getSharedPreferences("usuario", Context.MODE_PRIVATE);
        String Servicio = preferencias.getString("DatosUsuario", "No hay datos");
        Preferencias_datosUsuario = Servicio;
        return Servicio;
    }

    private String CargarToken() {
        SharedPreferences preferencias = getActivity().getSharedPreferences("token", Context.MODE_PRIVATE);
        String Servicio = preferencias.getString("DatosToken", "No hay token");
        Preferencias_token = Servicio;
        return Servicio;
    }

    /*
     * limpiar datos del usuario de las propertys
     */

    public void ClearPassword() {
        //limpiamos los datos del servicio activo
        SharedPreferences preferenciasSA = getActivity().getSharedPreferences("usuario", Context.MODE_PRIVATE);
        SharedPreferences.Editor edito = preferenciasSA.edit();
        edito.clear().apply();
        edito.commit();
    }

    public void ClearDatosUsuario() {
        //limpiamos los datos del servicio activo
        SharedPreferences preferenciasSA = getActivity().getSharedPreferences("token", Context.MODE_PRIVATE);
        SharedPreferences.Editor edito = preferenciasSA.edit();
        edito.clear().apply();
        edito.commit();
    }

    public void ClearDatosToken() {
        //limpiamos los datos del servicio activo
        SharedPreferences preferenciasSA = getActivity().getSharedPreferences("password", Context.MODE_PRIVATE);
        SharedPreferences.Editor edito = preferenciasSA.edit();
        edito.clear().apply();
        edito.commit();
    }

    public void BorrarSesion() {
        //limpiamos los datos del servicio activo
        SharedPreferences preferencias = getActivity().getSharedPreferences("estado", Context.MODE_PRIVATE);
        SharedPreferences.Editor edito = preferencias.edit();
        edito.clear();
        edito.commit();
    }

    @Override
    public void onStart() {
        super.onStart();
       // Log.d("Yolikan", "onStart");
    }

    @Override
    public void onResume() {
        super.onResume();
       // Log.d("Yolikan", "onResume");

        //cargamos datos del usuario que tiene sesion activa
        String datosUser = CargarDatosUsuario();
        //Log.i("Yulis---", datosUser);

        try {

            JSONObject prueba = new JSONObject(datosUser.trim());
            //Log.i("Yulis---", prueba.getString("auth_user_data"));

            JSONObject messageJsonPrueba = new JSONObject(prueba.getString("auth_user_data").toString());

            auth_user_id = messageJsonPrueba.getString("auth_user_id");
            String first_name = messageJsonPrueba.getString("first_name");
            String last_name = messageJsonPrueba.getString("last_name");
            String email = messageJsonPrueba.getString("email");
            String phone = messageJsonPrueba.getString("phone");

            txtNombreP.setText(first_name + " " + last_name);
            txtEmailP.setText(email);
            txtPhoneP.setText(phone);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
       // Log.d("Yolikan", "onPause");

    }

    @Override
    public void onStop() {
        super.onStop();
       // Log.d("Yolikan", "onStop");

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
       // Log.d("Yolikan", "onDestroy");

    }
}