package mx.com.appyolikancv01.fragments;

import android.Manifest;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.SystemClock;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import java.util.UUID;

import mx.com.appyolikancv01.R;
import mx.com.appyolikancv01.TestMenuActivity;
import mx.com.appyolikancv01.bluetooth.BluetoothController;
import mx.com.appyolikancv01.view.DeviceRecyclerViewAdapter;
import mx.com.appyolikancv01.view.ListInteractionListener;
import mx.com.appyolikancv01.view.RecyclerViewProgressEmptySupport;

public class PageFragment extends Fragment implements ListInteractionListener<BluetoothDevice> {
    /**
     * The controller for Bluetooth functionalities.
     */
    private BluetoothController bluetooth;

    /**
     * The Bluetooth discovery button.
     */
    private FloatingActionButton fab;

    /**
     * Progress dialog shown during the pairing process.
     */
    private ProgressDialog bondingProgressDialog;

    /**
     * Adapter for the recycler view.
     */
    private DeviceRecyclerViewAdapter recyclerViewAdapter;

    private RecyclerViewProgressEmptySupport recyclerView;

    private UUID mDeviceUUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");

    private  View view;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.lista_dispositivos,container, false);
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);



        // Changes the theme back from the splashscreen. It's very important that this is called
        // BEFORE onCreate.
        SystemClock.sleep(getResources().getInteger(R.integer.splashscreen_duration));
        getActivity().setTheme(R.style.AppTheme_NoActionBar);

        super.onCreate(savedInstanceState);
        //setContentView(R.layout.lista_dispositivos);

        checkPermission();

        // Sets up the RecyclerView.
        this.recyclerViewAdapter = new DeviceRecyclerViewAdapter(this);
        this.recyclerView = (RecyclerViewProgressEmptySupport) view.findViewById(R.id.list);
        this.recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));


        // Sets the view to show when the dataset is empty. IMPORTANT : this method must be called
        // before recyclerView.setAdapter().
        View emptyView = view.findViewById(R.id.empty_list);
        this.recyclerView.setEmptyView(emptyView);

        // Sets the view to show during progress.
        ProgressBar progressBar = (ProgressBar) view.findViewById(R.id.progressBar);
        this.recyclerView.setProgressView(progressBar);

        this.recyclerView.setAdapter(recyclerViewAdapter);

        // [#11] Ensures that the Bluetooth is available on this device before proceeding.
        boolean hasBluetooth = getActivity().getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH);
        if (!hasBluetooth) {
            AlertDialog dialog = new AlertDialog.Builder(getActivity()).create();
            dialog.setTitle(getString(R.string.bluetooth_not_available_title));
            dialog.setMessage(getString(R.string.bluetooth_not_available_message));
            dialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // Closes the dialog and terminates the activity.
                            dialog.dismiss();
                            getActivity().finish();
                        }
                    });
            dialog.setCancelable(false);
            dialog.show();
        }

        // Sets up the bluetooth controller.
        this.bluetooth = new BluetoothController(getActivity(), BluetoothAdapter.getDefaultAdapter(), recyclerViewAdapter);

        fab = (FloatingActionButton) view.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // If the bluetooth is not enabled, turns it on.
                if (!bluetooth.isBluetoothEnabled()) {
                    Snackbar.make(view, R.string.enabling_bluetooth, Snackbar.LENGTH_SHORT).show();
                    bluetooth.turnOnBluetoothAndScheduleDiscovery();
                } else {
                    //Prevents the user from spamming the button and thus glitching the UI.
                    if (!bluetooth.isDiscovering()) {
                        // Starts the discovery.
                        Snackbar.make(view, R.string.device_discovery_started, Snackbar.LENGTH_SHORT).show();
                        bluetooth.startDiscovery();
                    } else {
                        Snackbar.make(view, R.string.device_discovery_stopped, Snackbar.LENGTH_SHORT).show();
                        bluetooth.cancelDiscovery();
                    }
                }
            }
        });
        return view;
        //return inflater.inflate(R.layout.lista_dispositivos,container,false);
    }

    private void checkPermission(){


        LocationManager locationManager =
                (LocationManager) this.getActivity().getSystemService(Context.LOCATION_SERVICE);

        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setMessage("El GPS de tu dispositivo está apagado. La aplicación utiliza los datos " +
                    "de geolocalización para fines estadísticos - analíticos. ¿Desea habilitarlo?")
                    .setCancelable(false)
                    .setPositiveButton("Activar GPS", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                            startActivity(intent);
                        }
                    })
                    .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });
            AlertDialog alert = builder.create();
            alert.show();
        }


        if (ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            //Log.i("info", "No fine location permissions");

            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 111);
        }

        /*String locationProviders = Settings.Secure.getString(getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
        if(locationProviders == null || locationProviders.equals("")){
            showGPSDisabledAlertToUser();
        }*/
    }

    private void showGPSDisabledAlertToUser(){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        alertDialogBuilder.setMessage("El GPS de tu dispositivo está apagado. La aplicación utiliza los datos " +
                "de geolocalización para fines estadísticos - analíticos. ¿Desea habilitarlo?")
                .setCancelable(false)
                .setPositiveButton("Activar GPS",
                        new DialogInterface.OnClickListener(){
                            public void onClick(DialogInterface dialog, int id){
                                Intent callGPSSettingIntent = new Intent(
                                        android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                                startActivity(callGPSSettingIntent);
                                getActivity().finish();
                            }
                        });
        alertDialogBuilder.setNegativeButton("Cancelar",
                new DialogInterface.OnClickListener(){
                    public void onClick(DialogInterface dialog, int id){
                        dialog.cancel();
                        getActivity().finish();
                    }
                });
        AlertDialog alert = alertDialogBuilder.create();
        alert.show();
    }


    @Override
    public void onItemClick(BluetoothDevice device) {
        //Log.d(Configuration.TAG, "Item clicked : " + BluetoothController.deviceToString(device));
        if (bluetooth.isAlreadyPaired(device)) {
            //Log.d(Configuration.TAG, "Device already paired!");
            //Toast.makeText(this, R.string.device_already_paired, Toast.LENGTH_SHORT).show();

            monitoreo(device);
        } else {
            //Log.d(Configuration.TAG, "Device not paired. Pairing.");
            boolean outcome = bluetooth.pair(device);

            // Prints a message to the user.
            String deviceName = BluetoothController.getDeviceName(device);
            if (outcome) {
                // The pairing has started, shows a progress dialog.
               //Log.d(Configuration.TAG, "Showing pairing dialog");
                bondingProgressDialog = ProgressDialog.show(getActivity(), "", "Conectando con el dispositivo " + deviceName + "...", true, false);
            } else {
                //Log.d(Configuration.TAG, "Error while pairing with device " + deviceName + "!");
                Toast.makeText(getActivity(), "Error al trata de emparejar con " + deviceName + "!", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void startLoading() {
        this.recyclerView.startLoading();

        // Changes the button icon.
        this.fab.setImageResource(R.drawable.ic_bluetooth_searching_white_24dp);
    }

    @Override
    public void endLoading(boolean partialResults) {
        this.recyclerView.endLoading();

        // If discovery has ended, changes the button icon.
        if (!partialResults) {
            fab.setImageResource(R.drawable.ic_bluetooth_white_24dp);
        }
    }

    @Override
    public void endLoadingWithDialog(boolean error, BluetoothDevice device) {
        if (this.bondingProgressDialog != null) {
            View v = view.findViewById(R.id.main_content);
            String message;
            String deviceName = BluetoothController.getDeviceName(device);

            // Gets the message to print.
            if (error) {
                message = "Falló la conexión con " + deviceName + "!";
            } else {
                message = "Se ha conectado correctamente con " + deviceName + "!";
            }

            // Dismisses the progress dialog and prints a message to the user.
            this.bondingProgressDialog.dismiss();
            Snackbar.make(v, message, Snackbar.LENGTH_SHORT).show();

            // Cleans up state.
            this.bondingProgressDialog = null;


            monitoreo(device);
        }
    }

    private void monitoreo(BluetoothDevice device) {

        ((TestMenuActivity)getActivity()).changeMenu(device,mDeviceUUID.toString());
        // cambiar nombre aqui
        // AT+NAME=MY-HC-05.
        //////Intent intent = new Intent(getApplicationContext(), MonitoreoActivity.class);

        /*Intent intent = new Intent(view.getContext(), ServiceActivity.class);
        intent.putExtra("device", device);
        intent.putExtra("UUID", mDeviceUUID.toString());
        startActivity(intent);
        getActivity().finish();*/



        /*Intent intent = new Intent(view.getContext(), PausaTemperaturaActivity.class);
        intent.putExtra("device", device);
        intent.putExtra("UUID", mDeviceUUID.toString());
        startActivity(intent);
        getActivity().finish();*/

        /*Intent intent = new Intent(view.getContext(), TestMenuActivity.class);
        intent.putExtra("tipo", "1");
        intent.putExtra("device", device);
        intent.putExtra("UUID", mDeviceUUID.toString());
        startActivity(intent);
        getActivity().finish();*/

         /*Intent intent = new Intent(view.getContext(), TestMenuActivity.class);
        intent.putExtra("tipo", "1");
        startActivity(intent);
        getActivity().finish();*/

        /*FragmentServiceActivity fragmentServiceActivity = new FragmentServiceActivity();
        Bundle bundle = new Bundle();
        bundle.putParcelable("device", device);
        bundle.putString("UUID", mDeviceUUID.toString());
        fragmentServiceActivity.setArguments(bundle);
        //guardar datos device

        FragmentManager manager = getFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.add(R.id.container,fragmentServiceActivity,"Prueba");
        transaction.addToBackStack(null);
        transaction.attach(fragmentServiceActivity);
        transaction.commit();*/


    }

    private void GuardarDevice(BluetoothDevice Datos) {
        SharedPreferences preferencias = getActivity().getSharedPreferences("device", Context.MODE_PRIVATE);
        BluetoothDevice device = Datos;
        SharedPreferences.Editor editor = preferencias.edit();
        editor.putString("Datosdevice", String.valueOf(device));
        editor.commit();
    }
}
