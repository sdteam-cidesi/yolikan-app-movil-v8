package mx.com.appyolikancv01.view;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.widget.DatePicker;

import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;

import java.util.Calendar;

public class DatePickerCustom extends DialogFragment {

    private DatePickerDialog.OnDateSetListener listener;

    public static DatePickerCustom newInstance(DatePickerDialog.OnDateSetListener listener) {
        DatePickerCustom datePicker = new DatePickerCustom();
        datePicker.setListener(listener);
        return datePicker;
    }

    public void setListener(DatePickerDialog.OnDateSetListener listener) {
        this.listener = listener;
    }

    @Override
    @NonNull
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);

        return new DatePickerDialog(getActivity(), listener, year, month, day);
    }

}
