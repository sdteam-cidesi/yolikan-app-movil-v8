package mx.com.appyolikancv01;

import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.yarolegovich.lovelydialog.LovelyStandardDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import me.aflak.bluetooth.Bluetooth;
import me.aflak.bluetooth.interfaces.BluetoothCallback;
import me.aflak.bluetooth.interfaces.DeviceCallback;
import me.aflak.bluetooth.reader.SocketReader;

public class VisorTemperaturaActivity extends AppCompatActivity implements View.OnClickListener{

    //@BindView(R.id.activity_chat_status)
    TextView state;
    TextView tv_temperatura;
    TextView btn_todas_temp;
    TextView btn_ayuda;
    TextView tv_mensaje_temp;
    TextView tv_mensaje_emergencia;
    TextView tv_usuario;

    //@BindView(R.id.activity_chat_messages) TextView messages;
    //@BindView(R.id.activity_chat_hello_world)
    //Button helloWorld;
    ImageView btn_config, btn_close, btn_salir;
    Integer counter = 0;
    double temp_format = 0.0;

    private Bluetooth bluetooth;
    private BluetoothDevice device;
    View tempView;

    String id_usuario, temperatura, device_name, device_mac;
    JSONObject jsonObj;
    String JsonResponse = null;

    // Table Layout
    TableLayout tableLayout;
    Context context;

    String id_registro, fecha, tempRecord, dispositivo;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_visor_temperatura);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        //Log.d("H","Hola");
        device = getIntent().getParcelableExtra("device");

        device_name = device.getName();
        device_mac = device.getAddress();

        bluetooth = new Bluetooth(this);
        bluetooth.setCallbackOnUI(this);
        bluetooth.setDeviceCallback(deviceCallback);

        btn_close = findViewById(R.id.imageView);
        //state = findViewById(R.id.activity_chat_status);

        tv_temperatura = findViewById(R.id.tv_temperatura);
        tv_mensaje_temp = findViewById(R.id.tv_mensaje_temp);
        //tv_mensaje_emergencia = findViewById(R.id.tv_mensaje_emergencia);
        tv_mensaje_emergencia.setVisibility(View.INVISIBLE);
        tv_usuario = findViewById(R.id.tv_usuario);

        btn_todas_temp.setOnClickListener(this);
        btn_config.setOnClickListener(this);
        btn_close.setOnClickListener(this);
        btn_ayuda.setOnClickListener(this);
        btn_salir.setOnClickListener(this);

        //tempView = findViewById(R.id.constraint_layout);
        tempView.setBackgroundColor(ContextCompat.getColor(this, R.color.backgroundVerde));


        context = getApplicationContext();
        ///tableLayout = findViewById(R.id.table_layout_table);
        //tableLayout.setStretchAllColumns(true);

        showRegisters();
    }

    private void showRegisters() {
        final String[] respuesta = new String[1];
        final String[] dataRecords = new String[1];

        tableLayout.removeAllViews();

        Locale spanishLocale = new Locale("ES", "MX");
        SimpleDateFormat originalFormat = new SimpleDateFormat("yyyy-mm-dd HH:mm:ss", Locale.US);
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MMMM/yyyy HH:mm", spanishLocale);

        try {
            RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
            final JSONObject jsonBody = new JSONObject();

            jsonBody.put("id_user", id_usuario);

            final String requestBody = jsonBody.toString();
            //Log.d(Configuration.TAG, "body: " + requestBody);

            StringRequest stringRequest = new StringRequest(Request.Method.POST, Configuration.MIS_MEDICIONES, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    try {
                        JSONArray array = new JSONArray(response);
                        JSONObject obj;
                        for (int i = 0; i < array.length(); i++) {
                            obj = array.getJSONObject(i);
                            respuesta[0] = obj.getString("status");
                            dataRecords[0] = obj.getString("mediciones");
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }

                    /*try {
                        jsonObj = new JSONObject(response);
                        JsonResponse = response;
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }*/
                    //Log.i(Configuration.TAG, "VOLLEY response mis mediciones: " + respuesta[0] );

                    if(respuesta[0].equals("OK")){
                        try {
                            JSONArray arrayOK = new JSONArray(dataRecords[0]);
                            JSONObject obj;
                            for (int i = 0; i < (arrayOK.length() / 1); i++) {
                                obj = arrayOK.getJSONObject(i);
                                id_registro = obj.getString("id_registro");
                                fecha = obj.getString("fecha");
                                tempRecord = obj.getString("temperatura");
                                dispositivo = obj.getString("dispositivo");

                                //Log.d(Configuration.TAG, "temp: " + tempRecord);
                                String fechaFormat = parseDate(fecha, originalFormat, dateFormat);

                                /////Log.d(Configuration.TAG, "fechaaa: " + fechaFormat);

                                /// TABLE LAYOUT
                                TableRow tableRow = new TableRow(context);
                                // Set new table row layout parameters.
                                TableRow.LayoutParams layoutParams = new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT);
                                tableRow.setLayoutParams(layoutParams);

                                // Add a TextView in the first column.
                                TextView textView2 = new TextView(context);
                                textView2.setText(tempRecord + " °C");
                                textView2.setTextSize(18);
                                tableRow.addView(textView2, 0);

                                // Add a TextView in the first column.
                                TextView textView = new TextView(context);
                                textView.setText(fechaFormat);
                                textView.setTextSize(18);
                                tableRow.addView(textView, 0);

                                tableLayout.addView(tableRow);

                                //// TABLE LAYOUT
                            }

                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }


                }

            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    //Log.e(Configuration.TAG, "VOLLEY: " + error.toString());
                    //Log.d(Configuration.TAG, "Error de conexión");
                    finish();
                }
            }) {
                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }

                @Override
                public byte[] getBody() throws AuthFailureError {
                    try {
                        return requestBody == null ? null : requestBody.getBytes("utf-8");
                    } catch (UnsupportedEncodingException uee) {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                        return null;
                    }
                }
            };

            requestQueue.add(stringRequest);

            requestQueue.addRequestFinishedListener(new RequestQueue.RequestFinishedListener<Object>() {
                @Override
                public void onRequestFinished(Request<Object> request) {
                    requestQueue.getCache().clear();
                }
            });

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    protected void onStart() {
        super.onStart();

        //if(bluetooth.isConnected()){
        bluetooth.onStart();
        bluetooth.connectToDevice(device);
        state.setText("Conectando con " + device.getName());
        //}
        //else{
        //   state.setText("Conectando con " + device.getName());
        //}

    }

    @Override
    protected void onStop() {
        super.onStop();

        if(bluetooth.isConnected()){
            bluetooth.disable();
            bluetooth.disconnect();
            bluetooth.onStop();
        }
    }

    private DeviceCallback deviceCallback = new DeviceCallback() {
        @Override
        public void onDeviceConnected(BluetoothDevice device) {
            state.setText("Conectado");
            //helloWorld.setEnabled(true);

            //bluetooth.setReader(LineReader.class);
            //Log.d(Configuration.TAG, "Iniciando...");

        }

        @Override
        public void onDeviceDisconnected(BluetoothDevice device, String message) {
            state.setText("Dispositivo desconectado");
            ///helloWorld.setEnabled(false);
        }

        @Override
        public void onMessage(byte[] message) {
            //Log.d(Configuration.TAG, "onMessage_yulis ");
            String str = new String(message);

            ////Log.d(Configuration.TAG, str);
            if(str.length() > 5){
                String temperatura = str.substring(2, 6);
                tv_temperatura.setText(temperatura);

                if (!temperatura.equals("����"))  {
                    temp_format = Float.parseFloat(temperatura);
                    if(temp_format <= 37.0){
                        tempView.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.backgroundVerde));
                        tv_mensaje_temp.setText("Temperatura Normal");
                        tv_mensaje_emergencia.setVisibility(View.INVISIBLE);
                    }
                    if(temp_format >= 37.1 && temp_format <= 39.0){
                        tempView.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.backgroundAmarillo));
                        tv_mensaje_temp.setText("Temperatura Alta");
                        tv_mensaje_emergencia.setVisibility(View.INVISIBLE);
                    }
                    if(temp_format >= 39.1){
                        tempView.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.backgroundRojo));
                        tv_mensaje_temp.setText("Temperatura Muy Alta.");
                        tv_mensaje_emergencia.setVisibility(View.VISIBLE);
                    }

                    if((temp_format >= 30)){
                        //Log.d(Configuration.TAG, "Yulis:");
                        counter++;
                        //Log.d(Configuration.TAG, "temp: " + temp_format + ", " + counter);
                        if(counter == 10){
                            guardaRegistro(temp_format);
                        }
                    }
                }
            }


        }


        @Override
        public void onError(int errorCode) {
            //Log.d(Configuration.TAG, "onError");
        }

        @Override
        public void onConnectError(final BluetoothDevice device, String message) {
            state.setText("No se puede conectar, siguiente intento en 3 segundos... ");
            //Log.d(Configuration.TAG, "No se puede conectar, siguiente intento en 3 segundos... " + message);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    bluetooth.connectToDevice(device);
                }
            }, 3000);
        }
    };

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        // nothing happens
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.imageView:
                terminarLectura();
                break;
        }
    }

    private void terminarLectura() {
        /*new SweetAlertDialog(TemperaturaActivity.this)
                .setTitleText("Yolikan")
                .show();*/

        new LovelyStandardDialog(this, LovelyStandardDialog.ButtonLayout.VERTICAL)
                .setTopColorRes(R.color.colorAccent)
                .setButtonsColorRes(R.color.colorPrimaryDark)
                //.setIcon(R.drawable.ic_icon_yolikan)
                .setTitle("Yolikan")
                .setMessage("¿Desea desconectar el dispositivo?")
                .setPositiveButton(android.R.string.ok, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //Toast.makeText(getApplicationContext(), "positive clicked", Toast.LENGTH_SHORT).show();

                        Intent back = new Intent(getApplicationContext(), ListaDispositivosActivity.class);
                        startActivity(back);
                        finish();
                    }
                })
                .setNegativeButton(android.R.string.no, null)
                .show();
    }









    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        bluetooth.onActivityResult(requestCode, resultCode);

        //Log.d(Configuration.TAG, "result: ");
    }


    private BluetoothCallback bluetoothCallback = new BluetoothCallback() {
        @Override public void onBluetoothTurningOn() {}
        @Override public void onBluetoothTurningOff() {}
        @Override public void onBluetoothOff() {}

        @Override
        public void onBluetoothOn() {
            // doStuffWhenBluetoothOn() ...
            //Log.d(Configuration.TAG, "onBluetoothOn");
        }

        @Override
        public void onUserDeniedActivation() {
            // handle activation denial...
        }
    };


    public class LineReader extends SocketReader {
        private BufferedReader reader;

        public LineReader(InputStream inputStream) {
            super(inputStream);
            reader = new BufferedReader(new InputStreamReader(inputStream));

            //Log.d(Configuration.TAG, "messages: " + bluetooth.toString());
        }

        @Override
        public byte[] read() throws IOException {
            return reader.readLine().getBytes();
        }
    }



    public void guardaRegistro(double temp){
        //Log.d(Configuration.TAG, "Yulis");
        String temperature = String.format("%.1f", temp);
        //Log.d(Configuration.TAG, "Conta: " + temperature);

        final String[] respuesta = new String[1];

        try {
            RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
            final JSONObject jsonBody = new JSONObject();

            jsonBody.put("id_user", id_usuario);
            jsonBody.put("temperatura", temperature);
            jsonBody.put("dispositivo", device_name);
            jsonBody.put("mac", device_mac);

            final String requestBody = jsonBody.toString();

            StringRequest stringRequest = new StringRequest(Request.Method.POST, Configuration.GUARDA_MEDICION, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    try {
                        jsonObj = new JSONObject(response);
                        JsonResponse = response;
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    //Log.i(Configuration.TAG, "VOLLEY response guarda medicion: " + response);

                    counter  = 0;

                    showRegisters();

                }

            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    //Log.e(Configuration.TAG, "VOLLEY: " + error.toString());
                    //Log.d(Configuration.TAG, "Error de conexión");
                    finish();
                }
            }) {
                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }

                @Override
                public byte[] getBody() throws AuthFailureError {
                    try {
                        return requestBody == null ? null : requestBody.getBytes("utf-8");
                    } catch (UnsupportedEncodingException uee) {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                        return null;
                    }
                }
            };

            requestQueue.add(stringRequest);

            requestQueue.addRequestFinishedListener(new RequestQueue.RequestFinishedListener<Object>() {
                @Override
                public void onRequestFinished(Request<Object> request) {
                    requestQueue.getCache().clear();
                }
            });

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public static String parseDate(String inputDateString, SimpleDateFormat inputDateFormat, SimpleDateFormat outputDateFormat) {
        Date date = null;
        String outputDateString = null;
        try {
            date = inputDateFormat.parse(inputDateString);
            outputDateString = outputDateFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return outputDateString;
    }
}
