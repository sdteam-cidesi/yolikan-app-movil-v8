package mx.com.appyolikancv01;

import android.graphics.text.LineBreaker;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import mx.com.appyolikancv01.fragments.PageCuenta;


public class AvisoPrivacidadActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_aviso_privacidad);
        TextView textview = (TextView) findViewById( R.id.text_view );
        if (Build.VERSION.SDK_INT>=26){
            textview.setJustificationMode(LineBreaker.JUSTIFICATION_MODE_INTER_WORD);
        }
        textview.setText( Html.fromHtml( getString(R.string.privacidad) ) );
    }

    public void Regresar(View view){
        Fragment selectedFragment=null;
        selectedFragment=new PageCuenta();
        AvisoPrivacidadActivity.this.finish();
    }
}
