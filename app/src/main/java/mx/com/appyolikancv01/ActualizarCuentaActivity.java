package mx.com.appyolikancv01;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.fragment.app.Fragment;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.tiper.MaterialSpinner;
import com.yarolegovich.lovelydialog.LovelyStandardDialog;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import mx.com.appyolikancv01.fragments.PageCuenta;
import mx.com.appyolikancv01.view.DatePickerCustom;

public class ActualizarCuentaActivity extends AppCompatActivity implements View.OnClickListener {

    EditText et_nombre, et_apellidos, et_correo, et_telefono, et_usuario, et_pass1, et_pass2, et_fechaNacimiento;
    MaterialSpinner spinnerInstitucion;
    Button btn_crear_cuenta, btn_cancelar;
    CheckBox ckb_terminos;

    String nombre_inst_perfil;

    JSONArray jsonArr;
    String JsonResponse = null;
    ProgressDialog progressDialog;

    ArrayList institucionesList, departamentosList, gruposList;
    String idInstituciones[], idDepartamentos[], idGrupos[];
    ProgressDialog progressDialogLista;

    String id_institucion, institucion, descripcion, id_departamento, departamento, institucionDescr,
            institucionSel = "", deptoSel = "", grupoSel = "", id_grupo, grupo, tipo_cuenta, medico;

    String Preferencias_datosUsuario = "";
    String Preferencias_token = "";
    String Preferencias_Password = "";
    String auth_user_id = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        setContentView(R.layout.activity_account);

        et_nombre = findViewById(R.id.et_nombreN);
        et_apellidos = findViewById(R.id.et_apellidosN);
        et_fechaNacimiento = findViewById(R.id.et_fechaNacimiento);
        et_correo = findViewById(R.id.et_correoN);
        et_telefono = findViewById(R.id.et_telefonoN);
        et_usuario = findViewById(R.id.et_UsuarioN);
        et_pass1 = findViewById(R.id.et_password1N);
        et_pass2 = findViewById(R.id.et_password2N);
        spinnerInstitucion = findViewById(R.id.spinnerInstitucion);
        btn_crear_cuenta = findViewById(R.id.btnGuardarN);
        btn_cancelar = findViewById(R.id.btnCancelarN);
        ckb_terminos = findViewById(R.id.ckb_terminos);


        btn_crear_cuenta.setOnClickListener(this);
        btn_cancelar.setOnClickListener(this);
        et_fechaNacimiento.setOnClickListener(this);

        et_usuario.isEnabled();
        et_usuario.setEnabled(false);
        et_correo.isEnabled();
        et_correo.setEnabled(false);
        ckb_terminos.isEnabled();
        ckb_terminos.setEnabled(false);
        ckb_terminos.setChecked(true);

        getInstituciones();

        spinnerInstitucion.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(@NotNull MaterialSpinner materialSpinner, @Nullable View view, int position, long id) {
                id_institucion = idInstituciones[position];
                //Log.d(Configuration.TAG, "Select id_inst: " + id_institucion + ", institucion: " + spinnerInstitucion.getSelectedItem().toString());
                institucionSel = id_institucion;
            }

            @Override
            public void onNothingSelected(@NotNull MaterialSpinner materialSpinner) {

            }
        });


        //cargamos datos del usuario que tiene sesion activa
        String datosUser = CargarDatosUsuario();
        String datos_token = CargarToken();
        String datos_password = CargarPassword();
        //Log.i("Yulis---", datos_token);
        //Log.i("Yulis---", datosUser);
        //Log.i("Yulis---", datos_password);

        try {

            JSONObject prueba = new JSONObject(datosUser);
            //Log.i("Yulis---", prueba.getString("auth_user_data"));

            JSONObject messageJsonPrueba = new JSONObject(prueba.getString("auth_user_data").toString());

            auth_user_id = messageJsonPrueba.getString("auth_user_id");
            String username = messageJsonPrueba.getString("username");
            String first_name = messageJsonPrueba.getString("first_name");
            String last_name = messageJsonPrueba.getString("last_name");
            String patient_birthdate = messageJsonPrueba.getString("patient_birthdate");
            String email = messageJsonPrueba.getString("email");
            String phone = messageJsonPrueba.getString("phone");
            nombre_inst_perfil = messageJsonPrueba.getString("institution_register_name");

            //int idInstituciones = Integer.parseInt(institution_register_name);

            ///spinnerInstitucion.setSelection(idInstituciones);

            et_nombre.setText(first_name);
            et_apellidos.setText(last_name);
            et_fechaNacimiento.setText(patient_birthdate.replace("-","/"));
            et_correo.setText(email);
            et_telefono.setText(phone);
            et_usuario.setText(username);
            et_pass1.setText(datos_password);
            et_pass2.setText(datos_password);


        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnGuardarN:
                //validacion de campos vacios
                if (et_nombre.getText().toString().equals("")) {
                    et_nombre.setError("Ingrese su nombre");
                    Toast.makeText(getApplicationContext(), "Ingrese su nombre.", Toast.LENGTH_LONG).show();
                    et_nombre.requestFocus();
                } else {
                    if (et_apellidos.getText().toString().equals("")) {
                        et_apellidos.setError("Ingrese sus apellidos");
                        Toast.makeText(getApplicationContext(), "Ingrese sus apellidos.", Toast.LENGTH_LONG).show();
                        et_apellidos.requestFocus();
                    } else {
                        if (et_correo.getText().toString().equals("")) {
                            et_correo.setError("Ingrese su correo electrónico");
                            Toast.makeText(getApplicationContext(), "Ingrese su correo electrónico.", Toast.LENGTH_LONG).show();
                            et_correo.requestFocus();
                        } else {
                            if (et_telefono.getText().toString().equals("")) {
                                et_telefono.setError("Ingrese su teléfono");
                                Toast.makeText(getApplicationContext(), "Ingrese su teléfono.", Toast.LENGTH_LONG).show();
                                et_telefono.requestFocus();
                            } else {
                                if (et_usuario.getText().toString().equals("")) {
                                    et_usuario.setError("Ingrese su usuario");
                                    Toast.makeText(getApplicationContext(), "Ingrese su usuario.", Toast.LENGTH_LONG).show();
                                    et_usuario.requestFocus();
                                } else {
                                    if (institucionSel.equals("")) {
                                        Toast.makeText(getApplicationContext(), "Seleccione la institución.", Toast.LENGTH_LONG).show();
                                    } else {
                                        if (isEmailValid(et_correo.getText().toString().trim()) == false) {
                                            et_correo.setError("Correo no valido");
                                            Toast.makeText(getApplicationContext(), "Correo no valido.", Toast.LENGTH_LONG).show();
                                            et_correo.requestFocus();
                                            et_correo.setText("");
                                        } else {
                                            if (et_pass1.getText().toString().equals("")) {
                                                et_pass1.setError("Ingrese su contraseña");
                                                Toast.makeText(getApplicationContext(), "Ingrese su contraseña.", Toast.LENGTH_LONG).show();
                                                et_pass1.requestFocus();
                                            } else {
                                                if (institucionSel.equals("")) {
                                                    Toast.makeText(getApplicationContext(), "Seleccione la institución.", Toast.LENGTH_LONG).show();
                                                } else {
                                                    if (isEmailValid(et_correo.getText().toString().trim()) == false) {
                                                        et_correo.setError("Correo no valido");
                                                        Toast.makeText(getApplicationContext(), "Correo no valido.", Toast.LENGTH_LONG).show();
                                                        et_correo.requestFocus();
                                                        et_correo.setText("");
                                                    } else {
                                                        if (et_pass1.getText().toString().equals(et_pass2.getText().toString())) {

                                                            if (Configuration.isFechaValida(String.valueOf(et_fechaNacimiento.getText())) == false) {
                                                                et_fechaNacimiento.setError("Formato de fecha incorrecto.");
                                                                Toast.makeText(getApplicationContext(), "Formato de fecha incorrecto.", Toast.LENGTH_LONG).show();
                                                                et_fechaNacimiento.requestFocus();
                                                                et_fechaNacimiento.setText("");

                                                            } else {
                                                                //Toast.makeText(getApplicationContext(), "Ya se puede Actualizar.", Toast.LENGTH_LONG).show();
                                                                String username = et_usuario.getText().toString();
                                                                String first_name = et_nombre.getText().toString();
                                                                String last_name = et_apellidos.getText().toString();
                                                                String email = et_correo.getText().toString();
                                                                String phone = et_telefono.getText().toString();
                                                                int idUsuario = Integer.parseInt(auth_user_id);
                                                                String Password = et_pass1.getText().toString();
                                                                String fecha = et_fechaNacimiento.getText().toString();

                                                                //metodo para realizar el update
                                                                ActualizarUsuario(idUsuario, username.trim(), Password.trim(), first_name.trim(), last_name.trim(), email.trim(), phone.trim(), fecha);
                                                            }


                                                        } else {
                                                            et_pass1.setError("Las contraseñas no coinciden.");
                                                            et_pass2.setError("Las contraseñas no coinciden.");
                                                            Toast.makeText(getApplicationContext(), "Las contraseñas no coinciden.", Toast.LENGTH_LONG).show();
                                                            et_pass1.requestFocus();
                                                            et_pass1.setText("");
                                                            et_pass2.setText("");
                                                        }
                                                    }
                                                }
                                            }

                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                break;
            case R.id.btnCancelarN:
                Fragment selectedFragment = null;
                selectedFragment = new PageCuenta();
                ActualizarCuentaActivity.this.finish();
                //getSupportFragmentManager().beginTransaction().replace(R.id.container,selectedFragment).commit();
                break;

            case R.id.et_fechaNacimiento:
                showDatePickerDialog();
                break;
        }
    }

    private void showDatePickerDialog() {
        DatePickerCustom newFragment = DatePickerCustom.newInstance(new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                final String selectedDate = year + "/" + ("00".substring(String.valueOf(month+1).length()) + String.valueOf(month+1)) + "/" +  ("00".substring(String.valueOf(day).length())+String.valueOf(day));
                et_fechaNacimiento.setText(selectedDate);
            }
        });

        newFragment.show(getSupportFragmentManager(), "datePicker");;
    }

    private void getInstituciones() {

        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        final JSONObject jsonBody = new JSONObject();
        final String requestBody = jsonBody.toString();

        StringRequest stringRequest = new StringRequest(Request.Method.GET, Configuration.HOST_Instituciones, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                //Log.d("Yulis", "respuesta positiva");
                try {
                    //Log.i("Yulis---: ", response.toString());

                    JSONObject prueba = new JSONObject(response);

                    //Log.i("Yulis---", prueba.getString("data"));
                    String institucionArr = prueba.getString("data");

                    JSONArray arrayOK = new JSONArray(institucionArr);
                    JSONObject objInst;

                    institucionesList = new ArrayList();
                    idInstituciones = new String[arrayOK.length()];

                    for (int i = 0; i < arrayOK.length(); i++) {
                        objInst = arrayOK.getJSONObject(i);
                        id_institucion = objInst.getString("id");
                        institucion = objInst.getString("institution_name");
                        descripcion = objInst.getString("institution_description");

                        //Log.d(Configuration.TAG, "Inst: " + institucion);

                        idInstituciones[i] = id_institucion;
                        institucionesList.add(institucion);
                    }

                    ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(ActualizarCuentaActivity.this, android.R.layout.simple_spinner_dropdown_item, institucionesList) {
                    };
                    adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spinnerInstitucion.setAdapter(adapter1);


                    spinnerInstitucion.setSelection(adapter1.getPosition(nombre_inst_perfil));

                    //getDepartamentos(id_institucion_perfil);

                    ///spinnerInstitucion.setSelection(0);

                    //progressDialogLista.dismiss();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                //Log.i(Configuration.TAG, "VOLLEY response get instituciones: " + response);
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Log.e(Configuration.TAG, "VOLLEY: " + error.toString());
                //progressDialogLista.dismiss();
                //Log.d(Configuration.TAG, "Error de conexión");
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("Api-Function", "getItems()");
                //params.put("api-parameters","{}");
                return params;
            }
        };

        requestQueue.add(stringRequest);

        requestQueue.addRequestFinishedListener(new RequestQueue.RequestFinishedListener<Object>() {
            @Override
            public void onRequestFinished(Request<Object> request) {
                requestQueue.getCache().clear();
            }
        });

    }

    public void ActualizarUsuario(Integer id, String username, String Password, String first_name, String last_name, String email, String phone, String Fecha) {

        progressDialog = new ProgressDialog(ActualizarCuentaActivity.this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Actualizando datos del usuario...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setProgress(0);
        progressDialog.show();

        final String[] respuesta = {""};

        try {
            RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
            final JSONObject jsonBody = new JSONObject();

            jsonBody.put("username", username);
            jsonBody.put("Password", Password);
            jsonBody.put("first_name", first_name);
            jsonBody.put("last_name", last_name);
            jsonBody.put("email", email);
            jsonBody.put("phone", phone);
            jsonBody.put("institution_register_id", Integer.parseInt(institucionSel));
            jsonBody.put("is_doctor_flag", false);
            jsonBody.put("patient_birthdate", Fecha);

            final String requestBody = jsonBody.toString();

            //Log.d(Configuration.TAG, "requestBody: " + requestBody);

            JsonObjectRequest request = new JsonObjectRequest(Request.Method.PUT, Configuration.HOST_UPDATE_USUARIO + id, jsonBody, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    //Dialog.cancel();
                    //Log.i("Respuesta: ", response.toString());

                    try {
                        //Log.i("Respuesta_data: ", response.getString("data"));
                        String p = response.getString("data").replace("[","");
                        String p1 = p.replace("]","");

                        //Log.i("Respuesta_p1: ", p1);

                        String p3 = "{ \"auth_user_data\":"+ p1+" ";

                        //Log.i("Respuesta_p3: ", p3);

                        JSONObject myJson = new JSONObject("{\"auth_user_data\": "+ p1 +"}");

                        //Log.i("Respuesta_myJson: ", myJson.toString());

                        ClearDatosUsuario();

                        GuardarDatosUsuario(myJson.toString());

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }




                    progressDialog.dismiss();

                    //Intent intent = new Intent(ActualizarCuentaActivity.this, MainActivity.class);
                    //ActualizarCuentaActivity.this.finish();
                    //startActivity(intent);
                    ActualizarCuentaActivity.this.finish();
                    Toast.makeText(getApplicationContext(), "Datos del usuario actualizados correctamente.", Toast.LENGTH_LONG).show();

                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    try {
                        //Dialog.cancel();
                        progressDialog.dismiss();
                        //Log.d("Login...", error.networkResponse.statusCode + "");

                        int estatus = error.networkResponse.statusCode;
                        String aString = Integer.toString(estatus);//403

                        //Log.d("Codigo...", aString);

                        switch (aString) {
                            case "401":
                                //Log.i("-------case 1-------", "Contraseña o usuario incorrectos.");
                                CharSequence text = "Contraseña o usuario incorrecto.";
                                Toast.makeText(getApplicationContext(), text, Toast.LENGTH_LONG).show();
                                break;

                            /*case "403":
                                //Log.i("-------case 2-------", "Sesión iniciada en otro dispositivo.");
                                CharSequence text1 = "Sesión iniciada en otro dispositivo.";
                                Toast.makeText(getApplicationContext(), text1, Toast.LENGTH_SHORT).show();
                                break;*/

                            default:
                                //Log.i("-------Default-------", "Error.");
                                CharSequence text2 = "Error de comunicación";
                                Toast.makeText(getApplicationContext(), text2, Toast.LENGTH_LONG).show();
                        }

                    } catch (Exception e) {
                        e.getMessage();
                    }

                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("Content-Type", "application/json");
                    params.put("Api-Function", "updateItemMobile()");
                    params.put("Authentication-Token", Preferencias_token);
                    //params.put("api-parameters","{}");
                    return params;
                }
            };

            requestQueue.add(request);

            requestQueue.addRequestFinishedListener(new RequestQueue.RequestFinishedListener<Object>() {
                @Override
                public void onRequestFinished(Request<Object> request) {
                    requestQueue.getCache().clear();
                }
            });

        } catch (JSONException e) {
            progressDialog.dismiss();
            e.printStackTrace();

            new LovelyStandardDialog(ActualizarCuentaActivity.this, LovelyStandardDialog.ButtonLayout.VERTICAL)
                    .setTopColorRes(R.color.colorAccent)
                    .setButtonsColorRes(R.color.colorPrimaryDark)
                    .setTitle("Yolikan")
                    .setMessage("Error de conexión. " + e.getMessage())
                    .setPositiveButton(android.R.string.ok, new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                        }
                    })
                    //.setNegativeButton(android.R.string.no, null)
                    .show();
        }

    }

    private void GuardarDatosUsuario(String Datos) {
        SharedPreferences preferencias = getSharedPreferences("usuario", Context.MODE_PRIVATE);
        String DatosUsuario = Datos;
        SharedPreferences.Editor editor = preferencias.edit();
        editor.putString("DatosUsuario", DatosUsuario);
        editor.commit();
    }

    public void ClearDatosUsuario() {
        //limpiamos los datos del servicio activo
        SharedPreferences preferenciasSA = getSharedPreferences("usuario", Context.MODE_PRIVATE);
        SharedPreferences.Editor edito = preferenciasSA.edit();
        edito.clear().apply();
        edito.commit();
    }


    public static boolean isEmailValid(String email) {
        boolean isValid = false;

        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        CharSequence inputStr = email;

        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);
        if (matcher.matches()) {
            isValid = true;
        }
        return isValid;
    }

    private String CargarDatosUsuario() {
        SharedPreferences preferencias = getSharedPreferences("usuario", Context.MODE_PRIVATE);
        String Servicio = preferencias.getString("DatosUsuario", "No hay datos");
        Preferencias_datosUsuario = Servicio;
        return Servicio;
    }

    private String CargarToken() {
        SharedPreferences preferencias = getSharedPreferences("token", Context.MODE_PRIVATE);
        String Servicio = preferencias.getString("DatosToken", "No hay token");
        Preferencias_token = Servicio;
        return Servicio;
    }

    private String CargarPassword() {
        SharedPreferences preferencias = getSharedPreferences("password", Context.MODE_PRIVATE);
        String Servicio = preferencias.getString("DatosPassword", "No hay password");
        Preferencias_Password = Servicio;
        return Servicio;
    }

}