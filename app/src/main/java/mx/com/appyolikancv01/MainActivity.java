package mx.com.appyolikancv01;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;


public class MainActivity extends AppCompatActivity {

    ProgressDialog progressDialog;
    private EditText etUsuario;
    private EditText etContrasena;
    private ProgressDialog Dialog;
    AlertDialog.Builder builder;

    private static String PREFERENCE_ESTADO_BUTTON_SESION = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Dialog = new ProgressDialog(MainActivity.this);
        builder = new AlertDialog.Builder(MainActivity.this);

        etUsuario = (EditText) findViewById(R.id.etUsuario);
        etContrasena = (EditText) findViewById(R.id.etContrasena);

    }

    public void Loginapp(View view) {

        boolean conn = ConexionInternet();
        if (conn == true) {

            ///ping a google
            if (!isOnlineNetServerPulseras()) {
                //System.out.println("it's false");

                Intent intent = new Intent(MainActivity.this, MainActivity.class);
                MainActivity.this.finish();
                startActivity(intent);

                new Thread()
                {
                    public void run()
                    {
                        MainActivity.this.runOnUiThread(new Runnable()
                        {
                            public void run()
                            {
                                //Do your UI operations like dialog opening or Toast here
                                Toast.makeText(MainActivity.this, "Error de conexion a internet.", Toast.LENGTH_LONG).show();
                            }
                        });
                    }
                }.start();

            } else {
                //System.out.println("ping a servidor IOT, it's true");
                String Usuario = etUsuario.getText().toString();
                String Password = etContrasena.getText().toString();

                if (etUsuario.getText().toString().isEmpty()) {
                    //Toast.makeText(this, "Ingrese usuario", Toast.LENGTH_LONG).show();

                    Toast toast = new Toast(this);
                    //usamos cualquier layout como Toast
                    View toast_layout = getLayoutInflater().inflate(R.layout.custom_toast, (ViewGroup) findViewById(R.id.lytLayout));
                    toast.setView(toast_layout);
                    //se podría definir el texto en el layout si es invariable pero lo hacemos programáticamente como ejemplo
                    //tenemos acceso a cualquier widget del layout del Toast
                    TextView textView = (TextView) toast_layout.findViewById(R.id.toastMessage);
                    textView.setText("Ingrese usuario");
                    toast.setDuration(Toast.LENGTH_SHORT);
                    toast.show();



                } else {
                    if (etContrasena.getText().toString().isEmpty()) {
                        //Toast.makeText(this, "Ingrese contraseña", Toast.LENGTH_LONG).show();

                        Toast toast = new Toast(this);
                        //usamos cualquier layout como Toast
                        View toast_layout = getLayoutInflater().inflate(R.layout.custom_toast, (ViewGroup) findViewById(R.id.lytLayout));
                        toast.setView(toast_layout);
                        //se podría definir el texto en el layout si es invariable pero lo hacemos programáticamente como ejemplo
                        //tenemos acceso a cualquier widget del layout del Toast
                        TextView textView = (TextView) toast_layout.findViewById(R.id.toastMessage);
                        textView.setText("Ingrese contraseña");
                        toast.setDuration(Toast.LENGTH_SHORT);
                        toast.show();

                    } else {
                        //Log.d("log", "iniciando sesion, espere");
                        Dialog.setTitle("Iniciando sesión");
                        Dialog.setMessage("Espere, por favor...");
                        Dialog.setCancelable(false);
                        Dialog.show();
                        //loader
                        Peticion_login(Usuario.trim(),Password.trim());
                    }
                }
            }
        } else {
            //Toast.makeText(getApplicationContext(), "Validar conexión de datos", Toast.LENGTH_LONG).show();

            Toast toast = new Toast(this);
            //usamos cualquier layout como Toast
            View toast_layout = getLayoutInflater().inflate(R.layout.custom_toast, (ViewGroup) findViewById(R.id.lytLayout));
            toast.setView(toast_layout);
            TextView textView = (TextView) toast_layout.findViewById(R.id.toastMessage);
            textView.setText("Validar conexión de datos");
            toast.setDuration(Toast.LENGTH_SHORT);
            toast.show();
            //Toast.makeText(this,"Conexion: " + conn, Toast.LENGTH_LONG).show();
        }

        //Peticion_login("yuliana","yuliana2021");

        /*Intent intent = new Intent(MainActivity.this, TestMenuActivity.class);
        intent.putExtra("tipo", "2");
        MainActivity.this.finish();
        startActivity(intent);*/

    }

    public void nuevaCuenta(View view){
        Intent intent = new Intent(MainActivity.this, NuevaCuentaActivity.class);
        MainActivity.this.finish();
        startActivity(intent);
    }

    public void resetPassword(View view){
        Intent intent = new Intent(MainActivity.this, ResetPassword.class);
        MainActivity.this.finish();
        startActivity(intent);
    }


    //peticiones con encabezados
    private void Peticion_login(String user, String pass){

        RequestQueue queue = Volley.newRequestQueue(this);
        try {

            JSONObject jsonBody = new JSONObject();
            jsonBody.put("username", user);//yuliana
            jsonBody.put("password", pass);//yuliana2021

            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, Configuration.HOST_login, jsonBody, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Dialog.cancel();
                    //Log.i("Respuesta: ", response.toString());

                    try {
                        String auth_user_data = response.getString("data");
                        //Log.i("Respuesta_auth_user", auth_user_data.toString());

                        JSONObject resultsObject = response.getJSONObject("data");
                        //Log.i("Respuesta_TOKEN", resultsObject.getString("token"));
                        //Log.i("Respuesta_user_data", resultsObject.getString("auth_user_data"));
                        //guardarPassword
                        GuardarPassword(etContrasena.getText().toString());


                        //progressDialog.dismiss();
                        //guardar el token
                        GuardarToken(resultsObject.getString("token"));
                        // Base64
                        String text = resultsObject.getString("auth_user_data");// "eyJhdXRoX3VzZXJfZGF0YSI6eyJhdXRoX3VzZXJfaWQiOjYzLCJ1c2VybmFtZSI6Inl1bGlhbmEiLCJmaXJzdF9uYW1lIjoiWXVsaWFuYSIsImxhc3RfbmFtZSI6Ik1hdGEgQ29yY2hhZG8iLCJlbWFpbCI6Inl1bGlhbmEubWF0YUBjaWRlc2kuZWR1Lm14IiwicGljdHVyZSI6bnVsbCwicGhvbmUiOiI0NDIyNDUxMDAwIiwiaXNfZG9jdG9yX2ZsYWciOmZhbHNlLCJpc19tb2JpbGVfZmxhZyI6dHJ1ZSwiZ3JvdXBfcmVnaXN0ZXJfaWQiOjEsImdyb3VwX3JlZ2lzdGVyX25hbWUiOiJHcnVwb19Vbm8iLCJkZXBhcm1lbnRfcmVnaXN0ZXJfaWQiOjEsImRlcGFybWVudF9yZWdpc3Rlcl9uYW1lIjoiVXJnZW5jaWFzIiwiaW5zdGl0dXRpb25fcmVnaXN0ZXJfaWQiOjIsImluc3RpdHV0aW9uX3JlZ2lzdGVyX25hbWUiOiJIRU5NIiwicGFzc3dvcmRfcmVzZXRfZmxhZyI6ZmFsc2UsInBhc3N3b3JkX3Jlc2V0X3V1aWQiOm51bGx9fQ";
                        byte[] data = Base64.decode(text, Base64.DEFAULT);
                        String text1 = new String(data, StandardCharsets.UTF_8);
                        //Log.i("Respuesta----", text1.toString());
                        //Guardar datos del usuario
                        GuardarDatosUsuario(text1);

                        //sesion activa
                        PREFERENCE_ESTADO_BUTTON_SESION = "si";
                        guardarEstado(PREFERENCE_ESTADO_BUTTON_SESION);

                        // redireccionar a menu
                        Intent intent = new Intent(MainActivity.this, TestMenuActivity.class);
                        MainActivity.this.finish();
                        startActivity(intent);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener(){

                @Override
                public void onErrorResponse(VolleyError error) {
                    try {
                        Dialog.cancel();

                        //Log.d("Login...", error.networkResponse.statusCode + "");

                        int estatus = error.networkResponse.statusCode;
                        String aString = Integer.toString(estatus);//403

                        //Log.d("Codigo...", aString);

                        switch (aString) {
                            case "401":
                                //Log.i("-------case 1-------", "Contraseña o usuario incorrectos.");
                                //CharSequence text = "Contraseña o usuario incorrecto.";
                                ///Toast.makeText(getApplicationContext(), text, Toast.LENGTH_LONG).show();
                                Toast toast = new Toast(MainActivity.this);
                                //usamos cualquier layout como Toast
                                View toast_layout = getLayoutInflater().inflate(R.layout.custom_toast, (ViewGroup) findViewById(R.id.lytLayout));
                                toast.setView(toast_layout);
                                TextView textView = (TextView) toast_layout.findViewById(R.id.toastMessage);
                                textView.setText("Contraseña o usuario incorrecto.");
                                toast.setDuration(Toast.LENGTH_SHORT);
                                toast.show();
                                break;

                            /*case "403":
                                //Log.i("-------case 2-------", "Sesión iniciada en otro dispositivo.");
                                CharSequence text1 = "Sesión iniciada en otro dispositivo.";
                                Toast.makeText(getApplicationContext(), text1, Toast.LENGTH_SHORT).show();
                                break;*/

                            default:
                                //Log.i("-------Default-------", "Error.");
                                CharSequence text2 = "Error de comunicación";
                                Toast.makeText(getApplicationContext(), text2, Toast.LENGTH_LONG).show();
                        }

                    } catch (Exception e) {
                        e.getMessage();
                    }

                }
            })
            {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String,String> params = new HashMap<String, String>();
                    params.put("Content-Type","application/json");
                    params.put("Api-Function","getTokenMobile()");
                    //params.put("api-parameters","{}");
                    return params;
                }
            };
            queue.add(request);

        } catch (Exception e) {
            e.getMessage();
        }
    }

    public boolean ConexionInternet() {
        boolean conexion = false;
        //PRIMERO ---  validacion si hay conexion a internet
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

        try {
            ConnectivityManager cm;
            NetworkInfo ni;
            cm = (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);
            ni = cm.getActiveNetworkInfo();
            boolean tipoConexion1 = false;
            boolean tipoConexion2 = false;

            if (ni != null) {
                ConnectivityManager connManager1 = (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo mWifi = connManager1.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

                ConnectivityManager connManager2 = (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo mMobile = connManager2.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

                if (mWifi.isConnected() || mMobile.isConnected()) {
                    //Log.d("MIAPP", "Estás Conectado al wifi o datos 3G");
                    //Log.d("MIAPP", " Estado actual wifi: " + mWifi.getState());
                    //Log.d("MIAPP", " Estado actual datos 3G: " + mMobile.getState());

                    conexion = true;
                }
            } else {
                /* No estas conectado a internet */
                //Log.d("MIAPP", "NO Estás Conectado a ninguno");
                Toast.makeText(getApplicationContext(), "No hay conexión a internet", Toast.LENGTH_LONG).show();
                conexion = false;
            }

            return conexion;

        } catch (Exception e) {
            e.getMessage();
            //Log.d("MIAPP", "entro en el catch");
            //Toast.makeText(getApplicationContext(), "Validar conexión de datos", Toast.LENGTH_LONG).show();
            conexion = false;
        }

        return conexion;
    }

    public Boolean isOnlineNetServerPulseras() {

        try {

            Process p = java.lang.Runtime.getRuntime().exec("ping -c 1 "+ Configuration.server);

            int val = p.waitFor();
            boolean reachable = (val == 0);
            return reachable;

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return false;
    }

    /*
     * Función para guardar datos del usuario
     *
     */
    private void GuardarDatosUsuario(String Datos) {
        SharedPreferences preferencias = getSharedPreferences("usuario", Context.MODE_PRIVATE);
        String DatosUsuario = Datos;
        SharedPreferences.Editor editor = preferencias.edit();
        editor.putString("DatosUsuario", DatosUsuario);
        editor.commit();
    }

    /*
     * Función para guardar token
     *
     */
    private void GuardarToken(String Datos) {
        SharedPreferences preferencias = getSharedPreferences("token", Context.MODE_PRIVATE);
        String Token = Datos;
        SharedPreferences.Editor editor = preferencias.edit();
        editor.putString("DatosToken", Token);
        editor.commit();
    }

    /*
     * Función para guardar contraseña
     *
     */
    private void GuardarPassword(String Datos) {
        SharedPreferences preferencias = getSharedPreferences("password", Context.MODE_PRIVATE);
        String Token = Datos;
        SharedPreferences.Editor editor = preferencias.edit();
        editor.putString("DatosPassword", Token);
        editor.commit();
    }

    public void guardarEstado(String estado) {
        SharedPreferences preferencias = getSharedPreferences("estado", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferencias.edit();
        editor.putString("activo", estado);
        editor.commit();
    }
}
